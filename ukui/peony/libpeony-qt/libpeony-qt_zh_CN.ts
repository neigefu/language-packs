<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>ConnectServerDialog</name>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="14"/>
        <source>Connect to Sever</source>
        <translation>连接到服务器</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="32"/>
        <source>Domain</source>
        <translation>域名</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="39"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="55"/>
        <source>Save Password</source>
        <translation>记住密码</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="62"/>
        <source>User</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="82"/>
        <source>Anonymous</source>
        <translation>匿名登录</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.cpp" line="35"/>
        <source>Ok</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.cpp" line="36"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>DiscControl</name>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="453"/>
        <source> is busy!</source>
        <translation>被占用！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="492"/>
        <source>is busy!</source>
        <translation>被占用！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="539"/>
        <source> not support udf at present.</source>
        <translation>目前不支持udf格式化</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="546"/>
        <source>unmount disc failed before udf format.</source>
        <translation>在 udf 格式之前卸载磁盘失败。</translation>
    </message>
    <message>
        <source>is not properly formatted.</source>
        <translation type="vanished">格式不正确。</translation>
    </message>
    <message>
        <source>Can not found newfs_udf tool.</source>
        <translation type="vanished">未找到newfs_udf工具。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="699"/>
        <source>DVD+RW udf format fail.</source>
        <translation>DVD+RW udf格式化失败</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="731"/>
        <source>preparation failed before DVD-RW udf format.</source>
        <translation>DVD-RW udf 格式之前的准备失败。</translation>
    </message>
</context>
<context>
    <name>FileLabelModel</name>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="37"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="56"/>
        <source>Red</source>
        <translation>红色</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="38"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="57"/>
        <source>Orange</source>
        <translation>橙色</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="39"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="58"/>
        <source>Yellow</source>
        <translation>黄色</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="40"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="59"/>
        <source>Green</source>
        <translation>绿色</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="41"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="60"/>
        <source>Blue</source>
        <translation>蓝色</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="42"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="61"/>
        <source>Purple</source>
        <translation>紫色</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="43"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="62"/>
        <source>Gray</source>
        <translation>灰色</translation>
    </message>
    <message>
        <source>Transparent</source>
        <translation type="vanished">无颜色</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="129"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="357"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="129"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="357"/>
        <source>Label or color is duplicated.</source>
        <translation>标签或者颜色重复</translation>
    </message>
</context>
<context>
    <name>FileOperationHelper</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-helper.cpp" line="157"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-helper.cpp" line="175"/>
        <source>Burn failed</source>
        <translation>刻录失败</translation>
    </message>
</context>
<context>
    <name>Format_Dialog</name>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>格式化</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="32"/>
        <source>rom_size</source>
        <translation>容量大小</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="45"/>
        <source>system</source>
        <translation>文件系统</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="59"/>
        <source>vfat/fat32</source>
        <translation>vfat/fat32</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="64"/>
        <source>exfat</source>
        <translation>exfat</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="69"/>
        <source>ntfs</source>
        <translation>NTFS</translation>
    </message>
    <message>
        <source>vfat</source>
        <translation type="vanished">VFAT</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="74"/>
        <source>ext4</source>
        <translation>Ext4</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="88"/>
        <source>device_name</source>
        <translation>设备名称</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="114"/>
        <source>clean it total</source>
        <translation>完全擦除(时间较长,请确认!)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="127"/>
        <source>ok</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="140"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="179"/>
        <source>TextLabel</source>
        <translation>容量</translation>
    </message>
    <message>
        <source>qmesg_notify</source>
        <translation type="vanished">通知</translation>
    </message>
    <message>
        <source>Format operation has been finished successfully.</source>
        <translation type="vanished">格式化操作已成功完成。</translation>
    </message>
    <message>
        <source>Sorry, the format operation is failed!</source>
        <translation type="vanished">很遗憾，格式化操作失败了，您可以重新试下！</translation>
    </message>
    <message>
        <source>Formatting this volume will erase all data on it. Please backup all retained data before formatting. Do you want to continue ?</source>
        <translation type="vanished">格式化此卷将清除其上的所有数据。请在格式化之前备份所有保留的数据。您想继续吗?</translation>
    </message>
    <message>
        <source>format</source>
        <translation type="vanished">格式化</translation>
    </message>
    <message>
        <source>begin format</source>
        <translation type="vanished">开始</translation>
    </message>
    <message>
        <source>format_success</source>
        <translation type="vanished">格式化成功!</translation>
    </message>
    <message>
        <source>format_err</source>
        <translation type="vanished">格式化失败!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="154"/>
        <source>Format</source>
        <translation>格式化</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="167"/>
        <source>Rom size:</source>
        <translation>容量大小</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="173"/>
        <source>Filesystem:</source>
        <translation>文件系统</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="183"/>
        <source>Disk name:</source>
        <translation>设备名称</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="213"/>
        <source>Completely erase(Time is longer, please confirm!)</source>
        <translation>完全擦除(时间较长,请确认!)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="231"/>
        <source>Set password</source>
        <translation>设置密码</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="232"/>
        <source>Set password for volume based on LUKS (only ext4)</source>
        <translation>为ext4分区设置基于LUKS的密码</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="238"/>
        <source>Formatting to the ext4 file system may cause other users to be unable to read or write to the USB drive</source>
        <translation>格式化为ext4文件系统时可能会导致其他用户无法读写U盘</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="246"/>
        <source>Cancel</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="247"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="360"/>
        <source>Data</source>
        <translation>数据盘</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="238"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="564"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="564"/>
        <source>Device name cannot start with a decimal point, Please re-enter!</source>
        <translation>设备名称不能以小数点开始，请重新输入！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="575"/>
        <source>Enter Password:</source>
        <translation>输入密码:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="592"/>
        <source>Password too short, please retype a password more than 6 characters</source>
        <translation>密码过短, 请重新输入大于6位的密码</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="691"/>
        <source>over one day</source>
        <translation>超过一天</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="691"/>
        <source>%1/sec, %2 remaining.</source>
        <translation>每秒%1, 剩余时间%2.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="693"/>
        <source>getting progress...</source>
        <translation>获取进度中...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1278"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1278"/>
        <source>Block not existed!</source>
        <translation>设备不存在!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1325"/>
        <source>Formatting. Do not close this window</source>
        <translation>正在格式化, 请勿关闭</translation>
    </message>
</context>
<context>
    <name>KyFileDialogRename</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="53"/>
        <source>Renaming &quot;%1&quot;</source>
        <translation>正在重命名 &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="54"/>
        <source>Renaming failed, the reason is: %1</source>
        <translation>重命名失败, 原因: %1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="54"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="63"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="72"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="77"/>
        <source>Filename too long</source>
        <translation>文件名过长</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="58"/>
        <source>Copying &quot;%1&quot;</source>
        <translation>正在复制 &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="62"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="71"/>
        <source>To &quot;%1&quot;</source>
        <translation>到 &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="63"/>
        <source>Copying failed, the reason is: %1</source>
        <translation>复制失败, 原因: %1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="67"/>
        <source>Moving &quot;%1&quot;</source>
        <translation>正在移动 &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="72"/>
        <source>Moving failed, the reason is: %1</source>
        <translation>移动失败, 原因: %1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="76"/>
        <source>File operation error:</source>
        <translation>文件操作错误:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="77"/>
        <source>The reason is: %1</source>
        <translation>原因: %1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="96"/>
        <source>Truncation</source>
        <translation>截断</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="127"/>
        <source>All applications</source>
        <translation>全部应用</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="130"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="168"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="228"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="131"/>
        <source>Apply</source>
        <translation>应用</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="284"/>
        <source>Explanation: Truncate the portion of the file name that exceeds 225 bytes and select</source>
        <translation>说明：截断文件名的超过225字节的部分，去选择</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="293"/>
        <source>Description: By default, save to &quot;%1/扩展&quot;.</source>
        <translation>说明：默认保存至“%1/扩展”。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="302"/>
        <source>Explanation: When renaming a file name, ensure it is within 225 bytes and </source>
        <translation>说明：用户重命名文件名，保证在225字节以内，去</translation>
    </message>
    <message>
        <source>Explanation: Truncate the portion of the file name that exceeds 225 bytes and </source>
        <translation type="obsolete">说明：用户重命名文件名，保证在225字节以内，</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="97"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="169"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="164"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="214"/>
        <source>Bytes</source>
        <translation>字节</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="220"/>
        <source>Front truncation</source>
        <translation>前截断</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="221"/>
        <source>Post truncation</source>
        <translation>后截断</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="277"/>
        <source>Description: Skip copying files of the current type</source>
        <translation>说明：跳过当前类型文件的复制。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="282"/>
        <source>truncate interval</source>
        <translation>截断区间</translation>
    </message>
    <message>
        <source>Explanation: Truncate the portion of the file name that exceeds 225 bytes </source>
        <translation type="vanished">说明：截断文件名的超过225字节的部分，去选择</translation>
    </message>
    <message>
        <source>Description: By default, save to &quot;%1/extension&quot;.</source>
        <translation type="vanished">说明：默认保存至“%1/拓展”。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="300"/>
        <source>modify the name</source>
        <translation>修改命名</translation>
    </message>
    <message>
        <source>Explanation: When renaming a file name, ensure it is within 225 bytes </source>
        <translation type="vanished">说明：用户重命名文件名，保证在225字节以内，去</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="95"/>
        <source>Skip</source>
        <translation>跳过</translation>
    </message>
    <message>
        <source>Skip All</source>
        <translation type="vanished">全部跳过</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="98"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <source>Please enter a new name</source>
        <translation type="vanished">请输入文件名</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="229"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>MainProgressBar</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="399"/>
        <source>File operation</source>
        <translation>文件操作</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="441"/>
        <source>starting ...</source>
        <translation>正在开始 ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="417"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="546"/>
        <source>cancel all file operations</source>
        <translation>取消所有文件操作</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="407"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="415"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="418"/>
        <source>Are you sure to cancel all file operations?</source>
        <translation>你确定要取消所有文件操作?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="547"/>
        <source>Are you sure want to cancel all file operations</source>
        <translation>你确定要取消所有文件操作?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="420"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="549"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="421"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="550"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="582"/>
        <source>continue</source>
        <translation>继续</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="584"/>
        <source>pause</source>
        <translation>暂停</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="664"/>
        <source>canceling ...</source>
        <translation>取消中 ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="667"/>
        <source>sync ...</source>
        <translation>正在同步...</translation>
    </message>
</context>
<context>
    <name>MessageDialog</name>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1766"/>
        <source>Peony</source>
        <translation>文件管理器</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1793"/>
        <source>Forcibly pulling out the device may cause data
 loss or device exceptions!</source>
        <translation>强制拔出设备可能会导致数据丢失或设备异常！</translation>
    </message>
</context>
<context>
    <name>OtherButton</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="768"/>
        <source>Other queue</source>
        <translation>其它队列</translation>
    </message>
</context>
<context>
    <name>Peony::AdvanceSearchBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="55"/>
        <source>Key Words</source>
        <translation>关键词</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="58"/>
        <source>input key words...</source>
        <translation>输入关键词...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="59"/>
        <source>Search Location</source>
        <translation>搜索路径</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="61"/>
        <source>choose search path...</source>
        <translation>选择要搜索的位置...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="68"/>
        <source>browse</source>
        <translation>浏览</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="69"/>
        <source>File Type</source>
        <translation>文件类型</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="71"/>
        <source>Choose File Type</source>
        <translation>选择文件类型</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="76"/>
        <source>Modify Time</source>
        <translation>修改时间</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="78"/>
        <source>Choose Modify Time</source>
        <translation>选择修改时间</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="83"/>
        <source>File Size</source>
        <translation>文件大小</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="85"/>
        <source>Choose file size</source>
        <translation>选择文件大小</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="90"/>
        <source>show hidden file</source>
        <translation>显示隐藏文件</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="91"/>
        <source>go back</source>
        <translation>后退</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="92"/>
        <source>hidden advance search page</source>
        <translation>隐藏高级搜索界面</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="94"/>
        <source>file name</source>
        <translation>文件名</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="95"/>
        <source>content</source>
        <translation>内容</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="100"/>
        <source>search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="101"/>
        <source>start search</source>
        <translation>开始搜索</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="174"/>
        <source>Select path</source>
        <translation>选择路径</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="193"/>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="202"/>
        <source>Operate Tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="194"/>
        <source>Have no key words or search location!</source>
        <translation>没有关键字或路径!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="203"/>
        <source>Search file name or content at least choose one!</source>
        <translation>搜索文件名或者内容请至少指定一个！</translation>
    </message>
    <message>
        <source>Search content or file name at least choose one!</source>
        <translation type="vanished">搜索文件名或者内容请至少指定一个！</translation>
    </message>
    <message>
        <source>all</source>
        <translation type="vanished">全部</translation>
    </message>
    <message>
        <source>file folder</source>
        <translation type="vanished">文件夹</translation>
    </message>
    <message>
        <source>image</source>
        <translation type="vanished">图片</translation>
    </message>
    <message>
        <source>video</source>
        <translation type="vanished">视频</translation>
    </message>
    <message>
        <source>text file</source>
        <translation type="vanished">文本</translation>
    </message>
    <message>
        <source>audio</source>
        <translation type="vanished">音频</translation>
    </message>
    <message>
        <source>others</source>
        <translation type="vanished">其它</translation>
    </message>
    <message>
        <source>wps file</source>
        <translation type="vanished">WPS文件</translation>
    </message>
    <message>
        <source>today</source>
        <translation type="vanished">今天</translation>
    </message>
    <message>
        <source>this week</source>
        <translation type="vanished">本周</translation>
    </message>
    <message>
        <source>this month</source>
        <translation type="vanished">本月</translation>
    </message>
    <message>
        <source>this year</source>
        <translation type="vanished">今年</translation>
    </message>
    <message>
        <source>year ago</source>
        <translation type="vanished">去年</translation>
    </message>
    <message>
        <source>tiny(0-16K)</source>
        <translation type="vanished">极小(0-16K)</translation>
    </message>
    <message>
        <source>small(16k-1M)</source>
        <translation type="vanished">较小(16k-1M)</translation>
    </message>
    <message>
        <source>medium(1M-100M)</source>
        <translation type="vanished">中等(1M-100M)</translation>
    </message>
    <message>
        <source>big(100M-1G)</source>
        <translation type="vanished">较大(100M-1G)</translation>
    </message>
    <message>
        <source>large(&gt;1G)</source>
        <translation type="vanished">极大(&gt;1G)</translation>
    </message>
</context>
<context>
    <name>Peony::AdvancedLocationBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/advanced-location-bar.cpp" line="187"/>
        <source>Search Content...</source>
        <translation>搜索内容</translation>
    </message>
</context>
<context>
    <name>Peony::AdvancedPermissionsPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="654"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="872"/>
        <source>Permission refinement settings</source>
        <translation>权限细化设置</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="767"/>
        <source>Permission refinement settings tip</source>
        <translation>权限细化设置提示</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="767"/>
        <source>Setting ACL permissions will result in a change in the user group permissions for basic permissions. Do you need to continue setting ACL permissions?</source>
        <translation>设置ACL权限会导致基本权限的用户组权限改变，是否需要继续设置ACL权限？</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="799"/>
        <source>User</source>
        <translation>用户</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="799"/>
        <source>Read</source>
        <translation>可读</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="799"/>
        <source>Write</source>
        <translation>可写</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="799"/>
        <source>Executable</source>
        <translation>可执行</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="884"/>
        <source>delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="886"/>
        <source>Inherit permission</source>
        <translation>继承权限</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="901"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="918"/>
        <source>Apply</source>
        <translation>应用</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="919"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>Peony::AllFileLaunchDialog</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="341"/>
        <source>Choose new application</source>
        <translation>选择一个新的应用</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="343"/>
        <source>Choose an Application to open this file</source>
        <translation>选择一个应用打开这个文件</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="350"/>
        <source>apply now</source>
        <translation>立即应用</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="356"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="357"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>Peony::AudioPlayManager</name>
    <message>
        <source>Operation file Warning</source>
        <translation type="vanished">文件操作警告</translation>
    </message>
</context>
<context>
    <name>Peony::BasicPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="969"/>
        <source>Choose a custom icon</source>
        <translation>选择自定义图标</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="401"/>
        <source>Type:</source>
        <translation>类型：</translation>
    </message>
    <message>
        <source>Display Name:</source>
        <translation type="vanished">名称：</translation>
    </message>
    <message>
        <source>Location:</source>
        <translation type="vanished">路径：</translation>
    </message>
    <message>
        <source>Overview:</source>
        <translation type="vanished">概览：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="244"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="245"/>
        <source>Change</source>
        <translation>更改</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="299"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="300"/>
        <source>Location</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="343"/>
        <source>move</source>
        <translation>移动</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="407"/>
        <source>symbolLink</source>
        <translation>快捷方式</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="411"/>
        <source>Folder</source>
        <translation>文件夹</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="418"/>
        <source>Include:</source>
        <translation>包含：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="422"/>
        <source>Open with:</source>
        <translation>打开方式：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="428"/>
        <source>Description:</source>
        <translation>描述：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="440"/>
        <source>Select multiple files</source>
        <translation>选中多个文件</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="445"/>
        <source>Size:</source>
        <translation>文件大小：</translation>
    </message>
    <message>
        <source>Total size:</source>
        <translation type="vanished">实际大小：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="446"/>
        <source>Space Useage:</source>
        <translation>占用空间：</translation>
    </message>
    <message>
        <source>yyyy-MM-dd, HH:mm:ss</source>
        <translation type="vanished">yyyy年MM月dd日, HH:mm:ss</translation>
    </message>
    <message>
        <source>yyyy-MM-dd, hh:mm:ss AP</source>
        <translation type="vanished">yyyy年MM月dd日, hh:mm:ss AP</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="488"/>
        <source>Time Created:</source>
        <translation>创建时间：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="180"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="492"/>
        <source>Time Modified:</source>
        <translation>修改时间：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="493"/>
        <source>Time Access:</source>
        <translation>访问时间：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="517"/>
        <source>Readonly</source>
        <translation>只读</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="518"/>
        <source>Hidden</source>
        <translation>隐藏</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="540"/>
        <source>Property:</source>
        <translation>属性：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="629"/>
        <source>usershare</source>
        <translation>本机共享</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="760"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1023"/>
        <source>%1 (%2 Bytes)</source>
        <translation>%1 (%2 字节)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="797"/>
        <source>Choose a new folder:</source>
        <translation>选择一个新的文件夹：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="804"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="809"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="804"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="809"/>
        <source>cannot move a folder to itself !</source>
        <translation>不能移动一个文件夹到它内部！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="977"/>
        <source>Please select a image that is smaller than 1MB.</source>
        <translation>请重新选择小于1MB的图片</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1016"/>
        <source>%1 Bytes</source>
        <translation>%1 字节</translation>
    </message>
    <message>
        <source>%1 KB (%2 Bytes)</source>
        <translation type="vanished">%1 KB (%2 字节)</translation>
    </message>
    <message>
        <source>%1 MB (%2 Bytes)</source>
        <translation type="vanished">%1 MB (%2 字节)</translation>
    </message>
    <message>
        <source>%1 GB (%2 Bytes)</source>
        <translation type="vanished">%1 GB (%2 字节)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1032"/>
        <source>%1 files, %2 folders</source>
        <translation>%1 个文件, %2 个文件夹</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1137"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1139"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1144"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1146"/>
        <source>Can&apos;t get remote file information</source>
        <translation>未能获取远程文件信息</translation>
    </message>
    <message>
        <source>%1 files (include root files), %2 hidden</source>
        <translation type="vanished">共%1个文件（包括顶层目录），有%2个隐藏文件</translation>
    </message>
    <message>
        <source>%1 total</source>
        <translation type="vanished">共%1</translation>
    </message>
</context>
<context>
    <name>Peony::ComputerPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="102"/>
        <source>CPU Name:</source>
        <translation>处理器：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="103"/>
        <source>CPU Core:</source>
        <translation>核心数：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="104"/>
        <source>Memory Size:</source>
        <translation>内存：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="116"/>
        <source>User Name: </source>
        <translation>用户名：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="117"/>
        <source>Desktop: </source>
        <translation>桌面环境：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="125"/>
        <source>You should mount this volume first</source>
        <translation>你需要挂载该卷才能查看信息</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="142"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="240"/>
        <source>Name: </source>
        <translation>分区名：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="142"/>
        <source>File System</source>
        <translation>文件系统</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="142"/>
        <source>Data</source>
        <translation>数据盘</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="143"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="245"/>
        <source>Total Space: </source>
        <translation>总容量：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="144"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="246"/>
        <source>Used Space: </source>
        <translation>使用空间：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="145"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="247"/>
        <source>Free Space: </source>
        <translation>剩余空间：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="146"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="249"/>
        <source>Type: </source>
        <translation>文件系统：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="262"/>
        <source>Kylin Burner</source>
        <translation>刻录</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="268"/>
        <source>Open with: 	</source>
        <translation>打开：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="275"/>
        <source>Unknown</source>
        <translation>未知的分区</translation>
    </message>
</context>
<context>
    <name>Peony::ConnectServerDialog</name>
    <message>
        <source>connect to server</source>
        <translation type="vanished">连接服务器</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="143"/>
        <source>Connect to server</source>
        <translation>连接服务器</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="168"/>
        <source>Ip</source>
        <translation>服务器</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="170"/>
        <source>Port</source>
        <translation>端口</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="171"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="215"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="216"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="217"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="296"/>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="387"/>
        <source>Ip input error, please re-enter!</source>
        <translation>ip输入错误，请重新输入！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="300"/>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="391"/>
        <source>Port input error, please re-enter!</source>
        <translation>端口输入错误，请重新输入！</translation>
    </message>
    <message>
        <source>ip</source>
        <translation type="vanished">服务器</translation>
    </message>
    <message>
        <source>port</source>
        <translation type="vanished">端口</translation>
    </message>
    <message>
        <source>type</source>
        <translation type="vanished">类型</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="202"/>
        <source>Personal Collection server:</source>
        <translation>个人收藏服务器</translation>
    </message>
    <message>
        <source>add</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>delete</source>
        <translatorcomment>连接</translatorcomment>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <source>connect</source>
        <translation type="vanished">连接</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="296"/>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="300"/>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="387"/>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="391"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <source>ip input error, please re-enter!</source>
        <translation type="vanished">ip输入错误，请重新输入！</translation>
    </message>
    <message>
        <source>port input error, please re-enter!</source>
        <translation type="vanished">端口输入错误，请重新输入！</translation>
    </message>
</context>
<context>
    <name>Peony::ConnectServerLogin</name>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="484"/>
        <source>The login user</source>
        <translation>登录身份</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="493"/>
        <source>Please enter the %1&apos;s user name and password of the server.</source>
        <translation>请输入服务器 %1 的用户名和密码。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="500"/>
        <source>User&apos;s identity</source>
        <translation>连接身份</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="501"/>
        <source>Guest</source>
        <translation>游客（匿名登录）</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="517"/>
        <source>Name</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="518"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="539"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="540"/>
        <source>OK</source>
        <translation>连接</translation>
    </message>
    <message>
        <source>guest</source>
        <translation type="vanished">游客（匿名登录）</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="502"/>
        <source>Registered users</source>
        <translation>注册用户</translation>
    </message>
    <message>
        <source>name</source>
        <translation type="vanished">用户名</translation>
    </message>
    <message>
        <source>password</source>
        <translation type="vanished">密码</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="519"/>
        <source>Remember the password</source>
        <translation>记住密码</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>ok</source>
        <translation type="vanished">连接</translation>
    </message>
</context>
<context>
    <name>Peony::CreateLinkInternalPlugin</name>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="124"/>
        <source>Create Link to Desktop</source>
        <translation>发送到桌面快捷方式</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="150"/>
        <source>Create Link to...</source>
        <translation>发送快捷方式到...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="153"/>
        <source>Choose a Directory to Create Link</source>
        <translation>选择创建链接的目录</translation>
    </message>
    <message>
        <source>Peony-Qt Create Link Extension</source>
        <translation type="vanished">创建链接</translation>
    </message>
    <message>
        <source>Create Link Menu Extension.</source>
        <translation type="vanished">创建链接.</translation>
    </message>
</context>
<context>
    <name>Peony::CreateSharedFileLinkMenuPlugin</name>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="242"/>
        <source>Create Link to Desktop</source>
        <translation>发送到桌面快捷方式</translation>
    </message>
</context>
<context>
    <name>Peony::CreateTemplateOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="62"/>
        <source>NewFile</source>
        <translation>新建文件</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="78"/>
        <source>Create file</source>
        <translation>文件创建</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="90"/>
        <source>NewFolder</source>
        <translation>新建文件夹</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="109"/>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="142"/>
        <source>Create file error</source>
        <translation>创建文件错误</translation>
    </message>
</context>
<context>
    <name>Peony::CustomErrorHandler</name>
    <message>
        <location filename="../../libpeony-qt/custom-error-handler.cpp" line="40"/>
        <source>Is Error Handled?</source>
        <translation>错误是否已处理？</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/custom-error-handler.cpp" line="45"/>
        <source>Error not be handled correctly</source>
        <translation>错误未被正确处理</translation>
    </message>
</context>
<context>
    <name>Peony::DefaultOpenWithWidget</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="400"/>
        <source>No default app</source>
        <translation>没有设置默认打开方式</translation>
    </message>
</context>
<context>
    <name>Peony::DefaultPreviewPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="76"/>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="217"/>
        <source>Select the file you want to preview...</source>
        <translation>选择你想要预览的文件...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="208"/>
        <source>Can not preview this file.</source>
        <translation>不能预览该文件.</translation>
    </message>
    <message>
        <source>Can not preivew this file.</source>
        <translation type="vanished">不能预览该文件</translation>
    </message>
</context>
<context>
    <name>Peony::DefaultPreviewPageFactory</name>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page-factory.h" line="50"/>
        <source>Default Preview</source>
        <translation>详细信息</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page-factory.h" line="53"/>
        <source>This is the Default Preview of peony-qt</source>
        <translation>显示文件的详细信息</translation>
    </message>
</context>
<context>
    <name>Peony::DetailsPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="180"/>
        <source>Name:</source>
        <translation>名称：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="183"/>
        <source>File type:</source>
        <translation>文件类型：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="199"/>
        <source>Location:</source>
        <translation>路径：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="213"/>
        <source>yyyy-MM-dd, HH:mm:ss</source>
        <translation>yyyy年MM月dd日, HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="205"/>
        <source>Create time:</source>
        <translation>创建时间：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="210"/>
        <source>Modify time:</source>
        <translation>修改时间：</translation>
    </message>
    <message>
        <source>yyyy-MM-dd, hh:mm:ss AP</source>
        <translation type="vanished">yyyy年MM月dd日, hh:mm:ss AP</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="234"/>
        <source>File size:</source>
        <translation>文件大小：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="241"/>
        <source>Width:</source>
        <translation>宽度：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="244"/>
        <source>Height:</source>
        <translation>高度：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="252"/>
        <source>Owner</source>
        <translation>拥有者</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="253"/>
        <source>Owner:</source>
        <translation>拥有者：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="255"/>
        <source>Computer</source>
        <translation>计算机</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="256"/>
        <source>Computer:</source>
        <translation>计算机：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="311"/>
        <source>%1 (this computer)</source>
        <translation>%1 (这台电脑)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="318"/>
        <source>Unknown</source>
        <translation>未知的分区</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="348"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="349"/>
        <source>Can&apos;t get remote file information</source>
        <translation>不能获取远程文件信息</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="358"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="359"/>
        <source>%1 px</source>
        <translation>%1 像素</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::IconView</name>
    <message>
        <source>Icon View</source>
        <translation type="vanished">图标视图</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/icon-view/icon-view.cpp" line="316"/>
        <source>warn</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/icon-view/icon-view.cpp" line="316"/>
        <source>This operation is not supported.</source>
        <translation>不支持此操作。</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::IconView2</name>
    <message>
        <source>Icon View</source>
        <translation type="vanished">图标视图</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::ListView</name>
    <message>
        <source>List View</source>
        <translation type="vanished">列表视图</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/list-view/list-view.cpp" line="565"/>
        <source>warn</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/list-view/list-view.cpp" line="565"/>
        <source>This operation is not supported.</source>
        <translation>不支持此操作。</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::ListView2</name>
    <message>
        <source>List View</source>
        <translation type="vanished">列表视图</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryViewFactoryManager</name>
    <message>
        <source>Icon View</source>
        <translation type="vanished">图标视图</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryViewMenu</name>
    <message>
        <source>Open in &amp;New Window</source>
        <translation type="vanished">在新窗口中打开(&amp;N)</translation>
    </message>
    <message>
        <source>Open in New &amp;Tab</source>
        <translation type="vanished">在新标签页中打开(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="343"/>
        <source>Add to bookmark</source>
        <translation>添加到快速访问</translation>
    </message>
    <message>
        <source>&amp;Open &quot;%1&quot;</source>
        <translation type="vanished">打开“%1”(&amp;O)</translation>
    </message>
    <message>
        <source>Open &quot;%1&quot; in &amp;New Window</source>
        <translation type="vanished">在新窗口中打开“%1”(&amp;N)</translation>
    </message>
    <message>
        <source>Open &quot;%1&quot; in New &amp;Tab</source>
        <translation type="vanished">在新标签页中打开“%1”(&amp;T)</translation>
    </message>
    <message>
        <source>Open &quot;%1&quot; with...</source>
        <translation type="vanished">选用其它应用打开“%1”...</translation>
    </message>
    <message>
        <source>&amp;More applications...</source>
        <translation type="vanished">更多应用...(&amp;M)</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation type="vanished">打开(&amp;O)</translation>
    </message>
    <message>
        <source>Open &amp;with...</source>
        <translation type="vanished">打开方式(&amp;W)...</translation>
    </message>
    <message>
        <source>&amp;Open %1 selected files</source>
        <translation type="vanished">打开%1个选中文件(&amp;O)</translation>
    </message>
    <message>
        <source>&amp;New...</source>
        <translation type="vanished">新建...(&amp;N)</translation>
    </message>
    <message>
        <source>Empty &amp;File</source>
        <translation type="vanished">空文件(&amp;E)</translation>
    </message>
    <message>
        <source>&amp;Folder</source>
        <translation type="vanished">文件夹(&amp;F)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="655"/>
        <source>New Folder</source>
        <translation>新建文件夹</translation>
    </message>
    <message>
        <source>Icon View</source>
        <translation type="vanished">图标视图</translation>
    </message>
    <message>
        <source>List View</source>
        <translation type="vanished">列表视图</translation>
    </message>
    <message>
        <source>View Type...</source>
        <translation type="vanished">视图类型...</translation>
    </message>
    <message>
        <source>Sort By...</source>
        <translation type="vanished">排序类型...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="707"/>
        <source>Name</source>
        <translation>文件名称</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="709"/>
        <source>File Type</source>
        <translation>文件类型</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="710"/>
        <source>File Size</source>
        <translation>文件大小</translation>
    </message>
    <message>
        <source>New...</source>
        <translation type="vanished">新建...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="307"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="396"/>
        <source>Open in New Window</source>
        <translation>在新窗口中打开</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="316"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="405"/>
        <source>Open in New Tab</source>
        <translation>在新标签页中打开</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="363"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="416"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="480"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="374"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="432"/>
        <source>Open with...</source>
        <translation>打开方式</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="389"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="469"/>
        <source>More applications...</source>
        <translation>更多应用</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="489"/>
        <source>Open %1 selected files</source>
        <translation>打开%1个选中文件</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="538"/>
        <source>New</source>
        <translation>新建</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="639"/>
        <source>Empty File</source>
        <translation>空文本</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="651"/>
        <source>Folder</source>
        <translation>文件夹</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="681"/>
        <source>View Type</source>
        <translation>视图类型</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="701"/>
        <source>Sort By</source>
        <translation>排序类型</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="708"/>
        <source>Modified Date</source>
        <translation>修改日期</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="711"/>
        <source>Orignal Path</source>
        <translation>原始路径</translation>
    </message>
    <message>
        <source>Sort Order...</source>
        <translation type="vanished">排序顺序...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="744"/>
        <source>Ascending Order</source>
        <translation>升序</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="743"/>
        <source>Descending Order</source>
        <translation>降序</translation>
    </message>
    <message>
        <source>Sort Preferences...</source>
        <translation type="vanished">排序偏好...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="762"/>
        <source>Folder First</source>
        <translation>文件夹优先</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="771"/>
        <source>Chinese First</source>
        <translation>中文优先</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="780"/>
        <source>Show Hidden</source>
        <translation>显示隐藏文件</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="815"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="823"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="985"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1228"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1288"/>
        <source>File:&quot;%1&quot; is not exist, did you moved or deleted it?</source>
        <translation>文件 ”%s“ 不存在，你是否已经删除或移动到别处？</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1323"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1333"/>
        <source>Peony-Qt filesafe menu Extension</source>
        <translation>文件保护箱扩展</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1323"/>
        <source>Peony File Labels Menu Extension</source>
        <translation>文件标记</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="vanished">复制(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="853"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1233"/>
        <source>Cut</source>
        <translation>剪切</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="880"/>
        <source>Delete to trash</source>
        <translation>删除到回收站</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="939"/>
        <source>Paste</source>
        <translation>粘贴</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="998"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1009"/>
        <source>Select All</source>
        <translation>全选</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1053"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1090"/>
        <source>Properties</source>
        <translation>属性</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1127"/>
        <source>format</source>
        <translation>格式化</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1181"/>
        <source>Restore</source>
        <translation>还原</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="892"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="979"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1208"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="736"/>
        <source>Sort Order</source>
        <translation>排序顺序</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="757"/>
        <source>Sort Preferences</source>
        <translation>排序偏好</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1287"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <source>File:&quot;%1 is not exist, did you moved or deleted it?</source>
        <translation type="vanished">文件：&quot;%1&quot; 不存在，您是否已经移动或者删除了它？</translation>
    </message>
    <message>
        <source>File original path not exist, are you deleted or moved it?</source>
        <translation type="vanished">文件原始路径未找到，您是否已经移动或删除了它？</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="vanished">剪切(&amp;T)</translation>
    </message>
    <message>
        <source>&amp;Delete to trash</source>
        <translation type="vanished">删除到回收站(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="895"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="906"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="914"/>
        <source>Delete forever</source>
        <translation>永久删除</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="922"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <source>Select &amp;All</source>
        <translation type="vanished">全选(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1018"/>
        <source>Reverse Select</source>
        <translation>反选</translation>
    </message>
    <message>
        <source>P&amp;roperties</source>
        <translation type="vanished">属性(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">删除(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="vanished">重命名(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="vanished">粘贴(&amp;P)</translation>
    </message>
    <message>
        <source>&amp;Refresh</source>
        <translation type="vanished">刷新(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Properties</source>
        <translation type="vanished">属性(&amp;P)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1154"/>
        <source>&amp;Clean the Trash</source>
        <translation>清空回收站(&amp;C)</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <source>Delete Permanently</source>
        <translation type="vanished">永久删除</translation>
    </message>
    <message>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="vanished">您确定要删除这些文件吗？一旦开始删除，这些文件将不可再恢复。</translation>
    </message>
    <message>
        <source>&amp;Restore</source>
        <translation type="vanished">还原(&amp;R)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1241"/>
        <source>Clean All</source>
        <translation>清空全部</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1258"/>
        <source>Open Parent Folder in New Window</source>
        <translation>在新窗口中打开文件所在目录</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryViewWidget</name>
    <message>
        <source>Directory View</source>
        <translation type="vanished">文件视图</translation>
    </message>
</context>
<context>
    <name>Peony::ExtensionsManagerWidget</name>
    <message>
        <location filename="../../libpeony-qt/extensions-manager-widget.cpp" line="42"/>
        <source>Extensions Manager</source>
        <translation>插件管理设置</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/extensions-manager-widget.cpp" line="46"/>
        <source>Available extensions</source>
        <translation>可使用的插件</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/extensions-manager-widget.cpp" line="48"/>
        <source>Ok</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/extensions-manager-widget.cpp" line="49"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>Peony::FMWindow</name>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="92"/>
        <source>File Manager</source>
        <translation>文件管理器</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="170"/>
        <source>advanced search</source>
        <translation>高级搜索</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="173"/>
        <source>clear record</source>
        <translation>清空历史</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="278"/>
        <source>Loaing... Press Esc to stop a loading.</source>
        <translation>正在加载...按下Esc键取消.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="394"/>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation>作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <source>Author: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <source>Ctrl+H</source>
        <comment>Show|Hidden</comment>
        <translation type="vanished">Ctrl+H</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="323"/>
        <source>Undo</source>
        <translation>撤销</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="330"/>
        <source>Redo</source>
        <translation>重做</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="393"/>
        <source>Peony Qt</source>
        <translation>文件管理器</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="447"/>
        <source>New Folder</source>
        <translation>新建文件夹</translation>
    </message>
</context>
<context>
    <name>Peony::FileBatchRenameOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-batch-rename-operation.cpp" line="53"/>
        <source>File Rename error</source>
        <translation>文件重命名错误</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-batch-rename-operation.cpp" line="54"/>
        <source>Invalid file name %1%2%3 .</source>
        <translation>非法的文件名%1%2%3.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-batch-rename-operation.cpp" line="69"/>
        <source>File Rename warning</source>
        <translation>文件重命名警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-batch-rename-operation.cpp" line="70"/>
        <source>Are you sure to hidden these files?</source>
        <translation>确定要隐藏这些文件吗？</translation>
    </message>
    <message>
        <source>The file %1%2%3 will be hidden when you refresh or change directory!</source>
        <translation type="vanished">文件 %1%2%3 在刷新或者切换路径后将会被隐藏!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-batch-rename-operation.cpp" line="184"/>
        <location filename="../../libpeony-qt/file-operation/file-batch-rename-operation.cpp" line="219"/>
        <source>Rename file error</source>
        <translation>重命名文件错误</translation>
    </message>
</context>
<context>
    <name>Peony::FileCopy</name>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="170"/>
        <location filename="../../libpeony-qt/file-copy.cpp" line="178"/>
        <location filename="../../libpeony-qt/file-copy.cpp" line="198"/>
        <source>Error in source or destination file path!</source>
        <translation>源地址或者目标文件路径出错！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="187"/>
        <source>Error when copy file: %1, can not copy special files, skip this file and continue?</source>
        <translation>拷贝文件: %1 时出错，不能拷贝特殊类型文件，是否跳过此文件并继续？</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="210"/>
        <source>Can not copy %1, file doesn&apos;t exist. Has the file been renamed or moved?</source>
        <translation>无法拷贝 %1, 文件不存在. 是否被重命名或移动?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="232"/>
        <source>The dest file &quot;%1&quot; has existed!</source>
        <translation>目标文件 %1 已经存在！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="260"/>
        <source>Vfat/FAT32 file systems do not support a single file that occupies more than 4 GB space!</source>
        <translation>vfat/fat32文件系统不支持单文件所占空间大于4g</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="286"/>
        <source>Error writing to file: Input/output error</source>
        <translation>写入文件出错: 输入/输出错误</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="471"/>
        <source>Failed to create %1. Please ensure if it is in root directory, or if the device supports gphoto2 protocol correctly.</source>
        <translation>创建文件 %1 失败，请确认是否是在根目录操作，或者设备是否正确支持gphoto2协议。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="477"/>
        <source>Failed to create %1.</source>
        <translation>创建文件 %1 失败。</translation>
    </message>
    <message>
        <source>Error opening source or destination file!</source>
        <translation type="vanished">打开源文件或者目标文件出错！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="388"/>
        <source>Please check whether the device has been removed!</source>
        <translation>请确认设备是否被移除!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="390"/>
        <source>Write file error: There is no avaliable disk space for device!</source>
        <translation>写入文件错误: 设备上没有足够可用空间!</translation>
    </message>
    <message>
        <source>Please confirm that the device controls are insufficient!</source>
        <translation type="vanished">请确认设备空间是否足够!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="466"/>
        <source>File opening failure</source>
        <translation>打开文件失败</translation>
    </message>
    <message>
        <source>Reading and Writing files are inconsistent!</source>
        <translation type="vanished">读和写文件不一致！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="277"/>
        <location filename="../../libpeony-qt/file-copy.cpp" line="405"/>
        <source>operation cancel</source>
        <translation>操作取消</translation>
    </message>
</context>
<context>
    <name>Peony::FileCopyOperation</name>
    <message>
        <source>File copy</source>
        <translation type="vanished">文件复制</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="210"/>
        <source>Create folder %1 failed: %2</source>
        <translation>创建目录%1失败: %2</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="214"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="584"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="1041"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="1102"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="1279"/>
        <source>File copy error</source>
        <translation>文件复制错误</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="228"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="255"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="609"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="633"/>
        <source>The file name exceeds the limit</source>
        <translation>文件名长度超出限制</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="592"/>
        <source>Cannot opening file, permission denied!</source>
        <translation>无法打开文件，权限不够！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="594"/>
        <source>File:%1 was not found.</source>
        <translation>未找到文件：%1.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="1028"/>
        <source>File System</source>
        <translation>文件系统</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="1030"/>
        <source>Data</source>
        <translation>数据盘</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="1036"/>
        <source>%1 no space left on device. Copy file size: %2 GB, Space needed: %3 GB.</source>
        <translation>%1 设备空间不足。拷贝文件大小: %2 GB, 需要空间：%3 GB。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="1147"/>
        <source>Link file error</source>
        <translation>创建文件链接失败</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="1281"/>
        <source>Burning does not support replacement</source>
        <translation>刻录暂不支持替换</translation>
    </message>
    <message>
        <source>Burn failed</source>
        <translation type="vanished">刻录失败</translation>
    </message>
</context>
<context>
    <name>Peony::FileDeleteOperation</name>
    <message>
        <source>File delete</source>
        <translation type="vanished">文件删除</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="84"/>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="110"/>
        <source>File delete error</source>
        <translation>文件删除错误</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="147"/>
        <source>Delete file error</source>
        <translation>删除错误</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="150"/>
        <source>Invalid Operation! Can not delete &quot;%1&quot;.</source>
        <translation>非法的操作! 不能删除 &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>Peony::FileEnumerator</name>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="575"/>
        <source>The password dialog box is canceled</source>
        <translation>密码对话框被取消</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="577"/>
        <source>Message recipient disconnected from message bus without replying!</source>
        <translation>消息接收者在没有回复的情况下与消息总线断开连接!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="579"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <source>Did not find target path, do you move or deleted it?</source>
        <translation type="vanished">未找到目标路径，您是否已经移动或删除了它？</translation>
    </message>
</context>
<context>
    <name>Peony::FileInfo</name>
    <message>
        <location filename="../../libpeony-qt/file-info.cpp" line="279"/>
        <source>data</source>
        <translation>数据盘</translation>
    </message>
</context>
<context>
    <name>Peony::FileInfoJob</name>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="275"/>
        <source>Trash</source>
        <translation>回收站</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="277"/>
        <source>Computer</source>
        <translation>计算机</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="279"/>
        <source>Network</source>
        <translation>网络</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="281"/>
        <source>Recent</source>
        <translation>最近</translation>
    </message>
</context>
<context>
    <name>Peony::FileInformationLabel</name>
    <message>
        <source>File location:</source>
        <translation type="vanished">文件位置：</translation>
    </message>
    <message>
        <source>File size:</source>
        <translation type="vanished">文件大小：</translation>
    </message>
    <message>
        <source>Modify time:</source>
        <translation type="vanished">修改时间：</translation>
    </message>
</context>
<context>
    <name>Peony::FileInfosJob</name>
    <message>
        <location filename="../../libpeony-qt/file-infos-job.cpp" line="162"/>
        <source>Trash</source>
        <translation>回收站</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-infos-job.cpp" line="164"/>
        <source>Computer</source>
        <translation>计算机</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-infos-job.cpp" line="166"/>
        <source>Network</source>
        <translation>网络</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-infos-job.cpp" line="168"/>
        <source>Recent</source>
        <translation>最近</translation>
    </message>
</context>
<context>
    <name>Peony::FileItem</name>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="246"/>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="306"/>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="318"/>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="326"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="293"/>
        <source>Open Link failed</source>
        <translation>打开链接失败</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="294"/>
        <source>File not exist, do you want to delete the link file?</source>
        <translation>目标文件夹不存在，是否删除该无效快捷方式？</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="307"/>
        <source>Can not open path &quot;%1&quot;，permission denied.</source>
        <translation>打开路径 &quot;%1&quot; 失败，权限被拒绝。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="317"/>
        <source>Can not find path &quot;%1&quot;，are you moved or renamed it?</source>
        <translation>未找到路径：&quot;%1&quot;，您是否已经移动或者重命名？</translation>
    </message>
    <message>
        <source>Can not find path &quot;%1&quot; .</source>
        <translation type="vanished">找不到路径: &quot;%1&quot; 。</translation>
    </message>
</context>
<context>
    <name>Peony::FileItemModel</name>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="356"/>
        <source>child(ren)</source>
        <translation>个子项</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="343"/>
        <source>Symbol Link, </source>
        <translation>快捷方式，</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="402"/>
        <source>File Name</source>
        <translation>文件名称</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="406"/>
        <source>Delete Date</source>
        <translation>删除日期</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="408"/>
        <source>Create Date</source>
        <translation>创建时间</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="415"/>
        <source>File Size</source>
        <translation>文件大小</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="417"/>
        <source>Original Path</source>
        <translation>原始路径</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="413"/>
        <source>File Type</source>
        <translation>文件类型</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="411"/>
        <source>Modified Date</source>
        <translation>修改日期</translation>
    </message>
</context>
<context>
    <name>Peony::FileLabelInternalMenuPlugin</name>
    <message>
        <source>Add File Label...</source>
        <translation type="vanished">添加标记...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="190"/>
        <source>Add File Label</source>
        <translation>添加标记</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="213"/>
        <source>Delete All Label</source>
        <translation>删除所有标记</translation>
    </message>
    <message>
        <source>Peony File Labels Menu Extension</source>
        <translation type="vanished">文件标记</translation>
    </message>
    <message>
        <source>Tag a File with Menu.</source>
        <translation type="vanished">菜单中增加标记功能.</translation>
    </message>
</context>
<context>
    <name>Peony::FileLauchDialog</name>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="47"/>
        <source>Applications</source>
        <translation>应用程序</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="48"/>
        <source>Choose an Application to open this file</source>
        <translation>选择一个应用打开这个文件</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="56"/>
        <source>Set as Default</source>
        <translation>设为默认</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="64"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="65"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>Peony::FileLaunchAction</name>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="144"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="251"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="447"/>
        <source>Execute Directly</source>
        <translation>直接运行</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="145"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="252"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="448"/>
        <source>Execute in Terminal</source>
        <translation>在终端运行</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="148"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="256"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="452"/>
        <source>Detected launching an executable file %1, you want?</source>
        <translation>正在打开一个可执行文件%1, 你希望?</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="166"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="290"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="486"/>
        <source>Open Failed</source>
        <translation>无法打开</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="166"/>
        <source>Can not open %1, file not exist, is it deleted?</source>
        <translation>无法打开%1，文件不存在，请确认是否已被删除？</translation>
    </message>
    <message>
        <source>File not exist, is it deleted or moved to other path?</source>
        <translation type="vanished">文件不存在，您是否已将其删除或挪动位置？</translation>
    </message>
    <message>
        <source>Can not open %1</source>
        <translation type="vanished">不能打开%1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="250"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="446"/>
        <source>By Default App</source>
        <translation>使用默认打开方式</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="255"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="451"/>
        <source>Launch Options</source>
        <translation>执行选项</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="279"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="475"/>
        <source>Open Link failed</source>
        <translation>打开链接失败</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="280"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="476"/>
        <source>File not exist, do you want to delete the link file?</source>
        <translation>目标文件不存在，您需要删除该链接吗？</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="291"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="487"/>
        <source>Can not open %1, Please confirm you have the right authority.</source>
        <translation>无法打开%1，请确认您有正确的打开权限。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="295"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="492"/>
        <source>Open App failed</source>
        <translation>快捷方式存在问题</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="296"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="493"/>
        <source>The linked app is changed or uninstalled, so it can not work correctly. 
Do you want to delete the link file?</source>
        <translation>该链接指向的应用已经被修改或者卸载，因此该快捷方式无法正常工作。
是否删除该快捷方式？</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="307"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="504"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="308"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="504"/>
        <source>Can not get a default application for opening %1, do you want open it with text format?</source>
        <translation>没有找到默认打开%1的应用, 是否用文本编辑器打开？</translation>
    </message>
</context>
<context>
    <name>Peony::FileLinkOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-link-operation.cpp" line="46"/>
        <location filename="../../libpeony-qt/file-operation/file-link-operation.cpp" line="49"/>
        <source>Symbolic Link</source>
        <translation>快捷方式</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-link-operation.cpp" line="89"/>
        <source>Link file error</source>
        <translation>创建文件链接失败</translation>
    </message>
    <message>
        <source>Link file</source>
        <translation type="vanished">创建文件链接</translation>
    </message>
</context>
<context>
    <name>Peony::FileMoveOperation</name>
    <message>
        <source>Invalid move operation, cannot move a file itself.</source>
        <translation type="vanished">非法的移动操作，不能自移动到自身。</translation>
    </message>
    <message>
        <source>Move file</source>
        <translation type="vanished">文件移动</translation>
    </message>
    <message>
        <source>Create file</source>
        <translation type="vanished">文件创建</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="184"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="399"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="519"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="848"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1753"/>
        <source>Move file error</source>
        <translation>移动文件错误</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="286"/>
        <source>File System</source>
        <translation>文件系统</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="288"/>
        <source>Data</source>
        <translation>数据盘</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="294"/>
        <source>%1 no space left on device. Copy file size: %2 GB, Space needed: %3 GB.</source>
        <translation>%1 设备空间不足。拷贝文件大小: %2 GB, 需要空间：%3 GB。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="299"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1928"/>
        <source>File move error</source>
        <translation>移动文件失败</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="812"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="822"/>
        <source>Invalid move operation, cannot move a file into its sub directories.</source>
        <translation>非法的移动操作，不能移动文件夹到自身路径下。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="866"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="897"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="950"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1219"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1243"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1374"/>
        <source>The file name exceeds the limit</source>
        <translation>文件名长度超出限制</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1196"/>
        <source>Create file error</source>
        <translation>创建文件错误</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1202"/>
        <source>Cannot opening file, permission denied!</source>
        <translation>无法打开文件，权限不够！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1204"/>
        <source>File:%1 was not found.</source>
        <translation>未找到文件：%1.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="813"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1660"/>
        <source>Invalid Operation.</source>
        <translation>非法的操作.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1720"/>
        <source>File delete error</source>
        <translation>文件删除错误</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1798"/>
        <source>Link file error</source>
        <translation>创建文件链接失败</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1930"/>
        <source>Burning does not support replacement</source>
        <translation>刻录暂不支持替换</translation>
    </message>
    <message>
        <source>Burn failed</source>
        <translation type="vanished">刻录失败</translation>
    </message>
    <message>
        <source>File delete</source>
        <translation type="vanished">文件删除</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="820"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1722"/>
        <source>Invalid Operation</source>
        <translation>非法的操作</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationAfterProgressPage</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="362"/>
        <source>&amp;More Details</source>
        <translation>详细信息(&amp;M)</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialog</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="45"/>
        <source>File Operation Error</source>
        <translation>文件操作错误</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="53"/>
        <source>unkwon</source>
        <translation>未知原因</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="54"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="55"/>
        <source>null</source>
        <translation>空</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="57"/>
        <source>Error message:</source>
        <translation>错误信息：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="58"/>
        <source>Source File:</source>
        <translation>源文件：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="59"/>
        <source>Dest File:</source>
        <translation>目标文件：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="63"/>
        <source>Ignore</source>
        <translation>忽略</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="64"/>
        <source>Ignore All</source>
        <translation>全部忽略</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="65"/>
        <source>Overwrite</source>
        <translation>覆盖</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="66"/>
        <source>Overwrite All</source>
        <translation>全部覆盖</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="67"/>
        <source>Backup</source>
        <translation>备份</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="68"/>
        <source>Backup All</source>
        <translation>全部备份</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="69"/>
        <source>&amp;Retry</source>
        <translation>重试(&amp;R)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="70"/>
        <source>&amp;Cancel</source>
        <translation>取消(&amp;C)</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogBase</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog-base.cpp" line="68"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogConflict</name>
    <message>
        <source>This location already contains a file with the same name.</source>
        <translation type="vanished">目标文件夹里已经包含有同名文件</translation>
    </message>
    <message>
        <source>Please select the file to keep</source>
        <translation type="vanished">请选择要保留的文件</translation>
    </message>
    <message>
        <source>This location already contains the file,</source>
        <translation type="vanished">这里已包含此文件</translation>
    </message>
    <message>
        <source>Do you want to override it?</source>
        <translation type="vanished">你确定要覆盖它吗</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="54"/>
        <source>Replace</source>
        <translation>替换</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="63"/>
        <source>Ignore</source>
        <translation>忽略</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="82"/>
        <source>Do the same</source>
        <translation>全部应用</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="105"/>
        <source>&lt;p&gt;This location already contains the file &apos;%1&apos;, Do you want to override it?&lt;/p&gt;</source>
        <translation>&lt;p&gt;此位置已包含名称为“%1”的文件，您确定要替换它吗？&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="111"/>
        <source>Unexpected error from %1 to %2</source>
        <translation>从 %1 至 %2 操作发生异常错误</translation>
    </message>
    <message>
        <source>Then do the same thing in a similar situation</source>
        <translation type="vanished">之后类似情况执行相同操作</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="72"/>
        <source>Backup</source>
        <translation>备份</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogNotSupported</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="320"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="312"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Do the same</source>
        <translation type="vanished">全部应用</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="357"/>
        <source>Make sure the disk is not full or write protected and that the file is not protected</source>
        <translation>请确认磁盘未满或未被写保护而且文件未被使用</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogWarning</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="204"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="213"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="245"/>
        <source>Make sure the disk is not full or write protected and that the file is not protected</source>
        <translation>请确认磁盘未满或未被写保护而且文件未被使用</translation>
    </message>
    <message>
        <source>Please make sure the disk is not full or not is write protected, or file is not being used.</source>
        <translation type="vanished">请确保磁盘未满或未被写保护或未被使用。</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationInfo</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="1059"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="1061"/>
        <source>Symbolic Link</source>
        <translation>快捷方式</translation>
    </message>
    <message>
        <source> - Symbolic Link</source>
        <translation type="vanished">-快捷方式</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationManager</name>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="231"/>
        <source>Warn</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="231"/>
        <source>&apos;%1&apos; is occupied，you cannot operate!</source>
        <translation>“%1”已被占用，您无法进行操作！</translation>
    </message>
    <message>
        <source>No, go to settings</source>
        <translation type="vanished">否，跳转到设置</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="249"/>
        <source>OK</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="253"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="258"/>
        <source>Do you want to put selected %1 item(s) into trash?</source>
        <translation>确认要将选中的 %1 项放入回收站吗？</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="260"/>
        <source>Do not show again</source>
        <translation>不再显示</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="427"/>
        <source>File System</source>
        <translation>文件系统</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="429"/>
        <source>Data</source>
        <translation>数据盘</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="433"/>
        <source>Insufficient storage space</source>
        <translation>存储空间不足</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="436"/>
        <source>%1 no space left on device. Copy file size: %2 GB, Space needed: %3 GB.</source>
        <translation>%1 设备可用空间不足。拷贝文件大小：%2 GB, 需要空间: %3 GB。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="478"/>
        <source>Can&apos;t delete.</source>
        <translation>不能删除</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="479"/>
        <source>You can&apos;t delete a file whenthe file is doing another operation</source>
        <translation>不能删除一个正在进行其它操作的文件</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="600"/>
        <source>File Operation is Busy</source>
        <translation>操作正忙</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="601"/>
        <source>There have been one or more fileoperation(s) executing before. Youroperation will wait for executinguntil it/them done. If you really want to execute file operations parallelly anyway, you can change the default option &quot;Allow Parallel&quot; in option menu.</source>
        <translation>在执行该操作之前有操作未完成, 它需要等待上一个操作完成后再执行. 如果你希望文件操作并行, 你可以更改选项菜单中的&quot;允许操作并行&quot;配置.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="624"/>
        <source>The long name file is saved to %1</source>
        <translation>长文件被保存到 %1</translation>
    </message>
    <message>
        <source>The system cannot hibernate or sleep</source>
        <translation type="vanished">无法进入休眠或睡眠模式</translation>
    </message>
    <message>
        <source>The file operation is in progress.                                         Ensure that the file operation is complete or canceled before hibernating or sleeping</source>
        <translation type="vanished">文件操作进行中,\
进入休眠或者睡眠之前，请先确保文件操作已完成或者取消</translation>
    </message>
    <message>
        <source>There have been one or more fileoperation(s) executing before. Youroperation will wait for executinguntil it/them done.</source>
        <translation type="vanished">在执行该操作之前有操作未完成，它需要等待上一个操作完成后再执行。</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationPreparePage</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="298"/>
        <source>counting:</source>
        <translation>总计：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="299"/>
        <source>state:</source>
        <translation>状态：</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationProgressPage</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="321"/>
        <source>&amp;More Details</source>
        <translation>详细信息(&amp;M)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="332"/>
        <source>From:</source>
        <translation>从：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="333"/>
        <source>To:</source>
        <translation>到：</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationProgressWizard</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="55"/>
        <source>File Manager</source>
        <translation>文件管理器</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="59"/>
        <source>&amp;Cancel</source>
        <translation>取消(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="68"/>
        <source>Preparing...</source>
        <translation>准备中...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="71"/>
        <source>Handling...</source>
        <translation>正在处理...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="74"/>
        <source>Clearing...</source>
        <translation>正在清理...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="77"/>
        <source>Rollbacking...</source>
        <translation>回滚中...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="81"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="94"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="120"/>
        <source>File Operation</source>
        <translation>文件操作</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="95"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="121"/>
        <source>A file operation is running backend...</source>
        <translation>一个文件操作正在后台运行中...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="160"/>
        <source>%1 files, %2</source>
        <translation>%1个文件，共%2</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="260"/>
        <source>%1 done, %2 total, %3 of %4.</source>
        <translation>完成%1，共%2，%4中的第%3个。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="203"/>
        <source>clearing: %1, %2 of %3</source>
        <translation>正在清理：%1，%3中的第%2个</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="248"/>
        <source>copying...</source>
        <translation>复制中...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="278"/>
        <source>Syncing...</source>
        <translation>正在同步...</translation>
    </message>
</context>
<context>
    <name>Peony::FilePreviewPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="261"/>
        <source>File Name:</source>
        <translation>文件名称：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="266"/>
        <source>File Type:</source>
        <translation>文件类型：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="270"/>
        <source>Time Access:</source>
        <translation>访问时间：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="274"/>
        <source>Time Modified:</source>
        <translation>修改时间：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="280"/>
        <source>Children Count:</source>
        <translation>包含文件：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="285"/>
        <source>Size:</source>
        <translation>文件大小：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="290"/>
        <source>Image resolution:</source>
        <translation>分辨率：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="294"/>
        <source>color model:</source>
        <translation>色彩模式：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="357"/>
        <source>usershare</source>
        <translation>本机共享</translation>
    </message>
    <message>
        <source>Image size:</source>
        <translation type="vanished">图片尺寸：</translation>
    </message>
    <message>
        <source>Image format:</source>
        <translation type="vanished">图片格式：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="396"/>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="397"/>
        <source>%1x%2</source>
        <translation>%1x%2</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="449"/>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="450"/>
        <source>%1 total, %2 hidden</source>
        <translation>共%1项，其中%2个隐藏文件</translation>
    </message>
</context>
<context>
    <name>Peony::FileRenameDialog</name>
    <message>
        <source>Names automatically add serial Numbers (e.g., 1,2,3...)</source>
        <translation type="vanished">名称后自动添加序号（如:1,2,3...）</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>New file name</source>
        <translation type="vanished">文件名</translation>
    </message>
    <message>
        <source>Please enter the file name</source>
        <translation type="vanished">请输入文件名</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
</context>
<context>
    <name>Peony::FileRenameOperation</name>
    <message>
        <source>Rename file</source>
        <translation type="vanished">文件重命名</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="75"/>
        <source>File Rename error</source>
        <translation>文件重命名错误</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="76"/>
        <source>Invalid file name %1%2%3 .</source>
        <translation>非法的文件名%1%2%3.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="92"/>
        <source>Are you sure to hidden this file?</source>
        <translation>确定要隐藏该文件吗？</translation>
    </message>
    <message>
        <source>The file %1%2%3 will be hidden when you refresh or change directory!</source>
        <translation type="vanished">文件 %1%2%3 在刷新或者切换路径后将会被隐藏!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="124"/>
        <source>When change the file suffix, the file may be invalid. Are you sure to change it ?</source>
        <translation>如果改变文件扩展名，可能会导致文件不可用。确实要更改吗？</translation>
    </message>
    <message>
        <source>The file %1%2%3 will be hidden when you refresh or rsort!</source>
        <translation type="vanished">文件 %1%2%3 在刷新或者排序后将会被隐藏!</translation>
    </message>
    <message>
        <source>The file %1%2%3 will be hidden!</source>
        <translation type="vanished">文件%1%2%3将被隐藏!</translation>
    </message>
    <message>
        <source>Invalid file name &quot;%1&quot; </source>
        <translation type="vanished">文件名 &quot;%1&quot; 不合法</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="91"/>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="123"/>
        <source>File Rename warning</source>
        <translation>文件重命名警告</translation>
    </message>
    <message>
        <source>The file &quot;%1&quot; will be hidden!</source>
        <translation type="vanished">文件 &quot;%1&quot; 将会被隐藏！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="210"/>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="240"/>
        <source>Rename file error</source>
        <translation>重命名文件错误</translation>
    </message>
</context>
<context>
    <name>Peony::FileTrashOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="72"/>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="95"/>
        <source>trash:///</source>
        <translation>trash:///</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="75"/>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="98"/>
        <source>Trash file error</source>
        <translation>刪除文件到回收站错误</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="78"/>
        <source>Invalid Operation! Can not trash &quot;%1&quot;.</source>
        <translation>非法的操作! 不能回收 &quot;%1&quot;.</translation>
    </message>
    <message>
        <source>Can not trash</source>
        <translation type="vanished">不能回收</translation>
    </message>
    <message>
        <source>Can not trash files more than 10GB, would you like to delete it permanently?</source>
        <translation type="vanished">无法回收大于10G的文件，是否需要永久删除？</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="202"/>
        <source>An unmanageable conflict exists. Please check the recycle bin.</source>
        <translation>存在无法处理的冲突,请检查回收站。</translation>
    </message>
    <message>
        <source>The user does not have read and write rights to the file &apos;%1&apos; and cannot delete it to the Recycle Bin.</source>
        <translation type="vanished">用户对当前文件 %1 没有读写权限，无法删除到回收站。</translation>
    </message>
    <message>
        <source>Can not trash this file, would you like to delete it permanently?</source>
        <translation type="vanished">不能回收该文件, 是否要永久删除?</translation>
    </message>
    <message>
        <source>Can not trash %1, would you like to delete this file permanently?</source>
        <translation type="vanished">不能回收%1, 是否永久删除?</translation>
    </message>
    <message>
        <source>. Are you sure you want to permanently delete the file</source>
        <translation type="vanished">，你确定要永久删除文件吗？</translation>
    </message>
    <message>
        <source>The user does not have read and write rights to the file &apos;%s&apos; and cannot delete it to the Recycle Bin.</source>
        <translation type="vanished">用户对当前文件 %s 没有读写权限，无法删除到回收站。</translation>
    </message>
    <message>
        <source>Trash file</source>
        <translation type="vanished">删除文件到回收站</translation>
    </message>
</context>
<context>
    <name>Peony::FileUntrashOperation</name>
    <message>
        <source>Untrash file</source>
        <translation type="vanished">撤销删除的文件</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-untrash-operation.cpp" line="157"/>
        <source>Untrash file error</source>
        <translation>从回收站恢复文件错误</translation>
    </message>
</context>
<context>
    <name>Peony::GlobalSettings</name>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="94"/>
        <location filename="../../libpeony-qt/global-settings.cpp" line="467"/>
        <source>yyyy/MM/dd</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="95"/>
        <location filename="../../libpeony-qt/global-settings.cpp" line="459"/>
        <source>HH:mm:ss</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="456"/>
        <source>AP hh:mm:ss</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="470"/>
        <source>yyyy-MM-dd</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Peony::LocationBar</name>
    <message>
        <source>click the blank area for edit</source>
        <translation type="vanished">点击空白区域编辑路径</translation>
    </message>
    <message>
        <source>Computer</source>
        <translation type="obsolete">计算机</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="366"/>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation>在%2中搜索%1</translation>
    </message>
    <message>
        <source>File System</source>
        <translation type="vanished">文件系统</translation>
    </message>
    <message>
        <source>&amp;Copy Directory</source>
        <translation type="vanished">拷贝路径(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="470"/>
        <source>Open In New Tab</source>
        <translation>在新标签页中打开</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="474"/>
        <source>Open In New Window</source>
        <translation>在新窗口中打开</translation>
    </message>
    <message>
        <source>Open In New &amp;Tab</source>
        <translation type="vanished">在新标签页中打开(&amp;T)</translation>
    </message>
    <message>
        <source>Open In &amp;New Window</source>
        <translation type="vanished">在新窗口中打开(&amp;N)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="468"/>
        <source>Copy Directory</source>
        <translation>拷贝路径</translation>
    </message>
</context>
<context>
    <name>Peony::MountOperation</name>
    <message>
        <location filename="../../libpeony-qt/mount-operation.cpp" line="90"/>
        <source>Operation Cancelled</source>
        <translation>操作被取消</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/mount-operation.cpp" line="187"/>
        <source>Login failed, unknown username or password error, please re-enter!</source>
        <translation>登录失败，未知用户名或密码错误，请重新输入！</translation>
    </message>
</context>
<context>
    <name>Peony::NavigationToolBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="35"/>
        <source>Go Back</source>
        <translation>后退</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="39"/>
        <source>Go Forward</source>
        <translation>前进</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="43"/>
        <source>History</source>
        <translation>历史</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="73"/>
        <source>Clear History</source>
        <translation>清空历史</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="88"/>
        <source>Cd Up</source>
        <translation>向上</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="94"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
</context>
<context>
    <name>Peony::NewFileLaunchDialog</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="217"/>
        <source>Choose new application</source>
        <translation>选择一个新的应用</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="219"/>
        <source>Choose an Application to open this file</source>
        <translation>选择一个应用打开这个文件</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="226"/>
        <source>apply now</source>
        <translation>立即应用</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="232"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="233"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>Peony::OpenWithPropertiesPage</name>
    <message>
        <source>How do you want to open %1%2 files ?</source>
        <translation type="vanished">您希望以什么方式打开 %1%2 文件？</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="93"/>
        <source>How do you want to open &quot;%1%2&quot; files ?</source>
        <translation>您希望以什么方式打开“%1%2”文件？</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="98"/>
        <source>Default open with:</source>
        <translation>默认打开方式：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="117"/>
        <source>Other:</source>
        <translation>其他：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="158"/>
        <source>Choose other application</source>
        <translation>选择其他应用</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="174"/>
        <source>Go to application center</source>
        <translation>去软件中心</translation>
    </message>
</context>
<context>
    <name>Peony::PathEdit</name>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/path-bar/path-edit.cpp" line="59"/>
        <source>Go To</source>
        <translation>跳转</translation>
    </message>
</context>
<context>
    <name>Peony::PermissionsPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="136"/>
        <source>User or Group</source>
        <translation>用户或组</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="136"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <source>Readable</source>
        <translation type="vanished">可读</translation>
    </message>
    <message>
        <source>Writeable</source>
        <translation type="vanished">可写</translation>
    </message>
    <message>
        <source>Excuteable</source>
        <translation type="vanished">可执行</translation>
    </message>
    <message>
        <source>File: %1</source>
        <translation type="vanished">文件：%1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="80"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="160"/>
        <source>Target: %1</source>
        <translation>对象名称： %1</translation>
    </message>
    <message>
        <source>Read and Write</source>
        <translation type="vanished">读写</translation>
    </message>
    <message>
        <source>Readonly</source>
        <translation type="vanished">只读</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="136"/>
        <source>Read</source>
        <translation>可读</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="136"/>
        <source>Write</source>
        <translation>可写</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="136"/>
        <source>Executable</source>
        <translation>可执行</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="182"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="191"/>
        <source>Can not get the permission info.</source>
        <translation>无法获取文件权限相关信息。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="252"/>
        <source>(Me)</source>
        <translation>(我)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="321"/>
        <source>Others</source>
        <translation>其他</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="324"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="326"/>
        <source>Owner</source>
        <translation>拥有者</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="327"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="329"/>
        <source>Group</source>
        <translation>用户组</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="330"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="332"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="542"/>
        <source>Permissions modify tip</source>
        <translation>权限修改提示</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="542"/>
        <source>The current file or folder has already set ACL permissions. Modifying user group permissions may cause the permissions set in ACL to be unusable. Do you want to continue modifying user group permissions?</source>
        <translation>当前文件或者文件夹已经被设置了ACL权限，修改用户组权限可能会导致ACL设置的权限无法使用，是否需要继续修改用户组权限？</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="560"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="581"/>
        <source>Permission refinement settings</source>
        <translation>权限细化设置</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="582"/>
        <source>The current user has set advanced sharing. If you still need to modify permissions, advanced sharing may not be available. Do you want to continue setting?</source>
        <translation>当前用户已经设置了高级共享，如果还需要修改权限可能会导致高级共享不可用，是否要继续设置？</translation>
    </message>
    <message>
        <source>Other Users</source>
        <translation type="vanished">其它用户</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="342"/>
        <source>You can not change the access of this file.</source>
        <translation>你无法修改该文件的权限。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="346"/>
        <source>Me</source>
        <translation>我</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="350"/>
        <source>User</source>
        <translation>用户</translation>
    </message>
</context>
<context>
    <name>Peony::PropertiesWindow</name>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="297"/>
        <source>Trash</source>
        <translation>回收站</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="301"/>
        <source>Recent</source>
        <translation>最近</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="309"/>
        <source>Selected</source>
        <translation>选中</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="309"/>
        <source> %1 Files</source>
        <translation>%1 个文件</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="315"/>
        <source>usershare</source>
        <translation>本机共享</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="326"/>
        <source>Data</source>
        <translation>数据盘</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="333"/>
        <source>Properties</source>
        <translation>属性</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="443"/>
        <source>Ok</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="444"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
</context>
<context>
    <name>Peony::RecentAndTrashPropertiesPage</name>
    <message>
        <source>Show confirm dialog while trashing: </source>
        <translation type="vanished">删除到回收站时弹出确认框:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="120"/>
        <source>Show confirm dialog while trashing.</source>
        <translation>删除到回收站时弹出确认框。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="151"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="157"/>
        <source>Origin Path: </source>
        <translation>原路径：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="192"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="226"/>
        <source>Deletion Date: </source>
        <translation>删除日期：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="178"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="241"/>
        <source>Size: </source>
        <translation>文件大小：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="235"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="242"/>
        <source>Original Location: </source>
        <translation>原路径: </translation>
    </message>
</context>
<context>
    <name>Peony::SearchBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="47"/>
        <source>Input the search key of files you would like to find.</source>
        <translation>输入关键词以搜索你想搜索的文件.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="83"/>
        <source>Input search key...</source>
        <translation>输入关键词...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="121"/>
        <source>advance search</source>
        <translation>高级搜索</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="122"/>
        <source>clear record</source>
        <translation>清空历史</translation>
    </message>
</context>
<context>
    <name>Peony::SearchBarContainer</name>
    <message>
        <source>Choose File Type</source>
        <translation type="vanished">选择文件类型</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.cpp" line="126"/>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.cpp" line="256"/>
        <source>Clear</source>
        <translation>清空</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="114"/>
        <source>all</source>
        <translation>全部</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="114"/>
        <source>file folder</source>
        <translation>文件夹</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="114"/>
        <source>image</source>
        <translation>图片</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="115"/>
        <source>video</source>
        <translation>视频</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="115"/>
        <source>text file</source>
        <translation>文本</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="115"/>
        <source>audio</source>
        <translation>音频</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="115"/>
        <source>others</source>
        <translation>其它</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="115"/>
        <source>wps file</source>
        <translation>WPS文件</translation>
    </message>
</context>
<context>
    <name>Peony::SharedFileLinkOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/shared-file-link-operation.cpp" line="44"/>
        <location filename="../../libpeony-qt/file-operation/shared-file-link-operation.cpp" line="47"/>
        <source>Symbolic Link</source>
        <translation>快捷方式</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/shared-file-link-operation.cpp" line="80"/>
        <source>The dest file &quot;%1&quot; has existed!</source>
        <translation>目标文件 %1 已经存在！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/shared-file-link-operation.cpp" line="86"/>
        <source>Link file error</source>
        <translation>创建文件链接失败</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarFavoriteItem</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-favorite-item.cpp" line="83"/>
        <source>Trash</source>
        <translation>回收站</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-favorite-item.cpp" line="86"/>
        <source>Recent</source>
        <translation>最近</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-favorite-item.cpp" line="94"/>
        <source>Favorite</source>
        <translation>快速访问</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarFileSystemItem</name>
    <message>
        <source>Computer</source>
        <translation type="vanished">计算机</translation>
    </message>
    <message>
        <source>File System</source>
        <translation type="vanished">文件系统</translation>
    </message>
    <message>
        <source>data</source>
        <translation type="vanished">数据盘</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="168"/>
        <source>Data</source>
        <translation>数据盘</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarMenu</name>
    <message>
        <source>&amp;Properties</source>
        <translation type="vanished">属性(&amp;P)</translation>
    </message>
    <message>
        <source>P&amp;roperties</source>
        <translation type="vanished">属性(&amp;R)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="65"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="88"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="114"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="129"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="301"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="351"/>
        <source>Properties</source>
        <translation>属性</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="99"/>
        <source>Delete Symbolic</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="154"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="345"/>
        <source>Unmount</source>
        <translation>卸载</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="163"/>
        <source>Eject</source>
        <translation>弹出</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="209"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="242"/>
        <source>Format</source>
        <translation>格式化</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="278"/>
        <source>burndata</source>
        <translation>刻录</translation>
    </message>
    <message>
        <source>&amp;Delete Symbolic</source>
        <translation type="vanished">删除(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Unmount</source>
        <translation type="vanished">卸载(&amp;U)</translation>
    </message>
    <message>
        <source>&amp;Eject</source>
        <translation type="vanished">弹出(&amp;E)</translation>
    </message>
    <message>
        <source>format</source>
        <translation type="vanished">格式化</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarModel</name>
    <message>
        <source>Shared Data</source>
        <translation type="vanished">共享数据</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-model.cpp" line="98"/>
        <source>Network</source>
        <translation>网络</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarPersonalItem</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-personal-item.cpp" line="42"/>
        <source>Personal</source>
        <translation>个人</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarSeparatorItem</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-separator-item.h" line="68"/>
        <source>(No Sub Directory)</source>
        <translation>（空）</translation>
    </message>
</context>
<context>
    <name>Peony::StatusBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="94"/>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="100"/>
        <source>; %1 folders</source>
        <translation>；%1个文件夹</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="95"/>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="102"/>
        <source>; %1 files, %2 total</source>
        <translation>；%1个文件，总共%2</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="97"/>
        <source>; %1 folder</source>
        <translation>；%1个文件夹</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="98"/>
        <source>; %1 file, %2</source>
        <translation>；%1个文件，%2</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="105"/>
        <source>%1 selected</source>
        <translation>选中%1项</translation>
    </message>
</context>
<context>
    <name>Peony::SyncThread</name>
    <message>
        <location filename="../../libpeony-qt/sync-thread.cpp" line="44"/>
        <source>notify</source>
        <translatorcomment>温馨提示</translatorcomment>
        <translation>温馨提示</translation>
    </message>
</context>
<context>
    <name>Peony::ToolBar</name>
    <message>
        <source>Open in new &amp;Window</source>
        <translation type="vanished">在新窗口中打开(&amp;W)</translation>
    </message>
    <message>
        <source>Open in &amp;New window</source>
        <translation type="vanished">在新窗口中打开(&amp;N)</translation>
    </message>
    <message>
        <source>Open in new &amp;Tab</source>
        <translation type="vanished">在新标签页中打开(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="138"/>
        <source>Sort Type</source>
        <translation>排序类型</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="140"/>
        <source>File Name</source>
        <translation>文件名称</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="146"/>
        <source>File Type</source>
        <translation>文件类型</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="149"/>
        <source>File Size</source>
        <translation>文件大小</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="143"/>
        <source>Modified Date</source>
        <translation>修改日期</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="72"/>
        <source>Open in New window</source>
        <translation>在新窗口中打开</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="74"/>
        <source>Open in new Tab</source>
        <translation>在新标签页中打开</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="160"/>
        <source>Ascending</source>
        <translation>升序</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="156"/>
        <source>Descending</source>
        <translation>降序</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="190"/>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="340"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="193"/>
        <source>Paste</source>
        <translation>粘贴</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="196"/>
        <source>Cut</source>
        <translation>剪切</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="199"/>
        <source>Trash</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="216"/>
        <source>Clean Trash</source>
        <translation>清空回收站</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <source>Delete Permanently</source>
        <translation type="vanished">永久删除</translation>
    </message>
    <message>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="vanished">您确定要删除这些文件吗？一旦开始删除，这些文件将不可再恢复。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="221"/>
        <source>Restore</source>
        <translation>还原</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="271"/>
        <source>Options</source>
        <translation>选项</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="274"/>
        <source>Forbid Thumbnail</source>
        <translation>禁用缩略图</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="281"/>
        <source>Show Hidden</source>
        <translation>显示隐藏文件</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="288"/>
        <source>Resident in Backend</source>
        <translation>常驻后台</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="289"/>
        <source>Let the program still run after closing the last window. This will reduce the time for the next launch, but it will also consume resources in backend.</source>
        <translation>让文件管理器在关闭所有窗口后仍然运行，这将缩短下次启动所需要的时间，但是也会一直占用资源。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="301"/>
        <source>&amp;Help</source>
        <translation>帮助(&amp;H)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="307"/>
        <source>&amp;About...</source>
        <translation>关于...(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="309"/>
        <source>Peony Qt</source>
        <translation>文件管理器</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="310"/>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation>作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <source>Author: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019,天津麒麟信息技术有限公司.</translation>
    </message>
</context>
<context>
    <name>Peony::UserShareInfoManager</name>
    <message>
        <location filename="../../libpeony-qt/usershare-manager.cpp" line="94"/>
        <location filename="../../libpeony-qt/usershare-manager.cpp" line="127"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
</context>
<context>
    <name>Peony::VolumeManager</name>
    <message>
        <location filename="../../libpeony-qt/volume-manager.cpp" line="150"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
</context>
<context>
    <name>ProgressBar</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="803"/>
        <source>starting ...</source>
        <translation>正在开始 ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="900"/>
        <source>canceling ...</source>
        <translation>取消中 ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="902"/>
        <source>sync ...</source>
        <translation>正在同步...</translation>
    </message>
    <message>
        <source>cancel file operation</source>
        <translation type="vanished">取消文件操作</translation>
    </message>
    <message>
        <source>Are you sure want to cancel the current selected file operation</source>
        <translation type="vanished">你确定要取消当前选中的文件操作</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="40"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="60"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="91"/>
        <source>Icon View</source>
        <translation>图标视图</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="46"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="97"/>
        <source>Show the folder children as icons.</source>
        <translation>以图标形式显示目录.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="42"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="62"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="93"/>
        <source>List View</source>
        <translation>列表视图</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="48"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="99"/>
        <source>Show the folder children as rows in a list.</source>
        <translation>以列表形式显示目录.</translation>
    </message>
    <message>
        <source>Basic Preview Page</source>
        <translation type="vanished">基本</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page-factory.h" line="40"/>
        <source>Basic</source>
        <translation>基本</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page-factory.h" line="46"/>
        <source>Show the basic file properties, and allow you to modify the access and name.</source>
        <translation>显示文件的基本属性，允许修改文件名称。</translation>
    </message>
    <message>
        <source>Permissions Page</source>
        <translation type="vanished">权限</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page-factory.h" line="41"/>
        <source>Permissions</source>
        <translation>权限</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page-factory.h" line="47"/>
        <source>Show and modify file&apos;s permission, owner and group.</source>
        <translation>查看和修改文件的权限。</translation>
    </message>
    <message>
        <source>Can not trash</source>
        <translation type="vanished">不能回收</translation>
    </message>
    <message>
        <source>Can not trash these files. You can delete them permanently. Are you sure doing that?</source>
        <translation type="vanished">这些文件不能完全放入回收站，可以选择永久删除这些文件，确定这样做吗？</translation>
    </message>
    <message>
        <source>Can not trash files more than 10GB, would you like to delete it permanently?</source>
        <translation type="vanished">无法回收大于10G的文件，是否需要永久删除？</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="195"/>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="108"/>
        <source>The file is too large to be moved to the recycle bin. Do you want to permanently delete it?</source>
        <translation>文件过大，不可移入回收站。是否永久删除该文件？</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="199"/>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="111"/>
        <source>These files are too large to be moved to the recycle bin. Do you want to permanently delete these %1 files?</source>
        <translation>文件过大，不可移入回收站。是否永久删除这 %1 项文件？</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="311"/>
        <source>Clean the Trash</source>
        <translation>清空回收站</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">清空回收站</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="320"/>
        <source>Do you want to empty the recycle bin and delete the files permanently? Once it has begun there is no way to restore them.</source>
        <translation>确认要清空回收站内的文件吗？此操作无法撤销。</translation>
    </message>
    <message>
        <source>Delete Permanently</source>
        <translation type="vanished">永久删除</translation>
    </message>
    <message>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="vanished">您确定要删除这些文件吗？一旦开始删除，这些文件将不可再恢复。</translation>
    </message>
    <message>
        <source>Computer Properties Page</source>
        <translation type="vanished">计算机</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page-factory.h" line="41"/>
        <source>Computer Properties</source>
        <translation>计算机属性</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page-factory.h" line="47"/>
        <source>Show the computer properties or items in computer.</source>
        <translation>显示计算机属性或计算机中的项。</translation>
    </message>
    <message>
        <source>Trash and Recent Properties Page</source>
        <translation type="vanished">最近/回收</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page-factory.h" line="40"/>
        <source>Trash and Recent</source>
        <translation>回收站/最近</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page-factory.h" line="46"/>
        <source>Show the file properties or items in trash or recent.</source>
        <translation>显示“回收站”或“最近”中的文件属性或项目。</translation>
    </message>
    <message>
        <source>eject device failed</source>
        <translation type="vanished">弹出设备失败</translation>
    </message>
    <message>
        <source>Please check whether the device is occupied and then eject the device again</source>
        <translation type="vanished">请检查设备是否正在使用,确认没有使用后再次弹出</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="541"/>
        <source>Format failed</source>
        <translation>格式化失败</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="543"/>
        <source>YES</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1022"/>
        <source>Formatting successful! But failed to set the device name.</source>
        <translation>格式化成功！设备名设置失败。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1039"/>
        <source>qmesg_notify</source>
        <translation>通知</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1054"/>
        <source>Format</source>
        <translation>格式化</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1056"/>
        <source>Begin Format</source>
        <translation>开始</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1058"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1020"/>
        <source>Format operation has been finished successfully.</source>
        <translation>格式化操作已成功完成。</translation>
    </message>
    <message>
        <source>Formatting successful! Description Failed to set the device name.</source>
        <translation type="vanished">格式化成功！设备名设置失败。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1039"/>
        <source>Sorry, the format operation is failed!</source>
        <translation>很遗憾，格式化操作失败了，您可以重新试下！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1052"/>
        <source>Formatting this volume will erase all data on it. Please backup all retained data before formatting. Do you want to continue ?</source>
        <translation>格式化此卷将清除其上的所有数据。请在格式化之前备份所有保留的数据。您想继续吗?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1020"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1022"/>
        <source>format</source>
        <translation>格式化</translation>
    </message>
    <message>
        <source>begin format</source>
        <translation type="vanished">开始</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/sync-thread.cpp" line="41"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="946"/>
        <source>File Manager</source>
        <translation>文件管理器</translation>
    </message>
    <message>
        <source>Default search vfs of peony</source>
        <translation type="vanished">默认文件搜索</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="117"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1681"/>
        <source>Force unmount failed</source>
        <translation>强制卸载失败</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="136"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="117"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1681"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="539"/>
        <source>Error: %1
</source>
        <translation>错误: %1
</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="142"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1684"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1734"/>
        <source>Data synchronization is complete,the device has been unmount successfully!</source>
        <translation>数据同步完成，设备已经成功卸载！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="131"/>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="136"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1713"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1716"/>
        <source>Unmount failed</source>
        <translation>卸载失败</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1701"/>
        <source>Not authorized to perform operation.</source>
        <translation>操作未获得授权。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="131"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1713"/>
        <source>Unable to unmount it, you may need to close some programs, such as: GParted etc.</source>
        <translation>无法卸载，您可能需要先关闭一些程序，如分区编辑器等。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1716"/>
        <source>Error: %1
Do you want to unmount forcely?</source>
        <translation>错误: %1
是否强制卸载?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="315"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Eject Anyway</source>
        <translation type="vanished">无论如何弹出</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1179"/>
        <source>Failed to activate device: Incorrect passphrase</source>
        <translation>无法激活设备: 错误的口令</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1189"/>
        <source>The device has been mount successfully!</source>
        <translation>设备挂载成功！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1382"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1420"/>
        <source>Eject device failed, the reason may be that the device has been removed, etc.</source>
        <translation>弹出设备失败，可能是设备已经移除等原因。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1388"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1427"/>
        <source>Data synchronization is complete and the device can be safely unplugged!</source>
        <translation>数据同步完成，设备可以安全拔出！</translation>
    </message>
    <message>
        <source>Unable to eject %1</source>
        <translation type="vanished">无法弹出 %1</translation>
    </message>
    <message>
        <source>PeonyNotify</source>
        <translation type="vanished">文件管理器通知</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1384"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1422"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1701"/>
        <source>Eject failed</source>
        <translation>弹出失败</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="262"/>
        <source>favorite</source>
        <translation>快速访问</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="295"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="300"/>
        <source>File is not existed.</source>
        <translation>文件不存在.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="308"/>
        <source>Share Data</source>
        <translation>本机共享</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="312"/>
        <source>Trash</source>
        <translation>回收站</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="316"/>
        <source>Recent</source>
        <translation>最近</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="362"/>
        <source>Operation not supported</source>
        <translation>操作不支持</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="457"/>
        <source>The virtual file system does not support folder creation</source>
        <translation>虚拟文件系统下不支持创建新文件夹</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="529"/>
        <source>Can not create a symbolic file for vfs location</source>
        <translation>无法为虚拟目录创建快捷方式</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="536"/>
        <source>Symbolic Link</source>
        <translation>快捷方式</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="548"/>
        <source>Can not create symbolic file here, %1</source>
        <translation>无法在此创建快捷方式, %1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="557"/>
        <source>Can not add a file to favorite directory.</source>
        <translation>文件不能被添加到收藏夹.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="615"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="623"/>
        <source>The virtual file system cannot be opened</source>
        <translation>虚拟文件系统无法打开</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="444"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="472"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="487"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="573"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="591"/>
        <source>Virtual file directories do not support move and copy operations</source>
        <translation>虚拟文件路径不支持移动和复制操作</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-register.h" line="43"/>
        <source>Default favorite vfs of peony</source>
        <translation>文件管理器虚拟文件系统默认快速访问</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page-factory.h" line="38"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page-factory.h" line="44"/>
        <source>Details</source>
        <translation>详细信息</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/mark-properties-page-factory.h" line="40"/>
        <source>Mark</source>
        <translation>标记</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/mark-properties-page-factory.h" line="46"/>
        <source>mark this file.</source>
        <translation>标记这个文件。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page-factory.h" line="40"/>
        <source>Open With</source>
        <translation>打开方式</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page-factory.h" line="46"/>
        <source>open with.</source>
        <translation>打开方式。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/sync-thread.cpp" line="11"/>
        <source>It need to synchronize before operating the device,place wait!</source>
        <translation>操作设备前需要同步数据，请稍等！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="409"/>
        <source>permission denied</source>
        <translation>没有权限</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="402"/>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="415"/>
        <source>file not found</source>
        <translation>没有发现该文件</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="499"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="516"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="532"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="825"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="171"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="193"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="215"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="222"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="238"/>
        <source>duplicate</source>
        <translation>副本</translation>
    </message>
    <message>
        <source>Error when getting information for file : No target file found</source>
        <translation type="vanished">获取文件信息时出现错误：没有目标文件。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-utils.cpp" line="361"/>
        <source>data</source>
        <translation>数据盘</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="185"/>
        <location filename="../../libpeony-qt/vfs/search-vfs-uri-parser.cpp" line="110"/>
        <source>Computer</source>
        <translation>计算机</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-utils.cpp" line="364"/>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="249"/>
        <source>File System</source>
        <translation>文件系统</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="254"/>
        <source>Data</source>
        <translation>数据盘</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="411"/>
        <source>Failed to open file &quot;%1&quot;: insufficient permissions.</source>
        <translation>打开文件&quot;%1&quot;失败：权限不足。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="422"/>
        <source>File “%1” does not exist. Please check whether the file has been deleted.</source>
        <translation>文件“%1”不存在，请检查文件是否被删除。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccommand.cpp" line="58"/>
        <source>burn operation has been cancelled</source>
        <translation>刻录操作已取消</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccommand.cpp" line="62"/>
        <source> is busy!</source>
        <translation>被占用！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1213"/>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="203"/>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="375"/>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="182"/>
        <source>Are you sure you want to permanently delete this file? Once deletion begins, the file will not be recoverable.</source>
        <translation>确定永久删除该文件吗？一旦开始删除，文件将不可恢复。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1217"/>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="207"/>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="379"/>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="186"/>
        <source>Are you sure you want to permanently delete these %1 files? Once deletion begins, these file will not be recoverable.</source>
        <translation>确定永久删除这 %1 项文件吗？一旦开始删除，文件将不可恢复。</translation>
    </message>
</context>
<context>
    <name>UdfBurn::UdfAppendBurnDataDialog</name>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="23"/>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="158"/>
        <source>AppendBurnData</source>
        <translation>追加刻录</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="36"/>
        <source>Disc Type:</source>
        <translation>光盘类型：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="44"/>
        <source>Device Name:</source>
        <translation>设备名称:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="57"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="59"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="67"/>
        <source>Unknow</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="109"/>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="126"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="109"/>
        <source>No burn data, please add!</source>
        <translation>没有刻录数据, 请添加!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="126"/>
        <source>The disc name cannot be set to empty, please re-enter it!</source>
        <translation>设备名称不能设置为空，请重新输入！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="158"/>
        <source>AppendBurnData operation has been finished successfully.</source>
        <translation>追加刻录操作成功。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="164"/>
        <source>Sorry, the appendBurnData operation is failed!</source>
        <translation>抱歉, 追加刻录操作失败!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="165"/>
        <source>Failed</source>
        <translation>失败</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="174"/>
        <source>Burning. Do not close this window</source>
        <translation>刻录中, 请勿关闭此窗口</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="183"/>
        <source>Burning this disc will append datas on it. Do you want to continue ?</source>
        <translation>刻录此光盘将追加这些数据到此光盘, 是否继续?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="184"/>
        <source>Burn</source>
        <translation>刻录</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="186"/>
        <source>Begin Burning</source>
        <translation>开始刻录</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="187"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>UdfBurn::UdfFormatDialog</name>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="24"/>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="143"/>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="194"/>
        <source>Format</source>
        <translation>格式化</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="37"/>
        <source>Disc Type:</source>
        <translation>光盘类型：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="45"/>
        <source>Device Name:</source>
        <translation>设备名称:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="59"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="61"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="69"/>
        <source>Unknow</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="108"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="108"/>
        <source>The disc name cannot be set to empty, please re-enter it!</source>
        <translation>设备名称不能设置为空，请重新输入！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="143"/>
        <source>Format operation has been finished successfully.</source>
        <translation>格式化操作已成功完成。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="148"/>
        <source>Sorry, the format operation is failed!</source>
        <translation>很遗憾，格式化操作失败了，您可以重新试下！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="149"/>
        <source>Failed</source>
        <translation>失败</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="183"/>
        <source>Formatting. Do not close this window</source>
        <translation>正在格式化, 请勿关闭</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="193"/>
        <source>Formatting this disc will erase all data on it. Please backup all retained data before formatting. Do you want to continue ?</source>
        <translation>格式化此光盘将擦除其上的所有数据。 请在格式化前备份所有保留的数据。 你想继续吗 ？</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="196"/>
        <source>Begin Format</source>
        <translation>开始</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="197"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>UdfFormatDialog</name>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="19"/>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="143"/>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="196"/>
        <source>Format</source>
        <translation>格式化</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="32"/>
        <source>Disc Type:</source>
        <translation>光盘类型：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="40"/>
        <source>Device Name:</source>
        <translation>设备名称:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="54"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="56"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="64"/>
        <source>Unknow</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="106"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="106"/>
        <source>The disc name cannot be set to empty, please re-enter it!</source>
        <translation>设备名称不能设置为空，请重新输入！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="143"/>
        <source>Format operation has been finished successfully.</source>
        <translation>格式化操作已成功完成。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="148"/>
        <source>Sorry, the format operation is failed!</source>
        <translation>很遗憾，格式化操作失败了，您可以重新试下！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="149"/>
        <source>Failed</source>
        <translation>失败</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="183"/>
        <source>Formatting. Do not close this window</source>
        <translation>正在格式化, 请勿关闭</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="195"/>
        <source>Formatting this disc will erase all data on it. Please backup all retained data before formatting. Do you want to continue ?</source>
        <translation>格式化此光盘将擦除其上的所有数据。 请在格式化前备份所有保留的数据。 你想继续吗 ？</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="198"/>
        <source>Begin Format</source>
        <translation>开始</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="199"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
</context>
</TS>
