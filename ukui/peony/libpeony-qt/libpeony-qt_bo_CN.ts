<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>ConnectServerDialog</name>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="14"/>
        <source>Connect to Sever</source>
        <translation>ཞབས་ཞུའི་འཕྲུལ་ཆས་དང་སྦྲེལ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="32"/>
        <source>Domain</source>
        <translation>ཁོངས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="39"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="55"/>
        <source>Save Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="62"/>
        <source>User</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="82"/>
        <source>Anonymous</source>
        <translation>མིང་མེད་ཐོ་འགོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.cpp" line="35"/>
        <source>Ok</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.cpp" line="36"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
</context>
<context>
    <name>DiscControl</name>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="453"/>
        <source> is busy!</source>
        <translation> བཟུང་ཟིན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="492"/>
        <source>is busy!</source>
        <translation>བཟུང་ཟིན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="539"/>
        <source> not support udf at present.</source>
        <translation> མིག་སྔར་udfརྣམ་གཞག་ཅན་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="546"/>
        <source>unmount disc failed before udf format.</source>
        <translation>udfརྣམ་གཞག་གི་སྔོན་ལ་སྡུད་སྡེར་ཕབ་པ་ལེགས་འགྲུབ་མ་བྱུང་།</translation>
    </message>
    <message>
        <source>is not properly formatted.</source>
        <translation type="vanished">格式不正确。</translation>
    </message>
    <message>
        <source>Can not found newfs_udf tool.</source>
        <translation type="vanished">未找到newfs_udf工具。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="699"/>
        <source>DVD+RW udf format fail.</source>
        <translation>DVD＋RWudfརྣམ་གཞག་ཅན་ཕམ་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="731"/>
        <source>preparation failed before DVD-RW udf format.</source>
        <translation>DVD-RWudfཚད་གཞི་སྔོན་གྱི་གྲ་སྒྲིག་ཕམ་སོང་།</translation>
    </message>
</context>
<context>
    <name>FileLabelModel</name>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="37"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="56"/>
        <source>Red</source>
        <translation>དམར་པོ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="38"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="57"/>
        <source>Orange</source>
        <translation>ལི་མདོག་</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="39"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="58"/>
        <source>Yellow</source>
        <translation>སེར་པོ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="40"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="59"/>
        <source>Green</source>
        <translation>ལྗང་མདོག</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="41"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="60"/>
        <source>Blue</source>
        <translation>སྔོན་པོ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="42"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="61"/>
        <source>Purple</source>
        <translation>སྨུག་ནག</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="43"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="62"/>
        <source>Gray</source>
        <translation>སྐྱ་བོ།</translation>
    </message>
    <message>
        <source>Transparent</source>
        <translation type="vanished">无颜色</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="129"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="357"/>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="129"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="357"/>
        <source>Label or color is duplicated.</source>
        <translation>ཡིག་བྱང་ངམ་ཡང་ན་ཁ་དོག་བསྐྱར་ཟློས་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>FileOperationHelper</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-helper.cpp" line="157"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-helper.cpp" line="175"/>
        <source>Burn failed</source>
        <translation>བརྐོས་པར་ཕམ་ཁ་བྱུང་།</translation>
    </message>
</context>
<context>
    <name>Format_Dialog</name>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>རྣམ་གཞག་ཅན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="32"/>
        <source>rom_size</source>
        <translation>ཤོང་ཚད་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="45"/>
        <source>system</source>
        <translation>ཡིག་ཆའི་རྒྱུད་ཁོངས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="59"/>
        <source>vfat/fat32</source>
        <translation>vfat/fat32</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="64"/>
        <source>exfat</source>
        <translation>exfat</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="69"/>
        <source>ntfs</source>
        <translation>NTFS</translation>
    </message>
    <message>
        <source>vfat</source>
        <translation type="vanished">VFAT</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="74"/>
        <source>ext4</source>
        <translation>Ext4</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="88"/>
        <source>device_name</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="114"/>
        <source>clean it total</source>
        <translation>ཡོངས་སུ་གསུབ་དགོས།(དུས་ཡུན་ཅུང་རིང་བས་ཁས་ལེན་གནང་རོགས།)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="127"/>
        <source>ok</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="140"/>
        <source>close</source>
        <translation>སྒོ་གཏན་རོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="179"/>
        <source>TextLabel</source>
        <translation>གཤོང་ཚད།</translation>
    </message>
    <message>
        <source>qmesg_notify</source>
        <translation type="vanished">通知</translation>
    </message>
    <message>
        <source>Format operation has been finished successfully.</source>
        <translation type="vanished">格式化操作已成功完成。</translation>
    </message>
    <message>
        <source>Sorry, the format operation is failed!</source>
        <translation type="vanished">很遗憾，格式化操作失败了，您可以重新试下！</translation>
    </message>
    <message>
        <source>Formatting this volume will erase all data on it. Please backup all retained data before formatting. Do you want to continue ?</source>
        <translation type="vanished">格式化此卷将清除其上的所有数据。请在格式化之前备份所有保留的数据。您想继续吗?</translation>
    </message>
    <message>
        <source>format</source>
        <translation type="vanished">格式化</translation>
    </message>
    <message>
        <source>begin format</source>
        <translation type="vanished">开始</translation>
    </message>
    <message>
        <source>format_success</source>
        <translation type="vanished">格式化成功!</translation>
    </message>
    <message>
        <source>format_err</source>
        <translation type="vanished">格式化失败!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="154"/>
        <source>Format</source>
        <translation>རྣམ་གཞག་ཅན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="167"/>
        <source>Rom size:</source>
        <translation>ཤོང་ཚད་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="173"/>
        <source>Filesystem:</source>
        <translation>ཡིག་ཆའི་རྒྱུད་ཁོངས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="183"/>
        <source>Disk name:</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="213"/>
        <source>Completely erase(Time is longer, please confirm!)</source>
        <translation>ཡོངས་སུ་གསུབ་དགོས།(དུས་ཡུན་ཅུང་རིང་བས་ཁས་ལེན་གནང་རོགས།)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="231"/>
        <source>Set password</source>
        <translation>གསང་གྲངས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="232"/>
        <source>Set password for volume based on LUKS (only ext4)</source>
        <translation>ext4ཁུལ་ལ་LUKSཡི་གསང་གྲངས་སྒྲིག་བཀོད་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="238"/>
        <source>Formatting to the ext4 file system may cause other users to be unable to read or write to the USB drive</source>
        <translation>རྣམ་གཞག་ཅན་དེ་ext4ཡིག་ཆའི་མ་ལག་ཏུ་བསྒྱུར་སྐབས་སྤྱོད་མཁན་གཞན་དག་གིས་Uསྡེར་ཀློག་མི་ཐུབ་པར་འགྱུར་སྲིད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="246"/>
        <source>Cancel</source>
        <translation>སྒོ་གཏན་རོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="247"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="360"/>
        <source>Data</source>
        <translation>གཞི་གྲངས་སྡེར།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="238"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="564"/>
        <source>Warning</source>
        <translation>ཉེན་བརྡ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="564"/>
        <source>Device name cannot start with a decimal point, Please re-enter!</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་དེ་སིལ་གྲངས་ཚེག་ནས་མགོ་བརྩམས་མི་རུང་།ཡང་བསྐྱར་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="575"/>
        <source>Enter Password:</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="592"/>
        <source>Password too short, please retype a password more than 6 characters</source>
        <translation>གསང་གྲངས་ཐུང་དྲགས་པས་ཡང་བསྐྱར་གསང་གྲངས་དྲུག་ལས་ཆེ་བ་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="691"/>
        <source>over one day</source>
        <translation>ཉིན་གཅིག་ལས་བརྒལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="691"/>
        <source>%1/sec, %2 remaining.</source>
        <translation>སྐར་ཆ་རེར་བརྒྱ་ཆའི1．2ལྷག་མའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="693"/>
        <source>getting progress...</source>
        <translation>མྱུར་ཚད་ཁྲོད་དུ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1278"/>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1278"/>
        <source>Block not existed!</source>
        <translation>སྒྲིག་ཆས་མི་འདུག</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1325"/>
        <source>Formatting. Do not close this window</source>
        <translation>རྣམ་གཞག་ཅན་དུ་འགྱུར་བཞིན་པའི་སྒང་ཡིན་པས་སྒོ་མ་བརྒྱག</translation>
    </message>
</context>
<context>
    <name>KyFileDialogRename</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="53"/>
        <source>Renaming &quot;%1&quot;</source>
        <translation>བརྒྱ་ཆ1ཞེས་པའི་མིང་བཏགས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="54"/>
        <source>Renaming failed, the reason is: %1</source>
        <translation>བསྐྱར་དུ་མིང་བཏགས་ནས་ཕམ་སོང་།རྒྱུ་རྐྱེན%1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="54"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="63"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="72"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="77"/>
        <source>Filename too long</source>
        <translation>ཡིག་ཆའི་མིང་ཧ་ཅང་རིང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="58"/>
        <source>Copying &quot;%1&quot;</source>
        <translation>བརྒྱ་ཆ་1འདྲ་བཟོ་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="62"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="71"/>
        <source>To &quot;%1&quot;</source>
        <translation>%དང1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="63"/>
        <source>Copying failed, the reason is: %1</source>
        <translation>འདྲ་བཟོ་ལེགས་འགྲུབ་མ་བྱུང་བ།རྒྱུ་རྐྱེན%1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="67"/>
        <source>Moving &quot;%1&quot;</source>
        <translation>བརྒྱ་ཆ1སྤོ་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="72"/>
        <source>Moving failed, the reason is: %1</source>
        <translation>སྤོ་འགུལ་ལ་ཕམ་ཁ་བྱུང་བའི་རྒྱུ་རྐྱེན།%1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="76"/>
        <source>File operation error:</source>
        <translation>ཡིག་ཆ་བཀོལ་སྤྱོད་བྱེད་སྟངས་ནོར་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="77"/>
        <source>The reason is: %1</source>
        <translation>རྒྱུ་རྐྱེན།%</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="96"/>
        <source>Truncation</source>
        <translation>བཅད་བཅད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="127"/>
        <source>All applications</source>
        <translation>ཚང་མ་བེད་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="130"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="168"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="228"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="131"/>
        <source>Apply</source>
        <translation>བེད་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="284"/>
        <source>Explanation: Truncate the portion of the file name that exceeds 225 bytes and select</source>
        <translation>གསལ་བཤད།ཡིག་ཆའི་མིང་བཅད་པའི་ཡིག་ཚིགས་225ལས་བརྒལ་ན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="293"/>
        <source>Description: By default, save to &quot;%1/扩展&quot;.</source>
        <translation>གསལ་བཤད།ཁས་བླངས་པ་དེ“%རྒྱ་བསྐྱེད་པའི”བར་ཉར་ཚགས་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="302"/>
        <source>Explanation: When renaming a file name, ensure it is within 225 bytes and </source>
        <translation>གསལ་བཤད།སྤྱོད་མཁན་གྱིས་ཡིག་ཆའི་མིང་བསྐྱར་འདོགས་བྱས་ཏེ་ཡིག་ཚིགས་༢༢༥ནང་དུ་འགྲོ་བར་ཁག་ཐེག་བྱེད་པ། </translation>
    </message>
    <message>
        <source>Explanation: Truncate the portion of the file name that exceeds 225 bytes and </source>
        <translation type="obsolete">说明：用户重命名文件名，保证在225字节以内，</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="97"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="169"/>
        <source>Save</source>
        <translation>ཉར་ཚགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="164"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="214"/>
        <source>Bytes</source>
        <translation>ཡིག་ཚིགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="220"/>
        <source>Front truncation</source>
        <translation>སྔོན་མའི་མཚམས་གཅོད་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="221"/>
        <source>Post truncation</source>
        <translation>ཕྱིས་སུ་གཅོད་གཏུབ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="277"/>
        <source>Description: Skip copying files of the current type</source>
        <translation>གསལ་བཤད།མིག་སྔའི་ཡིག་ཆ་འདྲ་བཟོ་བྱས་པ་ལས་མཆོང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="282"/>
        <source>truncate interval</source>
        <translation>བར་མཚམས་བཅད་པ།</translation>
    </message>
    <message>
        <source>Explanation: Truncate the portion of the file name that exceeds 225 bytes </source>
        <translation type="vanished">说明：截断文件名的超过225字节的部分，去选择</translation>
    </message>
    <message>
        <source>Description: By default, save to &quot;%1/extension&quot;.</source>
        <translation type="vanished">说明：默认保存至“%1/拓展”。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="300"/>
        <source>modify the name</source>
        <translation>མིང་བཏགས་བཟོ་བཅོས་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Explanation: When renaming a file name, ensure it is within 225 bytes </source>
        <translation type="vanished">说明：用户重命名文件名，保证在225字节以内，去</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="95"/>
        <source>Skip</source>
        <translation>ལྡིང་།</translation>
    </message>
    <message>
        <source>Skip All</source>
        <translation type="vanished">全部跳过</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="98"/>
        <source>Rename</source>
        <translation>མིང་བཏགས་པ།</translation>
    </message>
    <message>
        <source>Please enter a new name</source>
        <translation type="vanished">请输入文件名</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="229"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
</context>
<context>
    <name>MainProgressBar</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="399"/>
        <source>File operation</source>
        <translation>ཡིག་ཆ་བཀོལ་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="441"/>
        <source>starting ...</source>
        <translation>ད་ལྟ་མགོ་བརྩམས་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="417"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="546"/>
        <source>cancel all file operations</source>
        <translation>ཡིག་ཆ་བཀོལ་སྤྱོད་ཚང་མ་མེད་པར་བཟོས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="407"/>
        <source>Minimize</source>
        <translation>ཆེས་ཆུང་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="415"/>
        <source>Close</source>
        <translation>སྒོ་གཏན་རོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="418"/>
        <source>Are you sure to cancel all file operations?</source>
        <translation>ཁྱོད་ཀྱིས་ཡིག་ཆའི་བཀོལ་སྤྱོད་ཚང་མ་མེད་པར་གཏོང་ཁོ་ཐག་ཡིན་ནམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="547"/>
        <source>Are you sure want to cancel all file operations</source>
        <translation>ཁྱོད་ཀྱིས་ཡིག་ཆའི་བཀོལ་སྤྱོད་ཚང་མ་མེད་པར་གཏོང་ཁོ་ཐག་ཡིན་ནམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="420"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="549"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="421"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="550"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="582"/>
        <source>continue</source>
        <translation>མུ་མཐུད་དུ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="584"/>
        <source>pause</source>
        <translation>མཚམས་ཞོག</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="664"/>
        <source>canceling ...</source>
        <translation>མེད་པར་བཟོ་བཞིན་པའི་སྒང་རེད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="667"/>
        <source>sync ...</source>
        <translation>གོམ་མཉམ་བྱེད་བཞིན་ཡོད།</translation>
    </message>
</context>
<context>
    <name>MessageDialog</name>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1766"/>
        <source>Peony</source>
        <translation>ཡིག་ཆའི་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1793"/>
        <source>Forcibly pulling out the device may cause data
 loss or device exceptions!</source>
        <translation>བཙན་ཤེད་ཀྱིས་སྒྲིག་ཆས་བླངས་ན་གཞི་གྲངས་བོར་བའམ་ཡང་ན་སྒྲིག་ཆས་རྒྱུན་ལྡན་མིན་པར་འགྱུར་སྲིད།</translation>
    </message>
</context>
<context>
    <name>OtherButton</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="768"/>
        <source>Other queue</source>
        <translation>རུ་ཁག་གཞན་པ།</translation>
    </message>
</context>
<context>
    <name>Peony::AdvanceSearchBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="55"/>
        <source>Key Words</source>
        <translation>གནད་ཚིག</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="58"/>
        <source>input key words...</source>
        <translation>གནད་ཚིག་ནང་འཇུག་བྱོས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="59"/>
        <source>Search Location</source>
        <translation>བཤེར་འཚོལ་འགྲོ་ལམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="61"/>
        <source>choose search path...</source>
        <translation>འཚོལ་བཤེར་བྱེད་པའི་གནས་འདེམས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="68"/>
        <source>browse</source>
        <translation>རགས་ལྟ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="69"/>
        <source>File Type</source>
        <translation>ཡིག་ཆའི་རིགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="71"/>
        <source>Choose File Type</source>
        <translation>ཡིག་ཆའི་རིགས་འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="76"/>
        <source>Modify Time</source>
        <translation>བཟོ་བཅོས་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="78"/>
        <source>Choose Modify Time</source>
        <translation>བཟོ་བཅོས་རྒྱག་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="83"/>
        <source>File Size</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="85"/>
        <source>Choose file size</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="90"/>
        <source>show hidden file</source>
        <translation>མངོན་མེད་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="91"/>
        <source>go back</source>
        <translation>ཕྱི་ནུར་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="92"/>
        <source>hidden advance search page</source>
        <translation>མཐོ་རིམ་འཚོལ་ཞིབ་མཚམས་ངོས་སྦས་སྐུང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="94"/>
        <source>file name</source>
        <translation>ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="95"/>
        <source>content</source>
        <translation>ནང་དོན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="100"/>
        <source>search</source>
        <translation>བཤེར་འཚོལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="101"/>
        <source>start search</source>
        <translation>འཚོལ་ཞིབ་བྱེད་མགོ་བརྩམས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="174"/>
        <source>Select path</source>
        <translation>གདམ་གའི་ཐབས་ལམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="193"/>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="202"/>
        <source>Operate Tips</source>
        <translation>སྣེ་སྟོན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="194"/>
        <source>Have no key words or search location!</source>
        <translation>གནད་འགག་གི་ཡི་གེ་དང་འགྲོ་ལམ་མེད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="203"/>
        <source>Search file name or content at least choose one!</source>
        <translation>ཡིག་ཆའི་མིང་དང་ནང་དོན་འཚོལ་ཞིབ་བྱེད་ན་ཆེས་ཉུང་ནའང་གཅིག་གཏན་ཁེལ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>Search content or file name at least choose one!</source>
        <translation type="vanished">搜索文件名或者内容请至少指定一个！</translation>
    </message>
    <message>
        <source>all</source>
        <translation type="vanished">全部</translation>
    </message>
    <message>
        <source>file folder</source>
        <translation type="vanished">文件夹</translation>
    </message>
    <message>
        <source>image</source>
        <translation type="vanished">图片</translation>
    </message>
    <message>
        <source>video</source>
        <translation type="vanished">视频</translation>
    </message>
    <message>
        <source>text file</source>
        <translation type="vanished">文本</translation>
    </message>
    <message>
        <source>audio</source>
        <translation type="vanished">音频</translation>
    </message>
    <message>
        <source>others</source>
        <translation type="vanished">其它</translation>
    </message>
    <message>
        <source>wps file</source>
        <translation type="vanished">WPS文件</translation>
    </message>
    <message>
        <source>today</source>
        <translation type="vanished">今天</translation>
    </message>
    <message>
        <source>this week</source>
        <translation type="vanished">本周</translation>
    </message>
    <message>
        <source>this month</source>
        <translation type="vanished">本月</translation>
    </message>
    <message>
        <source>this year</source>
        <translation type="vanished">今年</translation>
    </message>
    <message>
        <source>year ago</source>
        <translation type="vanished">去年</translation>
    </message>
    <message>
        <source>tiny(0-16K)</source>
        <translation type="vanished">极小(0-16K)</translation>
    </message>
    <message>
        <source>small(16k-1M)</source>
        <translation type="vanished">较小(16k-1M)</translation>
    </message>
    <message>
        <source>medium(1M-100M)</source>
        <translation type="vanished">中等(1M-100M)</translation>
    </message>
    <message>
        <source>big(100M-1G)</source>
        <translation type="vanished">较大(100M-1G)</translation>
    </message>
    <message>
        <source>large(&gt;1G)</source>
        <translation type="vanished">极大(&gt;1G)</translation>
    </message>
</context>
<context>
    <name>Peony::AdvancedLocationBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/advanced-location-bar.cpp" line="187"/>
        <source>Search Content...</source>
        <translation>བཤེར་འཚོལ་ནང་དོན།</translation>
    </message>
</context>
<context>
    <name>Peony::AdvancedPermissionsPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="654"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="872"/>
        <source>Permission refinement settings</source>
        <translation>དབང་ཚད་ཞིབ་ཕྲ་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="767"/>
        <source>Permission refinement settings tip</source>
        <translation>དབང་ཚད་ཞིབ་ཕྲ་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="767"/>
        <source>Setting ACL permissions will result in a change in the user group permissions for basic permissions. Do you need to continue setting ACL permissions?</source>
        <translation>ACLཡི་དབང་ཚད་སྒྲིག་འགོད་བྱས་ན་གཞི་རྩའི་དབང་ཚད་ཀྱི་སྤྱོད་མཁན་ཚོ་སྐོར་གྱི་དབང་ཚད་ལ་འགྱུར་བ་འབྱུང་སྲིད་པས།ACLམུ་མཐུད་དུ་དབང་ཚད་སྒྲིག་འགོད་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="799"/>
        <source>User</source>
        <translation>སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="799"/>
        <source>Read</source>
        <translation>འདོན་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="799"/>
        <source>Write</source>
        <translation>འབྲི།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="799"/>
        <source>Executable</source>
        <translation>ལག་བསྟར།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="884"/>
        <source>delete</source>
        <translation>སུབ་པ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="886"/>
        <source>Inherit permission</source>
        <translation>ཤུལ་འཛིན་གྱི་དབང་ཚད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="901"/>
        <source>Add</source>
        <translation>ཁ་སྣོན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="918"/>
        <source>Apply</source>
        <translation>བེད་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="919"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
</context>
<context>
    <name>Peony::AllFileLaunchDialog</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="341"/>
        <source>Choose new application</source>
        <translation>བཀོལ་སྤྱོད་གསར་བ་ཞིག་འདེམས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="343"/>
        <source>Choose an Application to open this file</source>
        <translation>ཉེར་སྤྱོད་ཅིག་བདམས་ནས་ཡིག་ཆ་འདིའི་ཁ་ཕྱེས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="350"/>
        <source>apply now</source>
        <translation>ལམ་སེང་བེད་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="356"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="357"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
</context>
<context>
    <name>Peony::AudioPlayManager</name>
    <message>
        <source>Operation file Warning</source>
        <translation type="vanished">文件操作警告</translation>
    </message>
</context>
<context>
    <name>Peony::BasicPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="969"/>
        <source>Choose a custom icon</source>
        <translation>རང་གིས་རང་ལ་མཚན་ཉིད་འཇོག་པའི་རི་མོ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="401"/>
        <source>Type:</source>
        <translation>རིགས་ནི།</translation>
    </message>
    <message>
        <source>Display Name:</source>
        <translation type="vanished">名称：</translation>
    </message>
    <message>
        <source>Location:</source>
        <translation type="vanished">路径：</translation>
    </message>
    <message>
        <source>Overview:</source>
        <translation type="vanished">概览：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="244"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="245"/>
        <source>Change</source>
        <translation>བཟོ་བཅོས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="299"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="300"/>
        <source>Location</source>
        <translation>གནས་ས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="343"/>
        <source>move</source>
        <translation>གནས་སྤོ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="407"/>
        <source>symbolLink</source>
        <translation>མྱུར་བའི་ཐབས་ལམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="411"/>
        <source>Folder</source>
        <translation>ཡིག་སྒམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="418"/>
        <source>Include:</source>
        <translation>དེའི་ནང་དུ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="422"/>
        <source>Open with:</source>
        <translation>རྒྱག་སྟངས་ནི།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="428"/>
        <source>Description:</source>
        <translation>གསལ་བཤད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="440"/>
        <source>Select multiple files</source>
        <translation>ཡིག་ཆ་མང་པོ་འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="445"/>
        <source>Size:</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>Total size:</source>
        <translation type="vanished">实际大小：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="446"/>
        <source>Space Useage:</source>
        <translation>བར་སྟོང་བཟུང་སྤྱོད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>yyyy-MM-dd, HH:mm:ss</source>
        <translation type="vanished">yyyy年MM月dd日, HH:mm:ss</translation>
    </message>
    <message>
        <source>yyyy-MM-dd, hh:mm:ss AP</source>
        <translation type="vanished">yyyy年MM月dd日, hh:mm:ss AP</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="488"/>
        <source>Time Created:</source>
        <translation>གསར་འཛུགས་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="180"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="492"/>
        <source>Time Modified:</source>
        <translation>བཟོ་བཅོས་རྒྱག་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="493"/>
        <source>Time Access:</source>
        <translation>འཚམས་འདྲིའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="517"/>
        <source>Readonly</source>
        <translation>ཀློག་པ་ཙམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="518"/>
        <source>Hidden</source>
        <translation>སྦས་གསང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="540"/>
        <source>Property:</source>
        <translation>གཏོགས་གཤིས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="629"/>
        <source>usershare</source>
        <translation>འཕྲུལ་ཆས་འདི་མཉམ་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="760"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1023"/>
        <source>%1 (%2 Bytes)</source>
        <translation>%(%ཡིག་ཚིགས2)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="797"/>
        <source>Choose a new folder:</source>
        <translation>ཡིག་ཆའི་སྒམ་གསར་བ་ཞིག་འདེམ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="804"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="809"/>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="804"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="809"/>
        <source>cannot move a folder to itself !</source>
        <translation>དེའི་ནང་དུ་ཡིག་སྣོད་གཅིག་ཀྱང་སྤོ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="977"/>
        <source>Please select a image that is smaller than 1MB.</source>
        <translation>ཁྱོད་ཀྱིས་ཡང་བསྐྱར་1MBལས་ཆུང་པའི་པར་འདེམས་རོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1016"/>
        <source>%1 Bytes</source>
        <translation>%ཡིག་ཚིགས།</translation>
    </message>
    <message>
        <source>%1 KB (%2 Bytes)</source>
        <translation type="vanished">%1 KB (%2 字节)</translation>
    </message>
    <message>
        <source>%1 MB (%2 Bytes)</source>
        <translation type="vanished">%1 MB (%2 字节)</translation>
    </message>
    <message>
        <source>%1 GB (%2 Bytes)</source>
        <translation type="vanished">%1 GB (%2 字节)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1032"/>
        <source>%1 files, %2 folders</source>
        <translation>%ཡིག་ཆ1%ཡིག་ཆ2</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1137"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1139"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1144"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1146"/>
        <source>Can&apos;t get remote file information</source>
        <translation>ཐག་རིང་གི་ཡིག་ཆའི་ཆ་འཕྲིན་ཐོབ་མ་ཐུབ།</translation>
    </message>
    <message>
        <source>%1 files (include root files), %2 hidden</source>
        <translation type="vanished">共%1个文件（包括顶层目录），有%2个隐藏文件</translation>
    </message>
    <message>
        <source>%1 total</source>
        <translation type="vanished">共%1</translation>
    </message>
</context>
<context>
    <name>Peony::ComputerPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="102"/>
        <source>CPU Name:</source>
        <translation>སྒྲིག་གཅོད་ཆས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="103"/>
        <source>CPU Core:</source>
        <translation>ལྟེ་བའི་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="104"/>
        <source>Memory Size:</source>
        <translation>ནང་གསོག</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="116"/>
        <source>User Name: </source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་། </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="117"/>
        <source>Desktop: </source>
        <translation>ཅོག་ངོས་ཁོར་ཡུག </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="125"/>
        <source>You should mount this volume first</source>
        <translation>ཁྱོད་ཀྱིས་རྒྱུགས་ཤོག་འདི་བཞག་ན་ད་གཟོད་ཆ་འཕྲིན་ལ་བལྟ་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="142"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="240"/>
        <source>Name: </source>
        <translation>ཡན་ལག་ས་ཁུལ་གྱི་མིང་། </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="142"/>
        <source>File System</source>
        <translation>ཡིག་ཆའི་རྒྱུད་ཁོངས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="142"/>
        <source>Data</source>
        <translation>གཞི་གྲངས་སྡེར།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="143"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="245"/>
        <source>Total Space: </source>
        <translation>སྤྱིའི་ཤོང་ཚད་ནི། </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="144"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="246"/>
        <source>Used Space: </source>
        <translation>བཀོལ་སྤྱོད་བར་སྟོང་། </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="145"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="247"/>
        <source>Free Space: </source>
        <translation>ལྷག་མའི་བར་སྟོང་། </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="146"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="249"/>
        <source>Type: </source>
        <translation>ཡིག་ཆའི་མ་ལག </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="262"/>
        <source>Kylin Burner</source>
        <translation>རྐོས་མ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="268"/>
        <source>Open with: 	</source>
        <translation>ཁ་འབྱེད་རོགས། 	</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="275"/>
        <source>Unknown</source>
        <translation>ཤེས་མེད་པའི་ཡན་ལག་ས་ཁུལ།</translation>
    </message>
</context>
<context>
    <name>Peony::ConnectServerDialog</name>
    <message>
        <source>connect to server</source>
        <translation type="vanished">连接服务器</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="143"/>
        <source>Connect to server</source>
        <translation>སྦྲེལ་མཐུད་ཞབས་ཞུའི་འཕྲུལ་ཆས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="168"/>
        <source>Ip</source>
        <translation>ཞབས་ཞུའི་འཕྲུལ་ཆས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="170"/>
        <source>Port</source>
        <translation>མཐུད་སྣེ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="171"/>
        <source>Type</source>
        <translation>རིགས་དབྱེ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="215"/>
        <source>Add</source>
        <translation>ཁ་སྣོན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="216"/>
        <source>Delete</source>
        <translation>སུབ་པ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="217"/>
        <source>Connect</source>
        <translation>སྦྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="296"/>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="387"/>
        <source>Ip input error, please re-enter!</source>
        <translation>ipགཏགས་ནོར་བྱུང་སོང་།ཡང་བསྐྱར་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="300"/>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="391"/>
        <source>Port input error, please re-enter!</source>
        <translation>ཁ་ལ་ནོར་འཁྲུལ་བྱུང་ན།ཡང་བསྐྱར་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>ip</source>
        <translation type="vanished">服务器</translation>
    </message>
    <message>
        <source>port</source>
        <translation type="vanished">端口</translation>
    </message>
    <message>
        <source>type</source>
        <translation type="vanished">类型</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="202"/>
        <source>Personal Collection server:</source>
        <translation>མི་སྒེར་གྱི་ཉར་ཚགས་ཞབས་ཞུའི་འཕྲུལ་ཆས།</translation>
    </message>
    <message>
        <source>add</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>delete</source>
        <translatorcomment>连接</translatorcomment>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <source>connect</source>
        <translation type="vanished">连接</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="296"/>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="300"/>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="387"/>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="391"/>
        <source>Warning</source>
        <translation>ཉེན་བརྡ།</translation>
    </message>
    <message>
        <source>ip input error, please re-enter!</source>
        <translation type="vanished">ip输入错误，请重新输入！</translation>
    </message>
    <message>
        <source>port input error, please re-enter!</source>
        <translation type="vanished">端口输入错误，请重新输入！</translation>
    </message>
</context>
<context>
    <name>Peony::ConnectServerLogin</name>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="484"/>
        <source>The login user</source>
        <translation>ཐོ་འཇུག་ཐོབ་ཐང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="493"/>
        <source>Please enter the %1&apos;s user name and password of the server.</source>
        <translation>ཞབས་ཞུ་ཆས་%ཡི་སྤྱོད་མཁན་གྱི་མིང་དང་གསང་གྲངས་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="500"/>
        <source>User&apos;s identity</source>
        <translation>ཐོབ་ཐང་འབྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="501"/>
        <source>Guest</source>
        <translation>ཡུལ་སྐོར་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="517"/>
        <source>Name</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="518"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="539"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="540"/>
        <source>OK</source>
        <translation>སྦྲེལ་མཐུད།</translation>
    </message>
    <message>
        <source>guest</source>
        <translation type="vanished">游客（匿名登录）</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="502"/>
        <source>Registered users</source>
        <translation>ཐོ་འགོད་སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <source>name</source>
        <translation type="vanished">用户名</translation>
    </message>
    <message>
        <source>password</source>
        <translation type="vanished">密码</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="519"/>
        <source>Remember the password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>ok</source>
        <translation type="vanished">连接</translation>
    </message>
</context>
<context>
    <name>Peony::CreateLinkInternalPlugin</name>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="124"/>
        <source>Create Link to Desktop</source>
        <translation>ཅོག་ངོས་སུ་བསྐུར་ན་མགྱོགས་པའི་ཐབས་ཤེས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="150"/>
        <source>Create Link to...</source>
        <translation>བསྐུར་ན་མགྱོགས་པའི་ཐབས་ལ་བརྟེན་ནས་ཐོན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="153"/>
        <source>Choose a Directory to Create Link</source>
        <translation>འབྲེལ་མཐུད་བྱེད་པའི་དཀར་ཆག་འདེམས་དགོས།</translation>
    </message>
    <message>
        <source>Peony-Qt Create Link Extension</source>
        <translation type="vanished">创建链接</translation>
    </message>
    <message>
        <source>Create Link Menu Extension.</source>
        <translation type="vanished">创建链接.</translation>
    </message>
</context>
<context>
    <name>Peony::CreateSharedFileLinkMenuPlugin</name>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="242"/>
        <source>Create Link to Desktop</source>
        <translation>ཅོག་ངོས་སུ་བསྐུར་ན་མགྱོགས་པའི་ཐབས་ཤེས།</translation>
    </message>
</context>
<context>
    <name>Peony::CreateTemplateOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="62"/>
        <source>NewFile</source>
        <translation>གསར་སྐྲུན་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="78"/>
        <source>Create file</source>
        <translation>ཡིག་ཆ་གསར་གཏོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="90"/>
        <source>NewFolder</source>
        <translation>ཡིག་སྒམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="109"/>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="142"/>
        <source>Create file error</source>
        <translation>ཡིག་ཆའི་ནོར་འཁྲུལ་འཛུགས་པ།</translation>
    </message>
</context>
<context>
    <name>Peony::CustomErrorHandler</name>
    <message>
        <location filename="../../libpeony-qt/custom-error-handler.cpp" line="40"/>
        <source>Is Error Handled?</source>
        <translation>ནོར་འཁྲུལ་ནི་ཐག་གཅོད་བྱས་ཡོད་དམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/custom-error-handler.cpp" line="45"/>
        <source>Error not be handled correctly</source>
        <translation>ནོར་འཁྲུལ་ཡང་དག་པའི་སྒོ་ནས་ཐག་གཅོད་བྱས་མེད།</translation>
    </message>
</context>
<context>
    <name>Peony::DefaultOpenWithWidget</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="400"/>
        <source>No default app</source>
        <translation>ཐོག་མའི་ཁས་ལེན་བྱེད་སྟངས་བཀོད་སྒྲིག་བྱས་མེད།</translation>
    </message>
</context>
<context>
    <name>Peony::DefaultPreviewPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="76"/>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="217"/>
        <source>Select the file you want to preview...</source>
        <translation>ཁྱོད་ཀྱིས་སྔོན་ལ་ལྟ་འདོད་པའི་ཡིག་ཆ་འདེམས་རོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="208"/>
        <source>Can not preview this file.</source>
        <translation>ཡིག་ཆ་འདི་སྔོན་ལ་ལྟ་མི་རུང་།</translation>
    </message>
    <message>
        <source>Can not preivew this file.</source>
        <translation type="vanished">不能预览该文件</translation>
    </message>
</context>
<context>
    <name>Peony::DefaultPreviewPageFactory</name>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page-factory.h" line="50"/>
        <source>Default Preview</source>
        <translation>ཆ་འཕྲིན་ཞིབ་ཕྲ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page-factory.h" line="53"/>
        <source>This is the Default Preview of peony-qt</source>
        <translation>ཡིག་ཆའི་ཞིབ་ཕྲའི་བརྡ་འཕྲིན།</translation>
    </message>
</context>
<context>
    <name>Peony::DetailsPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="180"/>
        <source>Name:</source>
        <translation>མིང་ལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="183"/>
        <source>File type:</source>
        <translation>ཡིག་ཆའི་རིགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="199"/>
        <source>Location:</source>
        <translation>འགྲོ་ལམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="213"/>
        <source>yyyy-MM-dd, HH:mm:ss</source>
        <translation>yyyyལོ།MMཟླ་ddཉིན།HH：mm:ss</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="205"/>
        <source>Create time:</source>
        <translation>གསར་འཛུགས་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="210"/>
        <source>Modify time:</source>
        <translation>བཟོ་བཅོས་རྒྱག་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <source>yyyy-MM-dd, hh:mm:ss AP</source>
        <translation type="vanished">yyyy年MM月dd日, hh:mm:ss AP</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="234"/>
        <source>File size:</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="241"/>
        <source>Width:</source>
        <translation>ཞེང་ཚད་ནི།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="244"/>
        <source>Height:</source>
        <translation>མཐོ་ཚད་ནི།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="252"/>
        <source>Owner</source>
        <translation>ཡོད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="253"/>
        <source>Owner:</source>
        <translation>ཡོད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="255"/>
        <source>Computer</source>
        <translation>རྩིས་འཁོར།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="256"/>
        <source>Computer:</source>
        <translation>རྩིས་འཁོར།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="311"/>
        <source>%1 (this computer)</source>
        <translation>%གློག་ཀླད་འདི།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="318"/>
        <source>Unknown</source>
        <translation>ཤེས་མེད་པའི་ཡན་ལག་ས་ཁུལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="348"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="349"/>
        <source>Can&apos;t get remote file information</source>
        <translation>རྒྱང་བསྟེན་ཡིག་ཆའི་ཆ་འཕྲིན་ཐོབ་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="358"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="359"/>
        <source>%1 px</source>
        <translation>%བརྙན་རིས།</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::IconView</name>
    <message>
        <source>Icon View</source>
        <translation type="vanished">图标视图</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/icon-view/icon-view.cpp" line="316"/>
        <source>warn</source>
        <translation>ཉེན་བརྡ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/icon-view/icon-view.cpp" line="316"/>
        <source>This operation is not supported.</source>
        <translation>བཀོལ་སྤྱོད་འདི་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::IconView2</name>
    <message>
        <source>Icon View</source>
        <translation type="vanished">图标视图</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::ListView</name>
    <message>
        <source>List View</source>
        <translation type="vanished">列表视图</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/list-view/list-view.cpp" line="565"/>
        <source>warn</source>
        <translation>ཉེན་བརྡ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/list-view/list-view.cpp" line="565"/>
        <source>This operation is not supported.</source>
        <translation>བཀོལ་སྤྱོད་འདི་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::ListView2</name>
    <message>
        <source>List View</source>
        <translation type="vanished">列表视图</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryViewFactoryManager</name>
    <message>
        <source>Icon View</source>
        <translation type="vanished">图标视图</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryViewMenu</name>
    <message>
        <source>Open in &amp;New Window</source>
        <translation type="vanished">在新窗口中打开(&amp;N)</translation>
    </message>
    <message>
        <source>Open in New &amp;Tab</source>
        <translation type="vanished">在新标签页中打开(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="343"/>
        <source>Add to bookmark</source>
        <translation>མགྱོགས་མྱུར་འཚམས་འདྲི་བྱེད་པ།</translation>
    </message>
    <message>
        <source>&amp;Open &quot;%1&quot;</source>
        <translation type="vanished">打开“%1”(&amp;O)</translation>
    </message>
    <message>
        <source>Open &quot;%1&quot; in &amp;New Window</source>
        <translation type="vanished">在新窗口中打开“%1”(&amp;N)</translation>
    </message>
    <message>
        <source>Open &quot;%1&quot; in New &amp;Tab</source>
        <translation type="vanished">在新标签页中打开“%1”(&amp;T)</translation>
    </message>
    <message>
        <source>Open &quot;%1&quot; with...</source>
        <translation type="vanished">选用其它应用打开“%1”...</translation>
    </message>
    <message>
        <source>&amp;More applications...</source>
        <translation type="vanished">更多应用...(&amp;M)</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation type="vanished">打开(&amp;O)</translation>
    </message>
    <message>
        <source>Open &amp;with...</source>
        <translation type="vanished">打开方式(&amp;W)...</translation>
    </message>
    <message>
        <source>&amp;Open %1 selected files</source>
        <translation type="vanished">打开%1个选中文件(&amp;O)</translation>
    </message>
    <message>
        <source>&amp;New...</source>
        <translation type="vanished">新建...(&amp;N)</translation>
    </message>
    <message>
        <source>Empty &amp;File</source>
        <translation type="vanished">空文件(&amp;E)</translation>
    </message>
    <message>
        <source>&amp;Folder</source>
        <translation type="vanished">文件夹(&amp;F)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="655"/>
        <source>New Folder</source>
        <translation>ཡིག་སྒམ།</translation>
    </message>
    <message>
        <source>Icon View</source>
        <translation type="vanished">图标视图</translation>
    </message>
    <message>
        <source>List View</source>
        <translation type="vanished">列表视图</translation>
    </message>
    <message>
        <source>View Type...</source>
        <translation type="vanished">视图类型...</translation>
    </message>
    <message>
        <source>Sort By...</source>
        <translation type="vanished">排序类型...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="707"/>
        <source>Name</source>
        <translation>ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="709"/>
        <source>File Type</source>
        <translation>ཡིག་ཆའི་རིགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="710"/>
        <source>File Size</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>New...</source>
        <translation type="vanished">新建...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="307"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="396"/>
        <source>Open in New Window</source>
        <translation>སྒེའུ་ཁུང་གསར་བའི་ནང་ནས་ཁ་ཕྱེས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="316"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="405"/>
        <source>Open in New Tab</source>
        <translation>ཤོག་བྱང་གསར་བའི་ངོས་སུ་ཁ་ཕྱེས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="363"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="416"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="480"/>
        <source>Open</source>
        <translation>ཁ་ཕྱེ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="374"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="432"/>
        <source>Open with...</source>
        <translation>ཁ་འབྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="389"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="469"/>
        <source>More applications...</source>
        <translation>དེ་ལས་མང་བའི་བེད་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="489"/>
        <source>Open %1 selected files</source>
        <translation>གདམ་ཐོན་བྱུང་བའི་ཡིག་ཆ་1གི་ཁ་ཕྱེས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="538"/>
        <source>New</source>
        <translation>གསར་འཛུགས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="639"/>
        <source>Empty File</source>
        <translation>ཡིག་རྐྱང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="651"/>
        <source>Folder</source>
        <translation>ཡིག་སྒམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="681"/>
        <source>View Type</source>
        <translation>མཐོང་རིས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="701"/>
        <source>Sort By</source>
        <translation>གོ་རིམ་རིགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="708"/>
        <source>Modified Date</source>
        <translation>བཟོ་བཅོས་བརྒྱབ་པའི་ཚེས་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="711"/>
        <source>Orignal Path</source>
        <translation>གདོད་མའི་ཐབས་ལམ།</translation>
    </message>
    <message>
        <source>Sort Order...</source>
        <translation type="vanished">排序顺序...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="744"/>
        <source>Ascending Order</source>
        <translation>རིམ་འཕར།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="743"/>
        <source>Descending Order</source>
        <translation>རིམ་ཕབ།</translation>
    </message>
    <message>
        <source>Sort Preferences...</source>
        <translation type="vanished">排序偏好...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="762"/>
        <source>Folder First</source>
        <translation>ཡིག་སྒམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="771"/>
        <source>Chinese First</source>
        <translation>རྒྱ་ཡིག་སྔོན་ལ་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="780"/>
        <source>Show Hidden</source>
        <translation>མངོན་མེད་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="815"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="823"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="985"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1228"/>
        <source>Copy</source>
        <translation>འདྲ་བཟོ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1288"/>
        <source>File:&quot;%1&quot; is not exist, did you moved or deleted it?</source>
        <translation>ཡིག་ཆ%sམེད་པ་རེད།ཁྱོད་ཀྱིས་བསུབ་པ་དང་ཡང་ན་གནས་གཞན་དུ་སྤོས་པ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1323"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1333"/>
        <source>Peony-Qt filesafe menu Extension</source>
        <translation>ཡིག་ཆའི་སྲུང་སྐྱོབ་སྒམ་རྒྱ་བསྐྱེད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1323"/>
        <source>Peony File Labels Menu Extension</source>
        <translation>ཡིག་ཆའི་མཚོན་རྟགས།</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="vanished">复制(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="853"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1233"/>
        <source>Cut</source>
        <translation>དྲས་གཏུབ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="880"/>
        <source>Delete to trash</source>
        <translation>ཕྱིར་སྡུད་ས་ཚིགས་སུ་སུབ་པ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="939"/>
        <source>Paste</source>
        <translation>སྦྱར་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="998"/>
        <source>Refresh</source>
        <translation>གསར་འདོན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1009"/>
        <source>Select All</source>
        <translation>ཚང་མ་འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1053"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1090"/>
        <source>Properties</source>
        <translation>གཏོགས་གཤིས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1127"/>
        <source>format</source>
        <translation>རྣམ་གཞག་ཅན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1181"/>
        <source>Restore</source>
        <translation>སྔར་གྱི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="892"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="979"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1208"/>
        <source>Delete</source>
        <translation>སུབ་པ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="736"/>
        <source>Sort Order</source>
        <translation>གོ་རིམ་སྒྲིག་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="757"/>
        <source>Sort Preferences</source>
        <translation>གོ་རིམ་ཡག་པོ་སྒྲིག་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1287"/>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>File:&quot;%1 is not exist, did you moved or deleted it?</source>
        <translation type="vanished">文件：&quot;%1&quot; 不存在，您是否已经移动或者删除了它？</translation>
    </message>
    <message>
        <source>File original path not exist, are you deleted or moved it?</source>
        <translation type="vanished">文件原始路径未找到，您是否已经移动或删除了它？</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="vanished">剪切(&amp;T)</translation>
    </message>
    <message>
        <source>&amp;Delete to trash</source>
        <translation type="vanished">删除到回收站(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="895"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="906"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="914"/>
        <source>Delete forever</source>
        <translation>ཡུན་རིང་བསུབ་རྒྱུ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="922"/>
        <source>Rename</source>
        <translation>མིང་བཏགས་པ།</translation>
    </message>
    <message>
        <source>Select &amp;All</source>
        <translation type="vanished">全选(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1018"/>
        <source>Reverse Select</source>
        <translation>ལྡོག་འདེམས།</translation>
    </message>
    <message>
        <source>P&amp;roperties</source>
        <translation type="vanished">属性(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">删除(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="vanished">重命名(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="vanished">粘贴(&amp;P)</translation>
    </message>
    <message>
        <source>&amp;Refresh</source>
        <translation type="vanished">刷新(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Properties</source>
        <translation type="vanished">属性(&amp;P)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1154"/>
        <source>&amp;Clean the Trash</source>
        <translation>བར་སྣང་ཕྱིར་སྡུད་ས་ཚིགས།</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <source>Delete Permanently</source>
        <translation type="vanished">永久删除</translation>
    </message>
    <message>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="vanished">您确定要删除这些文件吗？一旦开始删除，这些文件将不可再恢复。</translation>
    </message>
    <message>
        <source>&amp;Restore</source>
        <translation type="vanished">还原(&amp;R)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1241"/>
        <source>Clean All</source>
        <translation>ཡོད་ཚད་གཙང་དག་བྱས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1258"/>
        <source>Open Parent Folder in New Window</source>
        <translation>སྒེའུ་ཁུང་གསར་བའི་ནང་ནས་ཡིག་ཆ་ཡོད་སའི་དཀར་ཆག་ཕྱེ།</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryViewWidget</name>
    <message>
        <source>Directory View</source>
        <translation type="vanished">文件视图</translation>
    </message>
</context>
<context>
    <name>Peony::ExtensionsManagerWidget</name>
    <message>
        <location filename="../../libpeony-qt/extensions-manager-widget.cpp" line="42"/>
        <source>Extensions Manager</source>
        <translation>བསྒར་ལྷུ་དོ་དམ་སྒྲིག་འགོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/extensions-manager-widget.cpp" line="46"/>
        <source>Available extensions</source>
        <translation>བཀོལ་སྤྱོད་བྱས་ཆོག་པའི་བསྒར་ཆས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/extensions-manager-widget.cpp" line="48"/>
        <source>Ok</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/extensions-manager-widget.cpp" line="49"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
</context>
<context>
    <name>Peony::FMWindow</name>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="92"/>
        <source>File Manager</source>
        <translation>ཡིག་ཆའི་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="170"/>
        <source>advanced search</source>
        <translation>མཐོ་རིམ་བཤེར་འཚོལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="173"/>
        <source>clear record</source>
        <translation>བར་སྣང་གི་ལོ་རྒྱུས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="278"/>
        <source>Loaing... Press Esc to stop a loading.</source>
        <translation>ད་ལྟ་ཁ་སྣོན་བྱེད་བཞིན་ཡོད།Escམནན་ནས་མེད་པར་གཏོང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="394"/>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation>རྩོམ་པ་པོ།YueLan〈lanyue.kylinos.cn〉MeihongHe〈hemeihong.kylinos.cn〉པར་དབང་ཡོད་ཚད།（C:2019ལོ་ནས་2020ལོའི་བར།ཐེན་ཅིན་ཆི་ལིན་ཆ་འཕྲིན་ལག་རྩལ་ཚད་ཡོད་ཀུང་སི།</translation>
    </message>
    <message>
        <source>Author: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <source>Ctrl+H</source>
        <comment>Show|Hidden</comment>
        <translation type="vanished">Ctrl+H</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="323"/>
        <source>Undo</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="330"/>
        <source>Redo</source>
        <translation>བསྐྱར་སྒྲུབ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="393"/>
        <source>Peony Qt</source>
        <translation>ཡིག་ཆའི་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="447"/>
        <source>New Folder</source>
        <translation>ཡིག་སྒམ།</translation>
    </message>
</context>
<context>
    <name>Peony::FileBatchRenameOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-batch-rename-operation.cpp" line="53"/>
        <source>File Rename error</source>
        <translation>ཡིག་ཆའི་བསྐྱར་དུ་མིང་བཏགས་པའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-batch-rename-operation.cpp" line="54"/>
        <source>Invalid file name %1%2%3 .</source>
        <translation>ཁྲིམས་དང་མི་མཐུན་པའི་ཡིག་ཆའི་མིང་བརྒྱ་ཆ12</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-batch-rename-operation.cpp" line="69"/>
        <source>File Rename warning</source>
        <translation>ཡིག་ཆ་ལ་བསྐྱར་དུ་མིང་བཏགས་ནས་ཉེན་བརྡ་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-batch-rename-operation.cpp" line="70"/>
        <source>Are you sure to hidden these files?</source>
        <translation>ཡིག་ཆ་འདི་དག་སྦས་དགོས་པ་ཁོ་ཐག་ཡིན་ནམ།</translation>
    </message>
    <message>
        <source>The file %1%2%3 will be hidden when you refresh or change directory!</source>
        <translation type="vanished">文件 %1%2%3 在刷新或者切换路径后将会被隐藏!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-batch-rename-operation.cpp" line="184"/>
        <location filename="../../libpeony-qt/file-operation/file-batch-rename-operation.cpp" line="219"/>
        <source>Rename file error</source>
        <translation>བསྐྱར་དུ་མིང་བཏགས་པའི་ཡིག་ཆའི་ནོར་འཁྲུལ།</translation>
    </message>
</context>
<context>
    <name>Peony::FileCopy</name>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="170"/>
        <location filename="../../libpeony-qt/file-copy.cpp" line="178"/>
        <location filename="../../libpeony-qt/file-copy.cpp" line="198"/>
        <source>Error in source or destination file path!</source>
        <translation>མ་ཁུངས་གནས་སའམ་ཡང་ན་དམིགས་ཚད་ཡིག་ཆའི་ཐབས་ལམ་ནོར་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="187"/>
        <source>Error when copy file: %1, can not copy special files, skip this file and continue?</source>
        <translation>ཡིག་ཆ་མཁོ་ཕབ་བྱེད་པ་སྟེ།%དུས་ཚོད་1གི་སྟེང་ནོར་འཁྲུལ་བྱུང་བ་དང་།དམིགས་བསལ་གྱི་ཡིག་ཆ་མཁོ་ཕབ་བྱེད་མི་ཐུབ།ཡིག་ཆ་འདི་ལས་བརྒལ་ནས་མུ་མཐུད་དུ་བྱེད་དམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="210"/>
        <source>Can not copy %1, file doesn&apos;t exist. Has the file been renamed or moved?</source>
        <translation>བརྒྱ་ཆ་1མཁོ་ཕབ་བྱེད་ཐབས་བྲལ་བ་དང་།ཡིག་ཆ་མེད་པ།མིང་བཏགས་པའམ་སྤོ་བསྒྱུར་བྱས་ཡོད་དམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="232"/>
        <source>The dest file &quot;%1&quot; has existed!</source>
        <translation>དམིགས་འབེན་ཡིག་ཆ1%ཡོད་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="260"/>
        <source>Vfat/FAT32 file systems do not support a single file that occupies more than 4 GB space!</source>
        <translation>vfat/fat32ཡིག་ཆའི་མ་ལག་གིས་ཡིག་ཆ་རྐྱང་པས་བཟུང་བའི་བར་སྟོང་4gལས་ཆེ་བར་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="286"/>
        <source>Error writing to file: Input/output error</source>
        <translation>ཡིག་ཆའི་ནང་བྲིས་པར་ནོར་འཁྲུལ་བྱུང་ན།ནང་འཇུག་/ཕྱིར་གཏོང་གི་ནོར་འཁྲུལ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="471"/>
        <source>Failed to create %1. Please ensure if it is in root directory, or if the device supports gphoto2 protocol correctly.</source>
        <translation>ཡིག་ཆ1%ལ་ཕམ་ཁ་བྱུང་ན།རྩ་བའི་དཀར་ཆག་བཀོལ་སྤྱོད་བྱེད་མིན་དང་།ཡང་ན་སྒྲིག་ཆས་ཡང་དག་པའི་སྒོ་ནས་gphoto2ལ་རྒྱབ་སྐྱོར་བྱེད་མིན་ཐག་གཅོད་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="477"/>
        <source>Failed to create %1.</source>
        <translation>གསར་སྐྲུན་ཡིག་ཆའི་%ལ་ཕམ་ཁ་བྱུང་།</translation>
    </message>
    <message>
        <source>Error opening source or destination file!</source>
        <translation type="vanished">打开源文件或者目标文件出错！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="388"/>
        <source>Please check whether the device has been removed!</source>
        <translation>སྒྲིག་ཆས་གནས་སྤོས་ཡོད་མེད་གཏན་ཁེལ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="390"/>
        <source>Write file error: There is no avaliable disk space for device!</source>
        <translation>ཡིག་ཆའི་ནང་བྲིས་པར་ནོར་འཁྲུལ་བྱུང་ན་སྒྲིག་ཆས་ཐོག་སྤྱད་ཆོག་པའི་བར་སྟོང་འདང་ངེས་ཤིག་མེད།</translation>
    </message>
    <message>
        <source>Please confirm that the device controls are insufficient!</source>
        <translation type="vanished">请确认设备空间是否足够!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="466"/>
        <source>File opening failure</source>
        <translation>ཡིག་ཆའི་ཁ་ཕྱེ་ནས་ཕམ་སོང་།</translation>
    </message>
    <message>
        <source>Reading and Writing files are inconsistent!</source>
        <translation type="vanished">读和写文件不一致！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="277"/>
        <location filename="../../libpeony-qt/file-copy.cpp" line="405"/>
        <source>operation cancel</source>
        <translation>བཀོལ་སྤྱོད་མེད་པར་བཟོ་བ།</translation>
    </message>
</context>
<context>
    <name>Peony::FileCopyOperation</name>
    <message>
        <source>File copy</source>
        <translation type="vanished">文件复制</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="210"/>
        <source>Create folder %1 failed: %2</source>
        <translation>གསར་དུ་གཏོད་པའི་དཀར་ཆག%ཕམ་པ།%2</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="214"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="584"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="1041"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="1102"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="1279"/>
        <source>File copy error</source>
        <translation>ཡིག་ཆ་བསྐྱར་པར་བྱས་པ་ནོར་སོང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="228"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="255"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="609"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="633"/>
        <source>The file name exceeds the limit</source>
        <translation>ཡིག་ཆའི་མིང་གི་རིང་ཚད་ཚད་བཀག་ལས་བརྒལ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="592"/>
        <source>Cannot opening file, permission denied!</source>
        <translation>ཡིག་ཆ་ཁ་ཕྱེ་མི་ཐུབ།དབང་ཚད་མི་འདང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="594"/>
        <source>File:%1 was not found.</source>
        <translation>ཡིག་ཆ་མ་རྙེད།%1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="1028"/>
        <source>File System</source>
        <translation>ཡིག་ཆའི་རྒྱུད་ཁོངས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="1030"/>
        <source>Data</source>
        <translation>གཞི་གྲངས་སྡེར།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="1036"/>
        <source>%1 no space left on device. Copy file size: %2 GB, Space needed: %3 GB.</source>
        <translation>བརྒྱ་ཆ་1སྒྲིག་ཆས་བར་སྟོང་མི་འདང་བ།མཁོ་ཕབ་ཡིག་ཆའི་ཆེ་ཆུང་།%GBབར་སྟོང་།%GB</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="1147"/>
        <source>Link file error</source>
        <translation>ཡིག་ཆའི་སྦྲེལ་མཐུད་ལ་ཕམ་ཁ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="1281"/>
        <source>Burning does not support replacement</source>
        <translation>གནས་སྐབས་བརྗེ་སྤོར་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <source>Burn failed</source>
        <translation type="vanished">刻录失败</translation>
    </message>
</context>
<context>
    <name>Peony::FileDeleteOperation</name>
    <message>
        <source>File delete</source>
        <translation type="vanished">文件删除</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="84"/>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="110"/>
        <source>File delete error</source>
        <translation>ཡིག་ཆ་བསུབ་ནོར་ཐེབས་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="147"/>
        <source>Delete file error</source>
        <translation>ནོར་འཁྲུལ་སུབ་པ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="150"/>
        <source>Invalid Operation! Can not delete &quot;%1&quot;.</source>
        <translation>ཁྲིམས་དང་མི་མཐུན་པའི་བཀོལ་སྤྱོད།བརྒྱ་ཆའི༡བསུབ་མི་ཐུབ།</translation>
    </message>
</context>
<context>
    <name>Peony::FileEnumerator</name>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="575"/>
        <source>The password dialog box is canceled</source>
        <translation>གསང་གྲངས་ཁ་བརྡའི་སྒྲོམ་མེད་པར་བཟོས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="577"/>
        <source>Message recipient disconnected from message bus without replying!</source>
        <translation>བརྡ་འཕྲིན་བསྡུ་ལེན་མཁན་གྱིས་ལན་མ་བཏབ་པའི་གནས་ཚུལ་འོག་བརྡ་འཕྲིན་སྐུད་པ་དང་འབྲེལ་མཐུད་བྱས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="579"/>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>Did not find target path, do you move or deleted it?</source>
        <translation type="vanished">未找到目标路径，您是否已经移动或删除了它？</translation>
    </message>
</context>
<context>
    <name>Peony::FileInfo</name>
    <message>
        <location filename="../../libpeony-qt/file-info.cpp" line="279"/>
        <source>data</source>
        <translation>གཞི་གྲངས་སྡེར།</translation>
    </message>
</context>
<context>
    <name>Peony::FileInfoJob</name>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="275"/>
        <source>Trash</source>
        <translation>ཕྱིར་སྡུད་ས་ཚིགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="277"/>
        <source>Computer</source>
        <translation>རྩིས་འཁོར།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="279"/>
        <source>Network</source>
        <translation>དྲ་རྒྱ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="281"/>
        <source>Recent</source>
        <translation>ཉེ་ལམ།</translation>
    </message>
</context>
<context>
    <name>Peony::FileInformationLabel</name>
    <message>
        <source>File location:</source>
        <translation type="vanished">文件位置：</translation>
    </message>
    <message>
        <source>File size:</source>
        <translation type="vanished">文件大小：</translation>
    </message>
    <message>
        <source>Modify time:</source>
        <translation type="vanished">修改时间：</translation>
    </message>
</context>
<context>
    <name>Peony::FileInfosJob</name>
    <message>
        <location filename="../../libpeony-qt/file-infos-job.cpp" line="162"/>
        <source>Trash</source>
        <translation>ཕྱིར་སྡུད་ས་ཚིགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-infos-job.cpp" line="164"/>
        <source>Computer</source>
        <translation>རྩིས་འཁོར།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-infos-job.cpp" line="166"/>
        <source>Network</source>
        <translation>དྲ་རྒྱ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-infos-job.cpp" line="168"/>
        <source>Recent</source>
        <translation>ཉེ་ལམ།</translation>
    </message>
</context>
<context>
    <name>Peony::FileItem</name>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="246"/>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="306"/>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="318"/>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="326"/>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="293"/>
        <source>Open Link failed</source>
        <translation>སྦྲེལ་མཐུད་ཁ་ཕྱེ་ན་ཕམ་ཁ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="294"/>
        <source>File not exist, do you want to delete the link file?</source>
        <translation>དམིགས་འབེན་ཡིག་ཆ་མེད་པས།བསུབ་ན་ནུས་པ་མེད་པར་གཏོང་ཐུབ་བམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="307"/>
        <source>Can not open path &quot;%1&quot;，permission denied.</source>
        <translation>ཐབས་ལམ་གྱི&quot;%&quot;ཕམ་པར་བྱས།དབང་ཚད་དང་ལེན་མ་བྱས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="317"/>
        <source>Can not find path &quot;%1&quot;，are you moved or renamed it?</source>
        <translation>ཐབས་ལམ་མ་རྙེད་པ་རེད།&quot;%&quot;།ཁྱེད་རང་སྤོ་འགུལ་བྱས་པའམ་ཡང་ན་མིང་བཏགས་པ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <source>Can not find path &quot;%1&quot; .</source>
        <translation type="vanished">找不到路径: &quot;%1&quot; 。</translation>
    </message>
</context>
<context>
    <name>Peony::FileItemModel</name>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="356"/>
        <source>child(ren)</source>
        <translation>གཟུགས་ཁག</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="343"/>
        <source>Symbol Link, </source>
        <translation>མྱུར་ཞིང་མྱུར་བའི་ཐབས་ཤེས། </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="402"/>
        <source>File Name</source>
        <translation>ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="406"/>
        <source>Delete Date</source>
        <translation>སུབ་པ་ཟླ་ཚེས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="408"/>
        <source>Create Date</source>
        <translation>གསར་འཛུགས་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="415"/>
        <source>File Size</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="417"/>
        <source>Original Path</source>
        <translation>གདོད་མའི་ཐབས་ལམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="413"/>
        <source>File Type</source>
        <translation>ཡིག་ཆའི་རིགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="411"/>
        <source>Modified Date</source>
        <translation>བཟོ་བཅོས་བརྒྱབ་པའི་ཚེས་གྲངས།</translation>
    </message>
</context>
<context>
    <name>Peony::FileLabelInternalMenuPlugin</name>
    <message>
        <source>Add File Label...</source>
        <translation type="vanished">添加标记...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="190"/>
        <source>Add File Label</source>
        <translation>རྟགས་སྣོན་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="213"/>
        <source>Delete All Label</source>
        <translation>རྟགས་ཚང་མ་བསུབ་རོགས།</translation>
    </message>
    <message>
        <source>Peony File Labels Menu Extension</source>
        <translation type="vanished">文件标记</translation>
    </message>
    <message>
        <source>Tag a File with Menu.</source>
        <translation type="vanished">菜单中增加标记功能.</translation>
    </message>
</context>
<context>
    <name>Peony::FileLauchDialog</name>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="47"/>
        <source>Applications</source>
        <translation>ཉེར་སྤྱོད་བྱ་རིམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="48"/>
        <source>Choose an Application to open this file</source>
        <translation>ཉེར་སྤྱོད་ཅིག་བདམས་ནས་ཡིག་ཆ་འདིའི་ཁ་ཕྱེས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="56"/>
        <source>Set as Default</source>
        <translation>ཁས་བླངས་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="64"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="65"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
</context>
<context>
    <name>Peony::FileLaunchAction</name>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="144"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="251"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="447"/>
        <source>Execute Directly</source>
        <translation>ཐད་ཀར་འཁོར་སྐྱོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="145"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="252"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="448"/>
        <source>Execute in Terminal</source>
        <translation>མཐའ་སྣེར་འཁོར་སྐྱོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="148"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="256"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="452"/>
        <source>Detected launching an executable file %1, you want?</source>
        <translation>ལག་བསྟར་བྱེད་ཆོག་པའི་ཡིག་ཆ་ཞིག་གི་ཁ་ཕྱེ་བཞིན་ཡོད།ཁྱོད་ཀྱིས་རེ་བ་བྱེད་དམ།</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="166"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="290"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="486"/>
        <source>Open Failed</source>
        <translation>ཁ་ཕྱེ་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="166"/>
        <source>Can not open %1, file not exist, is it deleted?</source>
        <translation>བརྒྱ་ཆའི་༡གི་ཁ་འབྱེད་མི་ཐུབ།ཡིག་ཆ་མེད་པར་གྱུར་སོང་།བསུབ་ཡོད་མེད་གཏན་ཁེལ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>File not exist, is it deleted or moved to other path?</source>
        <translation type="vanished">文件不存在，您是否已将其删除或挪动位置？</translation>
    </message>
    <message>
        <source>Can not open %1</source>
        <translation type="vanished">不能打开%1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="250"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="446"/>
        <source>By Default App</source>
        <translation>ཐོག་མར་ཁས་ལེན་པའི་རྣམ་པ་སྤྱད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="255"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="451"/>
        <source>Launch Options</source>
        <translation>ལག་བསྟར་གདམ་བྱ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="279"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="475"/>
        <source>Open Link failed</source>
        <translation>སྦྲེལ་མཐུད་ཁ་ཕྱེ་ན་ཕམ་ཁ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="280"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="476"/>
        <source>File not exist, do you want to delete the link file?</source>
        <translation>དམིགས་འབེན་ཡིག་ཆ་མེད་པས།ཁྱོད་ཀྱིས་འབྲེལ་མཐུད་འདི་བསུབ་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="291"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="487"/>
        <source>Can not open %1, Please confirm you have the right authority.</source>
        <translation>བརྒྱ་ཆའི་༡གི་ཁ་འབྱེད་མི་ཐུབ།ཁྱོད་ལ་ཡང་དག་པའི་ཁ་འབྱེད་ཆོག་པའི་དབང་ཆ་ཡོད་པ་གཏན་ཁེལ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="295"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="492"/>
        <source>Open App failed</source>
        <translation>མྱུར་བའི་ཐབས་ལམ་ལ་གནད་དོན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="296"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="493"/>
        <source>The linked app is changed or uninstalled, so it can not work correctly. 
Do you want to delete the link file?</source>
        <translation>འབྲེལ་མཐུད་འདིས་སྟོན་པའི་བཀོལ་སྤྱོད་བཟོ་བཅོས་སམ་བསུབ་ཟིན།དེའི་རྐྱེན་གྱིས་བདེ་མྱུར་བྱེད་ཐབས་འདི་རྒྱུན་ལྡན་ལྟར་བྱ་བ་སྒྲུབ་མི་ཐུབ།མྱུར་མྱུར་འགྲུབ་བྱེད་ཐབས་འདི་བསུབ་རྒྱུ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="307"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="504"/>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="308"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="504"/>
        <source>Can not get a default application for opening %1, do you want open it with text format?</source>
        <translation>ཁས་བླངས་པ་དེས་བརྒྱ་ཆའི་གཅིག་གི་བཀོལ་སྤྱོད་མ་རྙེད།ཡིག་རྐྱང་རྩོམ་སྒྲིག་ཆས་ཀྱིས་ཁ་འབྱེད་པ་ཡིན་ནམ།</translation>
    </message>
</context>
<context>
    <name>Peony::FileLinkOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-link-operation.cpp" line="46"/>
        <location filename="../../libpeony-qt/file-operation/file-link-operation.cpp" line="49"/>
        <source>Symbolic Link</source>
        <translation>མྱུར་བའི་ཐབས་ལམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-link-operation.cpp" line="89"/>
        <source>Link file error</source>
        <translation>ཡིག་ཆའི་སྦྲེལ་མཐུད་ལ་ཕམ་ཁ་བྱུང་།</translation>
    </message>
    <message>
        <source>Link file</source>
        <translation type="vanished">创建文件链接</translation>
    </message>
</context>
<context>
    <name>Peony::FileMoveOperation</name>
    <message>
        <source>Invalid move operation, cannot move a file itself.</source>
        <translation type="vanished">非法的移动操作，不能自移动到自身。</translation>
    </message>
    <message>
        <source>Move file</source>
        <translation type="vanished">文件移动</translation>
    </message>
    <message>
        <source>Create file</source>
        <translation type="vanished">文件创建</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="184"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="399"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="519"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="848"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1753"/>
        <source>Move file error</source>
        <translation>སྤོ་འགུལ་ཡིག་ཆའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="286"/>
        <source>File System</source>
        <translation>ཡིག་ཆའི་རྒྱུད་ཁོངས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="288"/>
        <source>Data</source>
        <translation>གཞི་གྲངས་སྡེར།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="294"/>
        <source>%1 no space left on device. Copy file size: %2 GB, Space needed: %3 GB.</source>
        <translation>བརྒྱ་ཆ་1སྒྲིག་ཆས་བར་སྟོང་མི་འདང་བ།མཁོ་ཕབ་ཡིག་ཆའི་ཆེ་ཆུང་།%GBབར་སྟོང་།%GB</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="299"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1928"/>
        <source>File move error</source>
        <translation>སྤོ་འགུལ་ཡིག་ཆ་ལེགས་འགྲུབ་མ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="812"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="822"/>
        <source>Invalid move operation, cannot move a file into its sub directories.</source>
        <translation>ཁྲིམས་འགལ་གྱི་སྤོ་འགུལ་བཀོལ་སྤྱོད་ནི།ཡིག་ཆ་རང་ངོས་ཀྱི་ཐབས་ལམ་འོག་ཏུ་སྤོ་འགུལ་བྱེད་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="866"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="897"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="950"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1219"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1243"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1374"/>
        <source>The file name exceeds the limit</source>
        <translation>ཡིག་ཆའི་མིང་གི་རིང་ཚད་ཚད་བཀག་ལས་བརྒལ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1196"/>
        <source>Create file error</source>
        <translation>ཡིག་ཆའི་ནོར་འཁྲུལ་འཛུགས་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1202"/>
        <source>Cannot opening file, permission denied!</source>
        <translation>ཡིག་ཆ་ཁ་ཕྱེ་མི་ཐུབ།དབང་ཚད་མི་འདང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1204"/>
        <source>File:%1 was not found.</source>
        <translation>ཡིག་ཆ་མ་རྙེད།%1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="813"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1660"/>
        <source>Invalid Operation.</source>
        <translation>ཁྲིམས་འགལ་གྱི་བཀོལ་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1720"/>
        <source>File delete error</source>
        <translation>ཡིག་ཆ་བསུབ་ནོར་ཐེབས་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1798"/>
        <source>Link file error</source>
        <translation>ཡིག་ཆའི་སྦྲེལ་མཐུད་ལ་ཕམ་ཁ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1930"/>
        <source>Burning does not support replacement</source>
        <translation>གནས་སྐབས་བརྗེ་སྤོར་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <source>Burn failed</source>
        <translation type="vanished">刻录失败</translation>
    </message>
    <message>
        <source>File delete</source>
        <translation type="vanished">文件删除</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="820"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1722"/>
        <source>Invalid Operation</source>
        <translation>ཁྲིམས་འགལ་གྱི་བཀོལ་སྤྱོད།</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationAfterProgressPage</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="362"/>
        <source>&amp;More Details</source>
        <translation>ཞིབ་ཕྲའི་བརྡ་འཕྲིན།</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialog</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="45"/>
        <source>File Operation Error</source>
        <translation>ཡིག་ཆའི་བཀོལ་སྤྱོད་ནོར་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="53"/>
        <source>unkwon</source>
        <translation>ཤེས་མེད་པའི་རྒྱུ་རྐྱེན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="54"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="55"/>
        <source>null</source>
        <translation>སྟོང་ལུས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="57"/>
        <source>Error message:</source>
        <translation>ཆ་འཕྲིན་ནོར་འཁྲུལ་ཅན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="58"/>
        <source>Source File:</source>
        <translation>མ་ཁུངས་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="59"/>
        <source>Dest File:</source>
        <translation>དམིགས་འབེན་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="63"/>
        <source>Ignore</source>
        <translation>སྣང་མེད་དུ་བཞག་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="64"/>
        <source>Ignore All</source>
        <translation>ཚང་མ་སྣང་མེད་དུ་བཞག</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="65"/>
        <source>Overwrite</source>
        <translation>ཁེབས་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="66"/>
        <source>Overwrite All</source>
        <translation>ཁྱོན་ཡོངས་ལ་ཁྱབ་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="67"/>
        <source>Backup</source>
        <translation>གྲབས་ཉར།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="68"/>
        <source>Backup All</source>
        <translation>ཚང་མ་གྲབས་ཉར།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="69"/>
        <source>&amp;Retry</source>
        <translation>བསྐྱར་དུ་ཚོད་ལྟ་བྱས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="70"/>
        <source>&amp;Cancel</source>
        <translation>མེད་པར་བཟོས་སོང་།</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogBase</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog-base.cpp" line="68"/>
        <source>Close</source>
        <translation>སྒོ་གཏན་རོགས།</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogConflict</name>
    <message>
        <source>This location already contains a file with the same name.</source>
        <translation type="vanished">目标文件夹里已经包含有同名文件</translation>
    </message>
    <message>
        <source>Please select the file to keep</source>
        <translation type="vanished">请选择要保留的文件</translation>
    </message>
    <message>
        <source>This location already contains the file,</source>
        <translation type="vanished">这里已包含此文件</translation>
    </message>
    <message>
        <source>Do you want to override it?</source>
        <translation type="vanished">你确定要覆盖它吗</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="54"/>
        <source>Replace</source>
        <translation>གོ་བརྗེ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="63"/>
        <source>Ignore</source>
        <translation>སྣང་མེད་དུ་བཞག་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="82"/>
        <source>Do the same</source>
        <translation>ཚང་མ་བེད་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="105"/>
        <source>&lt;p&gt;This location already contains the file &apos;%1&apos;, Do you want to override it?&lt;/p&gt;</source>
        <translation>&lt;p&gt;གོ་གནས་འདིའི་ནང་དུ་མིང་ལ&quot;%&quot;ཟེར་བའི་ཡིག་ཆ་འདུས་ཡོད་པ་དང་།ཁྱེད་ཀྱིས་དེ་བརྗེ་རྒྱུ་ཐག་གཅོད་བྱས་ཡོད་དམ།&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="111"/>
        <source>Unexpected error from %1 to %2</source>
        <translation>%ནས་%བར་གྱི་བཀོལ་སྤྱོད་བྱེད་སྟངས་ཐད་རྒྱུན་གཏན་མིན་པའི་ནོར་འཁྲུལ་བྱུང་བ།</translation>
    </message>
    <message>
        <source>Then do the same thing in a similar situation</source>
        <translation type="vanished">之后类似情况执行相同操作</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="72"/>
        <source>Backup</source>
        <translation>གྲབས་ཉར།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogNotSupported</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="320"/>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="312"/>
        <source>No</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Do the same</source>
        <translation type="vanished">全部应用</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="357"/>
        <source>Make sure the disk is not full or write protected and that the file is not protected</source>
        <translation>སྡུད་སྡེར་སྲུང་སྐྱོབ་བྱས་མེད་པ་དང་ཡིག་ཆ་བཀོལ་སྤྱོད་བྱས་མེད་པ་གཏན་ཁེལ་བྱེད་རོགས།</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogWarning</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="204"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="213"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="245"/>
        <source>Make sure the disk is not full or write protected and that the file is not protected</source>
        <translation>སྡུད་སྡེར་སྲུང་སྐྱོབ་བྱས་མེད་པ་དང་ཡིག་ཆ་བཀོལ་སྤྱོད་བྱས་མེད་པ་གཏན་ཁེལ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Please make sure the disk is not full or not is write protected, or file is not being used.</source>
        <translation type="vanished">请确保磁盘未满或未被写保护或未被使用。</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationInfo</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="1059"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="1061"/>
        <source>Symbolic Link</source>
        <translation>མྱུར་བའི་ཐབས་ལམ།</translation>
    </message>
    <message>
        <source> - Symbolic Link</source>
        <translation type="vanished">-快捷方式</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationManager</name>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="231"/>
        <source>Warn</source>
        <translation>ཉེན་བརྡ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="231"/>
        <source>&apos;%1&apos; is occupied，you cannot operate!</source>
        <translation>བརྒྱ་ཆ1”བཟུང་སྤྱོད་བྱས་ཟིན་པས་ཁྱེད་ཀྱིས་བཀོལ་སྤྱོད་བྱེད་ཐབས་བྲལ།</translation>
    </message>
    <message>
        <source>No, go to settings</source>
        <translation type="vanished">否，跳转到设置</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="249"/>
        <source>OK</source>
        <translation>སུབ་པ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="253"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="258"/>
        <source>Do you want to put selected %1 item(s) into trash?</source>
        <translation>འདེམ་པའི་བརྒྱ་ཆའི་གཅིག་ཕྱིར་སྡུད་ས་ཚིགས་སུ་འཇོག་དགོས་པ་གཏན་ཁེལ་བྱས་ཡོད་དམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="260"/>
        <source>Do not show again</source>
        <translation>མངོན་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="427"/>
        <source>File System</source>
        <translation>ཡིག་ཆའི་རྒྱུད་ཁོངས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="429"/>
        <source>Data</source>
        <translation>གཞི་གྲངས་སྡེར།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="433"/>
        <source>Insufficient storage space</source>
        <translation>གསོག་འཇོག་བར་སྟོང་མི་འདང་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="436"/>
        <source>%1 no space left on device. Copy file size: %2 GB, Space needed: %3 GB.</source>
        <translation>བརྒྱ་ཆ1སྒྲིག་ཆས་ལ་བར་སྟོང་མི་འདང་བ།མཁོ་ཕབ་ཡིག་ཆའི་ཆེ་ཆུང་།%GBལ་བར་སྟོང་།%GB།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="478"/>
        <source>Can&apos;t delete.</source>
        <translation>བསུབ་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="479"/>
        <source>You can&apos;t delete a file whenthe file is doing another operation</source>
        <translation>ད་ལྟ་བཀོལ་སྤྱོད་བྱེད་བཞིན་པའི་ཡིག་ཆ་ཞིག་བསུབ་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="600"/>
        <source>File Operation is Busy</source>
        <translation>བཀོལ་སྤྱོད་ལ་བྲེལ་བ་ཆེ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="601"/>
        <source>There have been one or more fileoperation(s) executing before. Youroperation will wait for executinguntil it/them done. If you really want to execute file operations parallelly anyway, you can change the default option &quot;Allow Parallel&quot; in option menu.</source>
        <translation>བཀོལ་སྤྱོད་འདི་ལག་བསྟར་མ་བྱས་སྔོན་ལ་བཀོལ་སྤྱོད་བྱས་ཚར་མེད་ལ།དེ་ཡིས་བཀོལ་སྤྱོད་སྔོན་མ་ལེགས་འགྲུབ་བྱུང་རྗེས་ད་གཟོད་ལག་བསྟར་བྱེད་དགོས།གལ་ཏེ་ཁྱོད་ཀྱིས་ཡིག་ཆ་བཀོལ་སྤྱོད་གཤིབ་བགྲོད་བྱེད་པར་རེ་བ་བྱེད་ན།ཁྱོད་ཀྱིས་འདེམས་བྱང་ཁྲོད་ཀྱི&quot;བཀོལ་སྤྱོད་གཤིབ་བགྲོད་&quot;ཀྱི་བཀོད་སྒྲིག་བསྒྱུར་ཆོག</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="624"/>
        <source>The long name file is saved to %1</source>
        <translation>ཡིག་ཆ་རིང་པོ་བརྒྱ་ཆ1བར་ཉར་ཚགས་བྱས་ཡོད།</translation>
    </message>
    <message>
        <source>The system cannot hibernate or sleep</source>
        <translation type="vanished">无法进入休眠或睡眠模式</translation>
    </message>
    <message>
        <source>The file operation is in progress.                                         Ensure that the file operation is complete or canceled before hibernating or sleeping</source>
        <translation type="vanished">文件操作进行中,\
进入休眠或者睡眠之前，请先确保文件操作已完成或者取消</translation>
    </message>
    <message>
        <source>There have been one or more fileoperation(s) executing before. Youroperation will wait for executinguntil it/them done.</source>
        <translation type="vanished">在执行该操作之前有操作未完成，它需要等待上一个操作完成后再执行。</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationPreparePage</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="298"/>
        <source>counting:</source>
        <translation>བསྡོམས་རྩིས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="299"/>
        <source>state:</source>
        <translation>རྣམ་པ།</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationProgressPage</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="321"/>
        <source>&amp;More Details</source>
        <translation>ཞིབ་ཕྲའི་བརྡ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="332"/>
        <source>From:</source>
        <translation>ནས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="333"/>
        <source>To:</source>
        <translation>སླེབས་བྱུང་།</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationProgressWizard</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="55"/>
        <source>File Manager</source>
        <translation>ཡིག་ཆའི་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="59"/>
        <source>&amp;Cancel</source>
        <translation>མེད་པར་བཟོས་སོང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="68"/>
        <source>Preparing...</source>
        <translation>གྲ་སྒྲིག་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="71"/>
        <source>Handling...</source>
        <translation>ཐག་གཅོད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="74"/>
        <source>Clearing...</source>
        <translation>གཙང་བཤེར་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="77"/>
        <source>Rollbacking...</source>
        <translation>ཕྱིར་སོང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="81"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="94"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="120"/>
        <source>File Operation</source>
        <translation>ཡིག་ཆ་བཀོལ་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="95"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="121"/>
        <source>A file operation is running backend...</source>
        <translation>ཡིག་ཆ་ཞིག་རྒྱབ་སྟེགས་སུ་འཁོར་སྐྱོད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="160"/>
        <source>%1 files, %2</source>
        <translation>བརྒྱ་ཆའི་༡གི་ཡིག་ཆ།བསྡོམས་པས་བརྒྱ་ཆའི་༢རེད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="260"/>
        <source>%1 done, %2 total, %3 of %4.</source>
        <translation>བརྒྱ་ཆའི་༡དང་བརྒྱ་ཆའི་༢ཚེག་༤ཁྲོད་ཀྱི་བརྒྱ་ཆའི་༣འགྲུབ་སོང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="203"/>
        <source>clearing: %1, %2 of %3</source>
        <translation>གཙང་བཤེར་བྱེད་བཞིན་པའི་སྒང་ཡིན།%%༣%ནང་གི་༢%</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="248"/>
        <source>copying...</source>
        <translation>བསྐྱར་པར་བྱེད་བཞིན་པའི་སྒང་ཡིན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="278"/>
        <source>Syncing...</source>
        <translation>གོམ་མཉམ་བྱེད་བཞིན་ཡོད།</translation>
    </message>
</context>
<context>
    <name>Peony::FilePreviewPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="261"/>
        <source>File Name:</source>
        <translation>ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="266"/>
        <source>File Type:</source>
        <translation>ཡིག་ཆའི་རིགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="270"/>
        <source>Time Access:</source>
        <translation>འཚམས་འདྲིའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="274"/>
        <source>Time Modified:</source>
        <translation>བཟོ་བཅོས་རྒྱག་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="280"/>
        <source>Children Count:</source>
        <translation>དེའི་ནང་དུ་ཡིག་ཆ་འདུས་ཏེ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="285"/>
        <source>Size:</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="290"/>
        <source>Image resolution:</source>
        <translation>དབྱེ་འབྱེད་ཚད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="294"/>
        <source>color model:</source>
        <translation>ཚོན་མདོག་གི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="357"/>
        <source>usershare</source>
        <translation>འཕྲུལ་ཆས་འདི་མཉམ་སྤྱོད།</translation>
    </message>
    <message>
        <source>Image size:</source>
        <translation type="vanished">图片尺寸：</translation>
    </message>
    <message>
        <source>Image format:</source>
        <translation type="vanished">图片格式：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="396"/>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="397"/>
        <source>%1x%2</source>
        <translation>%1x2%</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="449"/>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="450"/>
        <source>%1 total, %2 hidden</source>
        <translation>སྡོམ་པས་༡%དེའི་ནང་གི་༢%ཡི་གསང་བའི་ཡིག་ཆ།</translation>
    </message>
</context>
<context>
    <name>Peony::FileRenameDialog</name>
    <message>
        <source>Names automatically add serial Numbers (e.g., 1,2,3...)</source>
        <translation type="vanished">名称后自动添加序号（如:1,2,3...）</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>New file name</source>
        <translation type="vanished">文件名</translation>
    </message>
    <message>
        <source>Please enter the file name</source>
        <translation type="vanished">请输入文件名</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
</context>
<context>
    <name>Peony::FileRenameOperation</name>
    <message>
        <source>Rename file</source>
        <translation type="vanished">文件重命名</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="75"/>
        <source>File Rename error</source>
        <translation>ཡིག་ཆའི་བསྐྱར་དུ་མིང་བཏགས་པའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="76"/>
        <source>Invalid file name %1%2%3 .</source>
        <translation>ཁྲིམས་དང་མི་མཐུན་པའི་ཡིག་ཆའི་མིང་བརྒྱ་ཆ12</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="92"/>
        <source>Are you sure to hidden this file?</source>
        <translation>ཡིག་ཆ་འདི་སྦས་དགོས་པ་ཁོ་ཐག་ཡིན་ནམ།</translation>
    </message>
    <message>
        <source>The file %1%2%3 will be hidden when you refresh or change directory!</source>
        <translation type="vanished">文件 %1%2%3 在刷新或者切换路径后将会被隐藏!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="124"/>
        <source>When change the file suffix, the file may be invalid. Are you sure to change it ?</source>
        <translation>གལ་ཏེ་ཡིག་ཆའི་མིང་བསྒྱུར་ན།ཡིག་ཆ་བཀོལ་སྤྱོད་བྱེད་མི་ཐུབ་པར་འགྱུར་སྲིད།དངོས་གནས་བསྒྱུར་དགོས་སམ།</translation>
    </message>
    <message>
        <source>The file %1%2%3 will be hidden when you refresh or rsort!</source>
        <translation type="vanished">文件 %1%2%3 在刷新或者排序后将会被隐藏!</translation>
    </message>
    <message>
        <source>The file %1%2%3 will be hidden!</source>
        <translation type="vanished">文件%1%2%3将被隐藏!</translation>
    </message>
    <message>
        <source>Invalid file name &quot;%1&quot; </source>
        <translation type="vanished">文件名 &quot;%1&quot; 不合法</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="91"/>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="123"/>
        <source>File Rename warning</source>
        <translation>ཡིག་ཆ་ལ་བསྐྱར་དུ་མིང་བཏགས་ནས་ཉེན་བརྡ་གཏོང་བ།</translation>
    </message>
    <message>
        <source>The file &quot;%1&quot; will be hidden!</source>
        <translation type="vanished">文件 &quot;%1&quot; 将会被隐藏！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="210"/>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="240"/>
        <source>Rename file error</source>
        <translation>བསྐྱར་དུ་མིང་བཏགས་པའི་ཡིག་ཆའི་ནོར་འཁྲུལ།</translation>
    </message>
</context>
<context>
    <name>Peony::FileTrashOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="72"/>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="95"/>
        <source>trash:///</source>
        <translation>trash:///////</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="75"/>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="98"/>
        <source>Trash file error</source>
        <translation>ཡིག་ཆ་བླངས་ནས་ཕྱིར་སྡུད་ས་ཚིགས་སུ་འབྱོར་བའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="78"/>
        <source>Invalid Operation! Can not trash &quot;%1&quot;.</source>
        <translation>ཁྲིམས་དང་མི་མཐུན་པའི་བཀོལ་སྤྱོད།%་1་ཕྱིར་བསྡུ་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Can not trash</source>
        <translation type="vanished">不能回收</translation>
    </message>
    <message>
        <source>Can not trash files more than 10GB, would you like to delete it permanently?</source>
        <translation type="vanished">无法回收大于10G的文件，是否需要永久删除？</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="202"/>
        <source>An unmanageable conflict exists. Please check the recycle bin.</source>
        <translation>ཐག་གཅོད་བྱེད་ཐབས་བྲལ་བའི་འགལ་བ་ཡོད་པས་ཕྱིར་སྡུད་ས་ཚིགས་ལ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>The user does not have read and write rights to the file &apos;%1&apos; and cannot delete it to the Recycle Bin.</source>
        <translation type="vanished">用户对当前文件 %1 没有读写权限，无法删除到回收站。</translation>
    </message>
    <message>
        <source>Can not trash this file, would you like to delete it permanently?</source>
        <translation type="vanished">不能回收该文件, 是否要永久删除?</translation>
    </message>
    <message>
        <source>Can not trash %1, would you like to delete this file permanently?</source>
        <translation type="vanished">不能回收%1, 是否永久删除?</translation>
    </message>
    <message>
        <source>. Are you sure you want to permanently delete the file</source>
        <translation type="vanished">，你确定要永久删除文件吗？</translation>
    </message>
    <message>
        <source>The user does not have read and write rights to the file &apos;%s&apos; and cannot delete it to the Recycle Bin.</source>
        <translation type="vanished">用户对当前文件 %s 没有读写权限，无法删除到回收站。</translation>
    </message>
    <message>
        <source>Trash file</source>
        <translation type="vanished">删除文件到回收站</translation>
    </message>
</context>
<context>
    <name>Peony::FileUntrashOperation</name>
    <message>
        <source>Untrash file</source>
        <translation type="vanished">撤销删除的文件</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-untrash-operation.cpp" line="157"/>
        <source>Untrash file error</source>
        <translation>ཕྱིར་སྡུད་ས་ཚིགས་ནས་ཡིག་ཆའི་ནོར་འཁྲུལ་སླར་གསོ་བྱས།</translation>
    </message>
</context>
<context>
    <name>Peony::GlobalSettings</name>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="94"/>
        <location filename="../../libpeony-qt/global-settings.cpp" line="467"/>
        <source>yyyy/MM/dd</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="95"/>
        <location filename="../../libpeony-qt/global-settings.cpp" line="459"/>
        <source>HH:mm:ss</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="456"/>
        <source>AP hh:mm:ss</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="470"/>
        <source>yyyy-MM-dd</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Peony::LocationBar</name>
    <message>
        <source>click the blank area for edit</source>
        <translation type="vanished">点击空白区域编辑路径</translation>
    </message>
    <message>
        <source>Computer</source>
        <translation type="obsolete">计算机</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="366"/>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation>བརྒྱ་ཆ2ནང་བཤེར་འཚོལ་བྱས་པ།</translation>
    </message>
    <message>
        <source>File System</source>
        <translation type="vanished">文件系统</translation>
    </message>
    <message>
        <source>&amp;Copy Directory</source>
        <translation type="vanished">拷贝路径(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="470"/>
        <source>Open In New Tab</source>
        <translation>ཤོག་བྱང་གསར་བའི་ངོས་སུ་ཁ་ཕྱེས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="474"/>
        <source>Open In New Window</source>
        <translation>སྒེའུ་ཁུང་གསར་བའི་ནང་ནས་ཁ་ཕྱེས།</translation>
    </message>
    <message>
        <source>Open In New &amp;Tab</source>
        <translation type="vanished">在新标签页中打开(&amp;T)</translation>
    </message>
    <message>
        <source>Open In &amp;New Window</source>
        <translation type="vanished">在新窗口中打开(&amp;N)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="468"/>
        <source>Copy Directory</source>
        <translation>མཁོ་ཕབ་ཐབས་ལམ།</translation>
    </message>
</context>
<context>
    <name>Peony::MountOperation</name>
    <message>
        <location filename="../../libpeony-qt/mount-operation.cpp" line="90"/>
        <source>Operation Cancelled</source>
        <translation>བཀོལ་སྤྱོད་མེད་པར་བཟོས་སོང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/mount-operation.cpp" line="187"/>
        <source>Login failed, unknown username or password error, please re-enter!</source>
        <translation>ཐོ་འགོད་ལེགས་འགྲུབ་མ་བྱུང་ན།སྤྱོད་མཁན་གྱི་མིང་ངམ་གསང་གྲངས་ལ་ནོར་འཁྲུལ་བྱུང་ཡོད་པས་ཡང་བསྐྱར་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
</context>
<context>
    <name>Peony::NavigationToolBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="35"/>
        <source>Go Back</source>
        <translation>ཕྱི་ནུར་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="39"/>
        <source>Go Forward</source>
        <translation>མདུན་དུ་སྐྱོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="43"/>
        <source>History</source>
        <translation>ལོ་རྒྱུས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="73"/>
        <source>Clear History</source>
        <translation>བར་སྣང་གི་ལོ་རྒྱུས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="88"/>
        <source>Cd Up</source>
        <translation>གོང་ཕྱོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="94"/>
        <source>Refresh</source>
        <translation>གསར་འདོན།</translation>
    </message>
</context>
<context>
    <name>Peony::NewFileLaunchDialog</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="217"/>
        <source>Choose new application</source>
        <translation>བཀོལ་སྤྱོད་གསར་བ་ཞིག་འདེམས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="219"/>
        <source>Choose an Application to open this file</source>
        <translation>ཉེར་སྤྱོད་ཅིག་བདམས་ནས་ཡིག་ཆ་འདིའི་ཁ་ཕྱེས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="226"/>
        <source>apply now</source>
        <translation>ལམ་སེང་བེད་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="232"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="233"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
</context>
<context>
    <name>Peony::OpenWithPropertiesPage</name>
    <message>
        <source>How do you want to open %1%2 files ?</source>
        <translation type="vanished">您希望以什么方式打开 %1%2 文件？</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="93"/>
        <source>How do you want to open &quot;%1%2&quot; files ?</source>
        <translation>ཁྱེད་ཀྱི་རེ་བར་ཐབས་ཅི་ཞིག་གིས་རྒྱ་ཆའི་༡%ཡི་ཡིག་ཆའི་ཁ་ཕྱེས་ན་འདོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="98"/>
        <source>Default open with:</source>
        <translation>ཁས་བླངས་པའི་བྱེད་ཐབས་ནི།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="117"/>
        <source>Other:</source>
        <translation>དེ་ལས་གཞན་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="158"/>
        <source>Choose other application</source>
        <translation>གཞན་པའི་བཀོལ་སྤྱོད་འདེམས་དགོས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="174"/>
        <source>Go to application center</source>
        <translation>མཉེན་ཆས་ལྟེ་གནས་ལ་འགྲོ།</translation>
    </message>
</context>
<context>
    <name>Peony::PathEdit</name>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/path-bar/path-edit.cpp" line="59"/>
        <source>Go To</source>
        <translation>ལྡིང་བ།</translation>
    </message>
</context>
<context>
    <name>Peony::PermissionsPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="136"/>
        <source>User or Group</source>
        <translation>མཛད་སྤྱོད་པའམ་ཚོགས་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="136"/>
        <source>Type</source>
        <translation>རིགས་དབྱེ།</translation>
    </message>
    <message>
        <source>Readable</source>
        <translation type="vanished">可读</translation>
    </message>
    <message>
        <source>Writeable</source>
        <translation type="vanished">可写</translation>
    </message>
    <message>
        <source>Excuteable</source>
        <translation type="vanished">可执行</translation>
    </message>
    <message>
        <source>File: %1</source>
        <translation type="vanished">文件：%1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="80"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="160"/>
        <source>Target: %1</source>
        <translation>བྱ་ཡུལ་གྱི་མིང་།%</translation>
    </message>
    <message>
        <source>Read and Write</source>
        <translation type="vanished">读写</translation>
    </message>
    <message>
        <source>Readonly</source>
        <translation type="vanished">只读</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="136"/>
        <source>Read</source>
        <translation>འདོན་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="136"/>
        <source>Write</source>
        <translation>འབྲི།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="136"/>
        <source>Executable</source>
        <translation>ལག་བསྟར།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="182"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="191"/>
        <source>Can not get the permission info.</source>
        <translation>ཡིག་ཆའི་དབང་ཚད་ཀྱི་འབྲེལ་ཡོད་ཆ་འཕྲིན་བསྡུ་ལེན་བྱེད་ཐབས་བྲལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="252"/>
        <source>(Me)</source>
        <translation>ང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="321"/>
        <source>Others</source>
        <translation>གཞན་དག</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="324"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="326"/>
        <source>Owner</source>
        <translation>ཡོད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="327"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="329"/>
        <source>Group</source>
        <translation>སྤྱོད་མཁན་ཚོ་ཚོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="330"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="332"/>
        <source>Other</source>
        <translation>གཞན་དག</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="542"/>
        <source>Permissions modify tip</source>
        <translation>དབང་ཚད་བཟོ་བཅོས་གསལ་འདེབས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="542"/>
        <source>The current file or folder has already set ACL permissions. Modifying user group permissions may cause the permissions set in ACL to be unusable. Do you want to continue modifying user group permissions?</source>
        <translation>མིག་སྔར་ཡིག་ཆའམ་ཡང་ན་ཡིག་ཆའི་སྒམ་ལ་ACLདབང་ཚད་བཟོས་ཡོད་པ་དང་།བེད་སྤྱོད་བྱེད་མཁན་གྱི་ཚོགས་ཆུང་གི་དབང་ཚད་བཟོ་བཅོས་བརྒྱབ་ན་ACLཡི་དབང་ཚད་སྤྱོད་ཐབས་བྲལ་བས་མུ་མཐུད་བེད་སྤྱོད་བྱེད་མཁན་གྱི་ཚོགས་ཆུང་གི་དབང་ཚད་ལ་བཟོ་བཅོས་རྒྱག་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="560"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="581"/>
        <source>Permission refinement settings</source>
        <translation>དབང་ཚད་ཞིབ་ཕྲ་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="582"/>
        <source>The current user has set advanced sharing. If you still need to modify permissions, advanced sharing may not be available. Do you want to continue setting?</source>
        <translation>མིག་སྔར་སྤྱོད་མཁན་གྱིས་མཐོ་རིམ་མཉམ་སྤྱོད་བྱས་ཟིན་པས།གལ་ཏེ་ད་དུང་དབང་ཚད་བཟོ་བཅོས་རྒྱག་དགོས་ན་མཐོ་རིམ་མཉམ་སྤྱོད་བྱེད་མི་ཐུབ་པ་འབྱུང་སྲིད་པས།མུ་མཐུད་དུ་བཀོད་སྒྲིག་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <source>Other Users</source>
        <translation type="vanished">其它用户</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="342"/>
        <source>You can not change the access of this file.</source>
        <translation>ཁྱོད་ཀྱིས་ཡིག་ཆ་འདིའི་དབང་ཚད་བསྒྱུར་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="346"/>
        <source>Me</source>
        <translation>ང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="350"/>
        <source>User</source>
        <translation>སྤྱོད་མཁན།</translation>
    </message>
</context>
<context>
    <name>Peony::PropertiesWindow</name>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="297"/>
        <source>Trash</source>
        <translation>ཕྱིར་སྡུད་ས་ཚིགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="301"/>
        <source>Recent</source>
        <translation>ཉེ་ལམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="309"/>
        <source>Selected</source>
        <translation>འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="309"/>
        <source> %1 Files</source>
        <translation> %ཡིག་ཆ1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="315"/>
        <source>usershare</source>
        <translation>འཕྲུལ་ཆས་འདི་མཉམ་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="326"/>
        <source>Data</source>
        <translation>གཞི་གྲངས་སྡེར།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="333"/>
        <source>Properties</source>
        <translation>གཏོགས་གཤིས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="443"/>
        <source>Ok</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="444"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
</context>
<context>
    <name>Peony::RecentAndTrashPropertiesPage</name>
    <message>
        <source>Show confirm dialog while trashing: </source>
        <translation type="vanished">删除到回收站时弹出确认框:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="120"/>
        <source>Show confirm dialog while trashing.</source>
        <translation>ཕྱིར་སྡུད་ས་ཚིགས་སུ་བསུབ་སྐབས་ངོས་འཛིན་སྒྲོམ་ཕྱིར་བཏོན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="151"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="157"/>
        <source>Origin Path: </source>
        <translation>སྔར་གྱི་ཐབས་ལམ། </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="192"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="226"/>
        <source>Deletion Date: </source>
        <translation>བསུབ་བྱའི་དུས་ཚོད། </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="178"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="241"/>
        <source>Size: </source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་། </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="235"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="242"/>
        <source>Original Location: </source>
        <translation>སྔར་གྱི་ཐབས་ལམ། </translation>
    </message>
</context>
<context>
    <name>Peony::SearchBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="47"/>
        <source>Input the search key of files you would like to find.</source>
        <translation>གནད་ཚིག་ནང་འཇུག་བྱས་ནས་ཁྱོད་ཀྱིས་འཚོལ་འདོད་པའི་ཡིག་ཆ་འཚོལ་རོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="83"/>
        <source>Input search key...</source>
        <translation>གནད་ཚིག་ནང་འཇུག་བྱོས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="121"/>
        <source>advance search</source>
        <translation>མཐོ་རིམ་བཤེར་འཚོལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="122"/>
        <source>clear record</source>
        <translation>བར་སྣང་གི་ལོ་རྒྱུས།</translation>
    </message>
</context>
<context>
    <name>Peony::SearchBarContainer</name>
    <message>
        <source>Choose File Type</source>
        <translation type="vanished">选择文件类型</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.cpp" line="126"/>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.cpp" line="256"/>
        <source>Clear</source>
        <translation>བར་སྣང་གཙང་བཤེར།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="114"/>
        <source>all</source>
        <translation>ཚང་མ་ནི།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="114"/>
        <source>file folder</source>
        <translation>ཡིག་སྒམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="114"/>
        <source>image</source>
        <translation>པར་རིས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="115"/>
        <source>video</source>
        <translation>བརྙན་རིས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="115"/>
        <source>text file</source>
        <translation>ཡིག་རྐྱང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="115"/>
        <source>audio</source>
        <translation>སྒྲ་གདངས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="115"/>
        <source>others</source>
        <translation>གཞན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="115"/>
        <source>wps file</source>
        <translation>WPSཡིག་ཆ།</translation>
    </message>
</context>
<context>
    <name>Peony::SharedFileLinkOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/shared-file-link-operation.cpp" line="44"/>
        <location filename="../../libpeony-qt/file-operation/shared-file-link-operation.cpp" line="47"/>
        <source>Symbolic Link</source>
        <translation>མྱུར་བའི་ཐབས་ལམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/shared-file-link-operation.cpp" line="80"/>
        <source>The dest file &quot;%1&quot; has existed!</source>
        <translation>དམིགས་འབེན་ཡིག་ཆ1%ཡོད་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/shared-file-link-operation.cpp" line="86"/>
        <source>Link file error</source>
        <translation>ཡིག་ཆའི་སྦྲེལ་མཐུད་ལ་ཕམ་ཁ་བྱུང་།</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarFavoriteItem</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-favorite-item.cpp" line="83"/>
        <source>Trash</source>
        <translation>ཕྱིར་སྡུད་ས་ཚིགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-favorite-item.cpp" line="86"/>
        <source>Recent</source>
        <translation>ཉེ་ལམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-favorite-item.cpp" line="94"/>
        <source>Favorite</source>
        <translation>མགྱོགས་མྱུར་འཚམས་འདྲི།</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarFileSystemItem</name>
    <message>
        <source>Computer</source>
        <translation type="vanished">计算机</translation>
    </message>
    <message>
        <source>File System</source>
        <translation type="vanished">文件系统</translation>
    </message>
    <message>
        <source>data</source>
        <translation type="vanished">数据盘</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="168"/>
        <source>Data</source>
        <translation>གཞི་གྲངས་སྡེར།</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarMenu</name>
    <message>
        <source>&amp;Properties</source>
        <translation type="vanished">属性(&amp;P)</translation>
    </message>
    <message>
        <source>P&amp;roperties</source>
        <translation type="vanished">属性(&amp;R)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="65"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="88"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="114"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="129"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="301"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="351"/>
        <source>Properties</source>
        <translation>གཏོགས་གཤིས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="99"/>
        <source>Delete Symbolic</source>
        <translation>སུབ་པ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="154"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="345"/>
        <source>Unmount</source>
        <translation>བཤིག་འདོན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="163"/>
        <source>Eject</source>
        <translation>དཀྲོལ་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="209"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="242"/>
        <source>Format</source>
        <translation>རྣམ་གཞག་ཅན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="278"/>
        <source>burndata</source>
        <translation>རྐོས་མ།</translation>
    </message>
    <message>
        <source>&amp;Delete Symbolic</source>
        <translation type="vanished">删除(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Unmount</source>
        <translation type="vanished">卸载(&amp;U)</translation>
    </message>
    <message>
        <source>&amp;Eject</source>
        <translation type="vanished">弹出(&amp;E)</translation>
    </message>
    <message>
        <source>format</source>
        <translation type="vanished">格式化</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarModel</name>
    <message>
        <source>Shared Data</source>
        <translation type="vanished">共享数据</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-model.cpp" line="98"/>
        <source>Network</source>
        <translation>དྲ་རྒྱ།</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarPersonalItem</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-personal-item.cpp" line="42"/>
        <source>Personal</source>
        <translation>མི་སྒེར།</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarSeparatorItem</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-separator-item.h" line="68"/>
        <source>(No Sub Directory)</source>
        <translation>སྟོང་།</translation>
    </message>
</context>
<context>
    <name>Peony::StatusBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="94"/>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="100"/>
        <source>; %1 folders</source>
        <translation>%ཡིག་སྒམ1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="95"/>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="102"/>
        <source>; %1 files, %2 total</source>
        <translation>བརྒྱ་ཆའི་༡གི་ཡིག་ཆ།བསྡོམས་པས་བརྒྱ་ཆའི་༢རེད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="97"/>
        <source>; %1 folder</source>
        <translation>%ཡིག་སྒམ1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="98"/>
        <source>; %1 file, %2</source>
        <translation>%ཡིག་ཆ་1%%%2%</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="105"/>
        <source>%1 selected</source>
        <translation>གདམ་བྱའི་རྣམ་གྲངས1</translation>
    </message>
</context>
<context>
    <name>Peony::SyncThread</name>
    <message>
        <location filename="../../libpeony-qt/sync-thread.cpp" line="44"/>
        <source>notify</source>
        <translatorcomment>温馨提示</translatorcomment>
        <translation>དྲོ་སྐྱིད་ལྡན་པའི་དྲན་སྐུལ།</translation>
    </message>
</context>
<context>
    <name>Peony::ToolBar</name>
    <message>
        <source>Open in new &amp;Window</source>
        <translation type="vanished">在新窗口中打开(&amp;W)</translation>
    </message>
    <message>
        <source>Open in &amp;New window</source>
        <translation type="vanished">在新窗口中打开(&amp;N)</translation>
    </message>
    <message>
        <source>Open in new &amp;Tab</source>
        <translation type="vanished">在新标签页中打开(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="138"/>
        <source>Sort Type</source>
        <translation>གོ་རིམ་རིགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="140"/>
        <source>File Name</source>
        <translation>ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="146"/>
        <source>File Type</source>
        <translation>ཡིག་ཆའི་རིགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="149"/>
        <source>File Size</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="143"/>
        <source>Modified Date</source>
        <translation>བཟོ་བཅོས་བརྒྱབ་པའི་ཚེས་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="72"/>
        <source>Open in New window</source>
        <translation>སྒེའུ་ཁུང་གསར་བའི་ནང་ནས་ཁ་ཕྱེས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="74"/>
        <source>Open in new Tab</source>
        <translation>ཤོག་བྱང་གསར་བའི་ངོས་སུ་ཁ་ཕྱེས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="160"/>
        <source>Ascending</source>
        <translation>རིམ་འཕར།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="156"/>
        <source>Descending</source>
        <translation>རིམ་ཕབ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="190"/>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="340"/>
        <source>Copy</source>
        <translation>འདྲ་བཟོ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="193"/>
        <source>Paste</source>
        <translation>སྦྱར་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="196"/>
        <source>Cut</source>
        <translation>དྲས་གཏུབ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="199"/>
        <source>Trash</source>
        <translation>སུབ་པ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="216"/>
        <source>Clean Trash</source>
        <translation>བར་སྣང་ཕྱིར་སྡུད་ས་ཚིགས།</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <source>Delete Permanently</source>
        <translation type="vanished">永久删除</translation>
    </message>
    <message>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="vanished">您确定要删除这些文件吗？一旦开始删除，这些文件将不可再恢复。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="221"/>
        <source>Restore</source>
        <translation>སྔར་གྱི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="271"/>
        <source>Options</source>
        <translation>གདམ་ག</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="274"/>
        <source>Forbid Thumbnail</source>
        <translation>བཀག་སྡོམ་བསྡུས་རིས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="281"/>
        <source>Show Hidden</source>
        <translation>མངོན་མེད་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="288"/>
        <source>Resident in Backend</source>
        <translation>རྒྱུན་སྡོད་རྒྱབ་རྟེན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="289"/>
        <source>Let the program still run after closing the last window. This will reduce the time for the next launch, but it will also consume resources in backend.</source>
        <translation>ཡིག་ཆ་དོ་དམ་ཆས་སྒེའུ་ཁུང་ཡོད་ཚད་ཀྱི་སྒོ་རྒྱབ་རྗེས་སྔར་བཞིན་འཁོར་སྐྱོད་བྱེད་དུ་འཇུག་དགོས།འདིས་ཐེངས་རྗེས་མའི་ལག་བསྟར་བྱེད་པར་མཁོ་བའི་དུས་ཚོད་ཇེ་ཐུང་དུ་བཏང་།འོན་ཀྱང་རྒྱུན་པར་ཐོན་ཁུངས་བཀོལ་སྤྱོད་བྱེད་སྲིད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="301"/>
        <source>&amp;Help</source>
        <translation>རོགས་རམ།(&amp;H)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="307"/>
        <source>&amp;About...</source>
        <translation>འབྲེལ་ཡོད་ཀྱི།（&amp;A）</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="309"/>
        <source>Peony Qt</source>
        <translation>ཡིག་ཆའི་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="310"/>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation>རྩོམ་པ་པོ།YueLan〈lanyue.kylinos.cn〉MeihongHe〈hemeihongkylinos.cn〉པར་དབང་ཡོད་ཚད།（C）2019ལོ།ཐེན་ཅིན་ཆི་ལིན་ཆ་འཕྲིན་ལག་རྩལ་ཚད་ཡོད་ཀུང་སི།</translation>
    </message>
    <message>
        <source>Author: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019,天津麒麟信息技术有限公司.</translation>
    </message>
</context>
<context>
    <name>Peony::UserShareInfoManager</name>
    <message>
        <location filename="../../libpeony-qt/usershare-manager.cpp" line="94"/>
        <location filename="../../libpeony-qt/usershare-manager.cpp" line="127"/>
        <source>Warning</source>
        <translation>ཉེན་བརྡ།</translation>
    </message>
</context>
<context>
    <name>Peony::VolumeManager</name>
    <message>
        <location filename="../../libpeony-qt/volume-manager.cpp" line="150"/>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
</context>
<context>
    <name>ProgressBar</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="803"/>
        <source>starting ...</source>
        <translation>ད་ལྟ་མགོ་བརྩམས་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="900"/>
        <source>canceling ...</source>
        <translation>མེད་པར་བཟོ་བཞིན་པའི་སྒང་རེད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="902"/>
        <source>sync ...</source>
        <translation>གོམ་མཉམ་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source>cancel file operation</source>
        <translation type="vanished">取消文件操作</translation>
    </message>
    <message>
        <source>Are you sure want to cancel the current selected file operation</source>
        <translation type="vanished">你确定要取消当前选中的文件操作</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="40"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="60"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="91"/>
        <source>Icon View</source>
        <translation>རི་མོའི་མཐོང་རིས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="46"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="97"/>
        <source>Show the folder children as icons.</source>
        <translation>རི་མོའི་རྣམ་པའི་ཐོག་ནས་དཀར་ཆག་མངོན་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="42"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="62"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="93"/>
        <source>List View</source>
        <translation>གསལ་ཐོའི་དཔེ་རིས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="48"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="99"/>
        <source>Show the folder children as rows in a list.</source>
        <translation>རེའུ་མིག་གི་རྣམ་པའི་ཐོག་ནས་དཀར་ཆག་མངོན་པ།</translation>
    </message>
    <message>
        <source>Basic Preview Page</source>
        <translation type="vanished">基本</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page-factory.h" line="40"/>
        <source>Basic</source>
        <translation>གཞི་རྩ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page-factory.h" line="46"/>
        <source>Show the basic file properties, and allow you to modify the access and name.</source>
        <translation>ཡིག་ཆའི་གཞི་རྩའི་ཁྱད་ཆོས་མངོན་པར་བྱས་ཏེ་ཡིག་ཆའི་མིང་བཟོ་བཅོས་བརྒྱབ་ཆོག</translation>
    </message>
    <message>
        <source>Permissions Page</source>
        <translation type="vanished">权限</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page-factory.h" line="41"/>
        <source>Permissions</source>
        <translation>དབང་ཚད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page-factory.h" line="47"/>
        <source>Show and modify file&apos;s permission, owner and group.</source>
        <translation>ཡིག་ཆའི་དབང་ཚད་ལ་ལྟ་ཞིབ་དང་བཟོ་བཅོས་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Can not trash</source>
        <translation type="vanished">不能回收</translation>
    </message>
    <message>
        <source>Can not trash these files. You can delete them permanently. Are you sure doing that?</source>
        <translation type="vanished">这些文件不能完全放入回收站，可以选择永久删除这些文件，确定这样做吗？</translation>
    </message>
    <message>
        <source>Can not trash files more than 10GB, would you like to delete it permanently?</source>
        <translation type="vanished">无法回收大于10G的文件，是否需要永久删除？</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="195"/>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="108"/>
        <source>The file is too large to be moved to the recycle bin. Do you want to permanently delete it?</source>
        <translation>ཡིག་ཆ་ཆེ་དྲགས་པས་ཕྱིར་སྡུད་ས་ཚིགས་སུ་སྤོར་མི་རུང་།ཡིག་ཆ་འདི་གཏན་དུ་བསུབ་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="199"/>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="111"/>
        <source>These files are too large to be moved to the recycle bin. Do you want to permanently delete these %1 files?</source>
        <translation>ཡིག་ཆ་ཆེ་དྲགས་པས་ཕྱིར་སྡུད་ས་ཚིགས་སུ་སྤོར་མི་རུང་།%ཡི་ཡིག་ཆ་འདི་གཏན་དུ་བསུབ་རྒྱུ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="311"/>
        <source>Clean the Trash</source>
        <translation>བར་སྣང་ཕྱིར་སྡུད་ས་ཚིགས།</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">清空回收站</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="320"/>
        <source>Do you want to empty the recycle bin and delete the files permanently? Once it has begun there is no way to restore them.</source>
        <translation>ཕྱིར་སྡུད་ས་ཚིགས་ནང་གི་ཡིག་ཆ་གཙང་བཤེར་བྱེད་པ་གཏན་ཁེལ་ཡིན་ནམ།བཀོལ་སྤྱོད་འདི་ཕྱིར་འཐེན་བྱེད་ཐབས་མེད།</translation>
    </message>
    <message>
        <source>Delete Permanently</source>
        <translation type="vanished">永久删除</translation>
    </message>
    <message>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="vanished">您确定要删除这些文件吗？一旦开始删除，这些文件将不可再恢复。</translation>
    </message>
    <message>
        <source>Computer Properties Page</source>
        <translation type="vanished">计算机</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page-factory.h" line="41"/>
        <source>Computer Properties</source>
        <translation>རྩིས་འཁོར་གྱི་ཁྱད་ཆོས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page-factory.h" line="47"/>
        <source>Show the computer properties or items in computer.</source>
        <translation>རྩིས་འཁོར་གྱི་ངོ་བོ་དང་རྩིས་འཁོར་ནང་གི་ཚན་པ་མངོན་ཡོད།</translation>
    </message>
    <message>
        <source>Trash and Recent Properties Page</source>
        <translation type="vanished">最近/回收</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page-factory.h" line="40"/>
        <source>Trash and Recent</source>
        <translation>ཕྱིར་སྡུད་ས་ཚིགས།ཉེ་ཆར།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page-factory.h" line="46"/>
        <source>Show the file properties or items in trash or recent.</source>
        <translation>ཕྱིར་སྡུད་ས་ཚིགས་སམ་ཆེས་ཉེ་བའི་ཡིག་ཆའི་ངོ་བོ་དང་རྣམ་གྲངས་མངོན་ཡོད།</translation>
    </message>
    <message>
        <source>eject device failed</source>
        <translation type="vanished">弹出设备失败</translation>
    </message>
    <message>
        <source>Please check whether the device is occupied and then eject the device again</source>
        <translation type="vanished">请检查设备是否正在使用,确认没有使用后再次弹出</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="541"/>
        <source>Format failed</source>
        <translation>རྣམ་གཞག་ཅན་ལ་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="543"/>
        <source>YES</source>
        <translation>ངོས་འཛིན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1022"/>
        <source>Formatting successful! But failed to set the device name.</source>
        <translation>རྣམ་གཞག་ཅན་ལེགས་འགྲུབ་བྱུང་བ།སྒྲིག་ཆས་ཀྱི་མིང་སྒྲིག་པ་ལེགས་འགྲུབ་མ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1039"/>
        <source>qmesg_notify</source>
        <translation>བརྡ་ཐོ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1054"/>
        <source>Format</source>
        <translation>རྣམ་གཞག་ཅན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1056"/>
        <source>Begin Format</source>
        <translation>འགོ་བརྩམས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1058"/>
        <source>Close</source>
        <translation>སྒོ་གཏན་རོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1020"/>
        <source>Format operation has been finished successfully.</source>
        <translation>རྣམ་གཞག་ཅན་གྱི་བཀོལ་སྤྱོད་ལེགས་འགྲུབ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <source>Formatting successful! Description Failed to set the device name.</source>
        <translation type="vanished">格式化成功！设备名设置失败。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1039"/>
        <source>Sorry, the format operation is failed!</source>
        <translation>ཡིད་ཕངས་པ་ཞིག་ལ།རྣམ་གཞག་ཅན་གྱི་བཀོལ་སྤྱོད་ཕམ་སོང་།ཁྱེད་ཀྱིས་ཡང་བསྐྱར་ཚོད་ལྟ་བྱས་ཆོག</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1052"/>
        <source>Formatting this volume will erase all data on it. Please backup all retained data before formatting. Do you want to continue ?</source>
        <translation>རྣམ་གཞག་ཅན་གྱི་བམ་པོ་འདིའི་སྟེང་གི་གཞི་གྲངས་ཡོད་ཚད་མེད་པར་གཏོང་རྒྱུ་རེད།ཁྱོད་ཀྱིས་རྣམ་གཞག་ཅན་གྱི་སྔོན་ལ་སོར་བཞག་བྱས་པའི་གཞི་གྲངས་ཡོད་ཚད་གྲ་སྒྲིག་བྱེད་རོགས།ཁྱོད་ཀྱིས་མུ་མཐུད་དུ་བྱེད་འདོད་དམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1020"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1022"/>
        <source>format</source>
        <translation>རྣམ་གཞག་ཅན།</translation>
    </message>
    <message>
        <source>begin format</source>
        <translation type="vanished">开始</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/sync-thread.cpp" line="41"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="946"/>
        <source>File Manager</source>
        <translation>ཡིག་ཆའི་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <source>Default search vfs of peony</source>
        <translation type="vanished">默认文件搜索</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="117"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1681"/>
        <source>Force unmount failed</source>
        <translation>བཙན་ཤེད་ཀྱིས་བསུབ་པ་ལེགས་འགྲུབ་མ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="136"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="117"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1681"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="539"/>
        <source>Error: %1
</source>
        <translation>ནོར་འཁྲུལ།%
</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="142"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1684"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1734"/>
        <source>Data synchronization is complete,the device has been unmount successfully!</source>
        <translation>གཞི་གྲངས་དུས་མཉམ་དུ་འགྲུབ་སོང་།སྒྲིག་ཆས་བདེ་ལེགས་ངང་བསུབ་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="131"/>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="136"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1713"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1716"/>
        <source>Unmount failed</source>
        <translation>བསུབ་རྒྱུ་ལེགས་འགྲུབ་མ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1701"/>
        <source>Not authorized to perform operation.</source>
        <translation>བཀོལ་སྤྱོད་ལ་དབང་སྤྲོད་མ་ཐོབ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="131"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1713"/>
        <source>Unable to unmount it, you may need to close some programs, such as: GParted etc.</source>
        <translation>བསུབ་ཐབས་མེད།ཁྱེད་ཀྱིས་སྔོན་ལ་གོ་རིམ་འགའ་སྒོ་རྒྱག་དགོས།དཔེར་ན་ཁུལ་བགོ་རྩོམ་སྒྲིག་ཆས་སོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1716"/>
        <source>Error: %1
Do you want to unmount forcely?</source>
        <translation>ནོར་འཁྲུལ།%བཙན་ཤེད་ཀྱིས་སུབ་པ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="315"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <source>Eject Anyway</source>
        <translation type="vanished">无论如何弹出</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1179"/>
        <source>Failed to activate device: Incorrect passphrase</source>
        <translation>སྐུལ་སློང་བྱེད་མི་ཐུབ་པའི་སྒྲིག་ཆས།ནོར་འཁྲུལ་གྱི་བཀའ་བརྡ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1189"/>
        <source>The device has been mount successfully!</source>
        <translation>སྒྲིག་ཆས་འགེལ་འཇོག་ལེགས་འགྲུབ་བྱུང་སོང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1382"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1420"/>
        <source>Eject device failed, the reason may be that the device has been removed, etc.</source>
        <translation>སྒྲིག་ཆས་ལ་ཕམ་ཁ་བྱུང་བ་ནི་སྒྲིག་ཆས་མེད་པར་བཟོས་པ་སོགས་ཀྱི་རྒྱུ་རྐྱེན་ཡིན་སྲིད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1388"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1427"/>
        <source>Data synchronization is complete and the device can be safely unplugged!</source>
        <translation>གཞི་གྲངས་དུས་མཉམ་དུ་འགྲུབ་ན་སྒྲིག་ཆས་བདེ་འཇགས་ངང་ལེན་ཐུབ།</translation>
    </message>
    <message>
        <source>Unable to eject %1</source>
        <translation type="vanished">无法弹出 %1</translation>
    </message>
    <message>
        <source>PeonyNotify</source>
        <translation type="vanished">文件管理器通知</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1384"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1422"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1701"/>
        <source>Eject failed</source>
        <translation>ཕམ་ཁ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="262"/>
        <source>favorite</source>
        <translation>མགྱོགས་མྱུར་འཚམས་འདྲི།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="295"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="300"/>
        <source>File is not existed.</source>
        <translation>ཡིག་ཆ་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="308"/>
        <source>Share Data</source>
        <translation>འཕྲུལ་ཆས་འདི་མཉམ་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="312"/>
        <source>Trash</source>
        <translation>ཕྱིར་སྡུད་ས་ཚིགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="316"/>
        <source>Recent</source>
        <translation>ཉེ་ལམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="362"/>
        <source>Operation not supported</source>
        <translation>བཀོལ་སྤྱོད་ལ་རྒྱབ་སྐྱོར་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="457"/>
        <source>The virtual file system does not support folder creation</source>
        <translation>རྟོག་བཟོའི་ཡིག་ཆའི་མ་ལག་འོག་ཡིག་སྣོད་གསར་བ་འཛུགས་པར་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="529"/>
        <source>Can not create a symbolic file for vfs location</source>
        <translation>རྟོག་བཟོའི་དཀར་ཆག་ལ་མྱུར་བཟོའི་ཐབས་ལམ་གསར་གཏོད་བྱེད་ཐབས་བྲལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="536"/>
        <source>Symbolic Link</source>
        <translation>མྱུར་བའི་ཐབས་ལམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="548"/>
        <source>Can not create symbolic file here, %1</source>
        <translation>འདི་ནས་མགྱོགས་མྱུར་གྱི་ཐབས་ལམ་ཞིག་འཛུགས་མི་ཐུབ།%</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="557"/>
        <source>Can not add a file to favorite directory.</source>
        <translation>ཡིག་ཆ་སྡུད་གསོག་ཆས་སུ་འཇོག་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="615"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="623"/>
        <source>The virtual file system cannot be opened</source>
        <translation>རྟོག་བཟོའི་ཡིག་ཆའི་མ་ལག་ཁ་ཕྱེ་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="444"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="472"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="487"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="573"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="591"/>
        <source>Virtual file directories do not support move and copy operations</source>
        <translation>རྟོག་བཟོའི་ཡིག་ཆའི་ཐབས་ལམ་གྱིས་སྤོ་འགུལ་དང་འདྲ་བཟོ་བཀོལ་སྤྱོད་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-register.h" line="43"/>
        <source>Default favorite vfs of peony</source>
        <translation>ཡིག་ཆ་དོ་དམ་ཆས་ཀྱི་རྟོག་བཟོའི་ཡིག་ཆའི་མ་ལག་གིས་མགྱོགས་མྱུར་ངང་བཅར་འདྲི་བྱེད་པར་ཁས་ལེན་བྱས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page-factory.h" line="38"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page-factory.h" line="44"/>
        <source>Details</source>
        <translation>ཆ་འཕྲིན་ཞིབ་ཕྲ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/mark-properties-page-factory.h" line="40"/>
        <source>Mark</source>
        <translation>རྟགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/mark-properties-page-factory.h" line="46"/>
        <source>mark this file.</source>
        <translation>ཡིག་ཆ་འདི་ལ་རྟགས་རྒྱོབ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page-factory.h" line="40"/>
        <source>Open With</source>
        <translation>ཁ་འབྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page-factory.h" line="46"/>
        <source>open with.</source>
        <translation>སྒོ་འབྱེད་པའི་ཐབས་ཤེས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/sync-thread.cpp" line="11"/>
        <source>It need to synchronize before operating the device,place wait!</source>
        <translation>སྒྲིག་ཆས་བཀོལ་སྤྱོད་བྱེད་པའི་སྔོན་ལ་དུས་མཉམ་དུ་གཞི་གྲངས་དགོས།ཅུང་ཙམ་སྒུག་རོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="409"/>
        <source>permission denied</source>
        <translation>དབང་ཚད་མེད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="402"/>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="415"/>
        <source>file not found</source>
        <translation>ཡིག་ཆ་འདི་མ་རྙེད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="499"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="516"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="532"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="825"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="171"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="193"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="215"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="222"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="238"/>
        <source>duplicate</source>
        <translation>བུ་ཡིག</translation>
    </message>
    <message>
        <source>Error when getting information for file : No target file found</source>
        <translation type="vanished">获取文件信息时出现错误：没有目标文件。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-utils.cpp" line="361"/>
        <source>data</source>
        <translation>གཞི་གྲངས་སྡེར།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="185"/>
        <location filename="../../libpeony-qt/vfs/search-vfs-uri-parser.cpp" line="110"/>
        <source>Computer</source>
        <translation>རྩིས་འཁོར།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-utils.cpp" line="364"/>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="249"/>
        <source>File System</source>
        <translation>ཡིག་ཆའི་རྒྱུད་ཁོངས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="254"/>
        <source>Data</source>
        <translation>གཞི་གྲངས་སྡེར།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="411"/>
        <source>Failed to open file &quot;%1&quot;: insufficient permissions.</source>
        <translation>ཡིག་ཆའི་ཁ་ཕྱེ་ནས&quot;%&quot;ཕམ་པ།དབང་ཚད་མི་འདང་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="422"/>
        <source>File “%1” does not exist. Please check whether the file has been deleted.</source>
        <translation>ཡིག་ཆ&quot;%&quot;མེད་པ་རེད།ཡིག་ཆ་བསུབ་ཡོད་མེད་ལ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccommand.cpp" line="58"/>
        <source>burn operation has been cancelled</source>
        <translation>བརྐོས་པའི་བཀོལ་སྤྱོད་མེད་པར་བཟོས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccommand.cpp" line="62"/>
        <source> is busy!</source>
        <translation> བཟུང་ཟིན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1213"/>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="203"/>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="375"/>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="182"/>
        <source>Are you sure you want to permanently delete this file? Once deletion begins, the file will not be recoverable.</source>
        <translation>ཡིག་ཆ་འདི་ནམ་ཡང་བསུབ་རྒྱུ་ཁོ་ཐག་ཡིན་ནམ།གལ་ཏེ་བསུབ་ན་ཡིག་ཆ་སླར་གསོ་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1217"/>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="207"/>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="379"/>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="186"/>
        <source>Are you sure you want to permanently delete these %1 files? Once deletion begins, these file will not be recoverable.</source>
        <translation>ཡིག་ཆ་འདི་གཏན་དུ་བསུབ་རྒྱུ་ཡིན་ནམ།གལ་ཏེ་བསུབ་ན།ཡིག་ཆ་སླར་གསོ་བྱེད་མི་ཐུབ།</translation>
    </message>
</context>
<context>
    <name>UdfBurn::UdfAppendBurnDataDialog</name>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="23"/>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="158"/>
        <source>AppendBurnData</source>
        <translation>གསབ་སྣོན་ཟིན་ཐོ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="36"/>
        <source>Disc Type:</source>
        <translation>འོད་སྡེར་གྱི་རིགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="44"/>
        <source>Device Name:</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="57"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="59"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="67"/>
        <source>Unknow</source>
        <translation>མི་ཤེས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="109"/>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="126"/>
        <source>Warning</source>
        <translation>ཉེན་བརྡ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="109"/>
        <source>No burn data, please add!</source>
        <translation>གཞི་གྲངས་བརྐོས་མེད་ན་ཁ་སྣོན་གྱིས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="126"/>
        <source>The disc name cannot be set to empty, please re-enter it!</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་བར་སྟོང་དུ་འཇོག་མི་རུང་།ཡང་བསྐྱར་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="158"/>
        <source>AppendBurnData operation has been finished successfully.</source>
        <translation>རྐོས་མའི་བཀོལ་སྤྱོད་ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="164"/>
        <source>Sorry, the appendBurnData operation is failed!</source>
        <translation>ཐུགས་རྒྱལ་མ་བཞེངས།བསྐྱར་ཕབ་བཀོལ་སྤྱོད་ལེགས་འགྲུབ་མ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="165"/>
        <source>Failed</source>
        <translation>ཕམ་ཁ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="174"/>
        <source>Burning. Do not close this window</source>
        <translation>རྐོས་མའི་ནང་དུ་སྒེའུ་ཁུང་འདི་མ་རྒྱག</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="183"/>
        <source>Burning this disc will append datas on it. Do you want to continue ?</source>
        <translation>འོད་སྡེར་འདི་བརྐོས་ནས་འོད་སྡེར་འདིའི་སྟེང་དུ་འཇོག་རྒྱུ་ཡིན་ནམ།མུ་མཐུད་དུ་བྱེད་དམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="184"/>
        <source>Burn</source>
        <translation>རྐོས་མ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="186"/>
        <source>Begin Burning</source>
        <translation>ཕབ་ལེན་བྱེད་འགོ་ཚུགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfAppendBurnDataDialog.cpp" line="187"/>
        <source>Close</source>
        <translation>སྒོ་གཏན་རོགས།</translation>
    </message>
</context>
<context>
    <name>UdfBurn::UdfFormatDialog</name>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="24"/>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="143"/>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="194"/>
        <source>Format</source>
        <translation>རྣམ་གཞག་ཅན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="37"/>
        <source>Disc Type:</source>
        <translation>འོད་སྡེར་གྱི་རིགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="45"/>
        <source>Device Name:</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="59"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="61"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="69"/>
        <source>Unknow</source>
        <translation>མི་ཤེས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="108"/>
        <source>Warning</source>
        <translation>ཉེན་བརྡ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="108"/>
        <source>The disc name cannot be set to empty, please re-enter it!</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་བར་སྟོང་དུ་འཇོག་མི་རུང་།ཡང་བསྐྱར་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="143"/>
        <source>Format operation has been finished successfully.</source>
        <translation>རྣམ་གཞག་ཅན་གྱི་བཀོལ་སྤྱོད་ལེགས་འགྲུབ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="148"/>
        <source>Sorry, the format operation is failed!</source>
        <translation>ཡིད་ཕངས་པ་ཞིག་ལ།རྣམ་གཞག་ཅན་གྱི་བཀོལ་སྤྱོད་ཕམ་སོང་།ཁྱེད་ཀྱིས་ཡང་བསྐྱར་ཚོད་ལྟ་བྱས་ཆོག</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="149"/>
        <source>Failed</source>
        <translation>ཕམ་ཁ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="183"/>
        <source>Formatting. Do not close this window</source>
        <translation>རྣམ་གཞག་ཅན་དུ་འགྱུར་བཞིན་པའི་སྒང་ཡིན་པས་སྒོ་མ་བརྒྱག</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="193"/>
        <source>Formatting this disc will erase all data on it. Please backup all retained data before formatting. Do you want to continue ?</source>
        <translation>རྣམ་གཞག་ཅན་གྱི་འོད་སྡེར་འདིའི་སྟེང་གི་གཞི་གྲངས་ཡོད་ཚད་བསུབ་རྒྱུ་རེད།ཁྱོད་ཀྱིས་རྣམ་གཞག་ཅན་གྱི་སྔོན་ལ་སོར་བཞག་བྱས་པའི་གཞི་གྲངས་ཡོད་ཚད་གྲ་སྒྲིག་བྱེད་རོགས།ཁྱོད་ཀྱིས་མུ་མཐུད་དུ་བྱེད་འདོད་དམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="196"/>
        <source>Begin Format</source>
        <translation>འགོ་བརྩམས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/ky-udf-format-dialog.cpp" line="197"/>
        <source>Close</source>
        <translation>སྒོ་གཏན་རོགས།</translation>
    </message>
</context>
<context>
    <name>UdfFormatDialog</name>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="19"/>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="143"/>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="196"/>
        <source>Format</source>
        <translation>རྣམ་གཞག་ཅན།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="32"/>
        <source>Disc Type:</source>
        <translation>འོད་སྡེར་གྱི་རིགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="40"/>
        <source>Device Name:</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="54"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="56"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="64"/>
        <source>Unknow</source>
        <translation>མི་ཤེས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="106"/>
        <source>Warning</source>
        <translation>ཉེན་བརྡ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="106"/>
        <source>The disc name cannot be set to empty, please re-enter it!</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་བར་སྟོང་དུ་འཇོག་མི་རུང་།ཡང་བསྐྱར་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="143"/>
        <source>Format operation has been finished successfully.</source>
        <translation>རྣམ་གཞག་ཅན་གྱི་བཀོལ་སྤྱོད་ལེགས་འགྲུབ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="148"/>
        <source>Sorry, the format operation is failed!</source>
        <translation>ཡིད་ཕངས་པ་ཞིག་ལ།རྣམ་གཞག་ཅན་གྱི་བཀོལ་སྤྱོད་ཕམ་སོང་།ཁྱེད་ཀྱིས་ཡང་བསྐྱར་ཚོད་ལྟ་བྱས་ཆོག</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="149"/>
        <source>Failed</source>
        <translation>ཕམ་ཁ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="183"/>
        <source>Formatting. Do not close this window</source>
        <translation>རྣམ་གཞག་ཅན་དུ་འགྱུར་བཞིན་པའི་སྒང་ཡིན་པས་སྒོ་མ་བརྒྱག</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="195"/>
        <source>Formatting this disc will erase all data on it. Please backup all retained data before formatting. Do you want to continue ?</source>
        <translation>རྣམ་གཞག་ཅན་གྱི་འོད་སྡེར་འདིའི་སྟེང་གི་གཞི་གྲངས་ཡོད་ཚད་བསུབ་རྒྱུ་རེད།ཁྱོད་ཀྱིས་རྣམ་གཞག་ཅན་གྱི་སྔོན་ལ་སོར་བཞག་བྱས་པའི་གཞི་གྲངས་ཡོད་ཚད་གྲ་སྒྲིག་བྱེད་རོགས།ཁྱོད་ཀྱིས་མུ་མཐུད་དུ་བྱེད་འདོད་དམ།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="198"/>
        <source>Begin Format</source>
        <translation>འགོ་བརྩམས།</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="199"/>
        <source>Close</source>
        <translation>སྒོ་གཏན་རོགས།</translation>
    </message>
</context>
</TS>
