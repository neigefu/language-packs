<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>Peony::DesktopIconView</name>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.h" line="81"/>
        <source>Desktop Icon View</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ᠎ᠤ᠋ᠨ ᠢᠺᠦᠨ ᠵᠢᠷᠤᠭ᠎ᠤ᠋ᠨ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="631"/>
        <source>New Folder</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠴᠣᠮᠣᠭ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="725"/>
        <source>set background</source>
        <translation>ᠠᠷᠤ ᠠᠬᠤᠢ᠎ᠶ᠋ᠢ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="967"/>
        <source>Open failed</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠭᠰᠡᠨ ᠨᠢ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="968"/>
        <source>Open directory failed, you have no permission!</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠬᠠᠪᠴᠢᠭᠤᠷ᠎ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠬᠦ᠎ᠳ᠋ᠦ᠍ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠲᠤᠰ ᠭᠠᠷᠴᠠᠭ᠎ᠤ᠋ᠨ ᠡᠷᠬᠡ ᠥᠬᠡᠢ !︕</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="950"/>
        <source>Open Link failed</source>
        <translation>ᠲᠦᠳᠡ ᠠᠷᠭ᠎ᠠ ᠮᠠᠶᠢᠭ᠎ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="951"/>
        <source>File not exist, do you want to delete the link file?</source>
        <translation>ᠬᠠᠷᠠᠯᠲᠠ ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠬᠠᠪᠴᠢᠭᠤᠷ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ ᠂ ᠲᠤᠰ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠲᠦᠳᠡ ᠠᠷᠭ᠎ᠠ ᠮᠠᠶᠢᠭ᠎ᠢ᠋ ᠬᠠᠰᠤᠨ᠎ᠠ ᠤᠤ ？</translation>
    </message>
</context>
<context>
    <name>Peony::DesktopItemModel</name>
    <message>
        <location filename="../../peony-qt-desktop/desktop-item-model.cpp" line="624"/>
        <source>Computer</source>
        <translation>ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-item-model.cpp" line="626"/>
        <source>Trash</source>
        <translation>ᠪᠤᠴᠠᠭᠠᠨ ᠬᠤᠷᠢᠶᠠᠬᠤ ᠥᠷᠲᠡᠭᠡ</translation>
    </message>
</context>
<context>
    <name>Peony::DesktopMenu</name>
    <message>
        <source>&amp;Open in new Window</source>
        <translation type="vanished">在新窗口中打开(&amp;N)</translation>
    </message>
    <message>
        <source>Select &amp;All</source>
        <translation type="vanished">全选(&amp;A)</translation>
    </message>
    <message>
        <source>&amp;Open &quot;%1&quot;</source>
        <translation type="vanished">打开 &quot;%1&quot;(&amp;O)</translation>
    </message>
    <message>
        <source>Open &quot;%1&quot; &amp;with...</source>
        <translation type="vanished">打开方式(&amp;W)...</translation>
    </message>
    <message>
        <source>&amp;More applications...</source>
        <translation type="vanished">更多应用(&amp;M)...</translation>
    </message>
    <message>
        <source>Open &quot;%1&quot; with...</source>
        <translation type="vanished">打开方式...</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation type="vanished">打开(&amp;O)</translation>
    </message>
    <message>
        <source>Open &amp;with...</source>
        <translation type="vanished">打开方式(&amp;W)...</translation>
    </message>
    <message>
        <source>&amp;Open %1 selected files</source>
        <translation type="vanished">打开%1个选中文件(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="237"/>
        <source>Reverse Select</source>
        <translation>ᠭᠡᠳᠡᠷᠭᠦ ᠰᠣᠩᠭᠣᠭᠤᠯᠢ</translation>
    </message>
    <message>
        <source>&amp;New...</source>
        <translation type="vanished">新建(&amp;N)</translation>
    </message>
    <message>
        <source>New...</source>
        <translation type="vanished">新建...</translation>
    </message>
    <message>
        <source>Empty &amp;File</source>
        <translation type="vanished">空文件(&amp;E)</translation>
    </message>
    <message>
        <source>&amp;Folder</source>
        <translation type="vanished">文件夹(&amp;F)</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="338"/>
        <source>New Folder</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠴᠣᠮᠣᠭ</translation>
    </message>
    <message>
        <source>View Type...</source>
        <translation type="vanished">视图类型...</translation>
    </message>
    <message>
        <source>&amp;Small</source>
        <translation type="vanished">小图标(&amp;S)</translation>
    </message>
    <message>
        <source>&amp;Normal</source>
        <translation type="vanished">中图标(&amp;N)</translation>
    </message>
    <message>
        <source>&amp;Large</source>
        <translation type="vanished">大图标(&amp;L)</translation>
    </message>
    <message>
        <source>&amp;Huge</source>
        <translation type="vanished">超大图标(&amp;H)</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="109"/>
        <source>Open in new Window</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠴᠣᠩᠬᠣᠨ᠎ᠳ᠋ᠤ᠌ ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="114"/>
        <source>Select All</source>
        <translation>ᠪᠦᠬᠦ ᠰᠣᠩᠭᠣᠯᠲᠠ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="137"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="165"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="199"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="210"/>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="145"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="170"/>
        <source>Open with...</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠬᠦ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="158"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="193"/>
        <source>More applications...</source>
        <translation>ᠨᠡᠩ ᠣᠯᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠯᠲᠡ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="213"/>
        <source>Open %1 selected files</source>
        <translation>%1 ᠰᠣᠩᠭᠣᠭᠳᠠᠭᠰᠠᠨ ᠹᠧᠢᠯ᠎ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="251"/>
        <source>New</source>
        <translation>ᠰᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="322"/>
        <source>Empty File</source>
        <translation>ᠬᠣᠭᠣᠰᠣᠨ ᠲᠧᠺᠰᠲ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="334"/>
        <source>Folder</source>
        <translation>ᠴᠣᠮᠣᠭ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="357"/>
        <source>View Type</source>
        <translation>ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ᠎ᠤ᠋ᠨ ᠲᠥᠷᠥᠯ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="363"/>
        <source>Small</source>
        <translation>ᠵᠢᠵᠢᠭ ᠢᠺᠦᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="366"/>
        <source>Normal</source>
        <translation>ᠳᠤᠮᠳᠠ ᠢᠺᠦᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="369"/>
        <source>Large</source>
        <translation>ᠲᠣᠮᠣ ᠰᠢᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="372"/>
        <source>Huge</source>
        <translation>ᠬᠡᠳᠦ ᠶᠡᠬᠡ ᠢᠺᠦᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="400"/>
        <source>Sort By</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠬᠤ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <source>Sort By...</source>
        <translation type="vanished">排序方式...</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="405"/>
        <source>Name</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="407"/>
        <source>File Type</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠲᠥᠷᠥᠯ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="408"/>
        <source>File Size</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="480"/>
        <source>Clean the trash</source>
        <translation>ᠬᠤᠭᠯᠠᠭᠤᠷ᠎ᠢ᠋ ᠬᠣᠭᠣᠰᠣᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="501"/>
        <source>Copy</source>
        <translation>ᠺᠣᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="526"/>
        <source>Delete to trash</source>
        <translation>ᠬᠤᠭᠯᠠᠭᠤᠷ᠎ᠲᠤ᠌ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>Can not show trash properties with other files together!</source>
        <translation type="vanished">不能将回收站与其他文件一起查看属性!</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="406"/>
        <source>Modified Date</source>
        <translation>ᠵᠠᠰᠠᠭᠰᠠᠨ ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="505"/>
        <source>Cut</source>
        <translation>ᠬᠠᠢᠴᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>&amp;Delete to trash</source>
        <translation type="vanished">删除到回收站(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="535"/>
        <source>Delete forever</source>
        <translation>ᠥᠨᠢᠳᠡ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="543"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="548"/>
        <source>Rename</source>
        <translation>ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="555"/>
        <source>Paste</source>
        <translation>ᠨᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="562"/>
        <source>Refresh</source>
        <translation>ᠰᠢᠨᠡᠳᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="576"/>
        <source>Properties</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠲᠤ ᠴᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <source>P&amp;roperties</source>
        <translation type="vanished">属性(&amp;R)</translation>
    </message>
    <message>
        <source>Sort Order...</source>
        <translation type="vanished">排列顺序...</translation>
    </message>
    <message>
        <source>Ascending Order</source>
        <translation type="vanished">升序</translation>
    </message>
    <message>
        <source>Descending Order</source>
        <translation type="vanished">降序</translation>
    </message>
    <message>
        <source>Zoom &amp;In</source>
        <translation type="vanished">放大(&amp;I)</translation>
    </message>
    <message>
        <source>Zoom &amp;Out</source>
        <translation type="vanished">缩小(&amp;O)</translation>
    </message>
    <message>
        <source>&amp;Restore all</source>
        <translation type="vanished">恢复全部(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Clean the trash</source>
        <translation type="vanished">清空回收站(&amp;C)</translation>
    </message>
    <message>
        <source>Delete Permanently</source>
        <translation type="vanished">永久删除</translation>
    </message>
    <message>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="vanished">您确定要删除这些文件吗？一旦开始删除，这些文件将不可再恢复。</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="vanished">复制(&amp;C)</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="vanished">剪切(&amp;T)</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">删除(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="vanished">重命名(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="vanished">粘贴(&amp;P)</translation>
    </message>
    <message>
        <source>&amp;Refresh</source>
        <translation type="vanished">刷新(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Properties</source>
        <translation type="vanished">属性(&amp;P)</translation>
    </message>
</context>
<context>
    <name>Peony::DesktopWindow</name>
    <message>
        <source>Desktop</source>
        <translation type="vanished">桌面</translation>
    </message>
    <message>
        <source>set background</source>
        <translation type="vanished">设置壁纸</translation>
    </message>
    <message>
        <source>New Folder</source>
        <translation type="vanished">新建文件夹</translation>
    </message>
</context>
<context>
    <name>PeonyDesktopApplication</name>
    <message>
        <source>Close the peony-qt desktop window</source>
        <translation type="vanished">关闭桌面并退出</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="233"/>
        <source>peony-qt-desktop</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ</translation>
    </message>
    <message>
        <source>Peony-Qt Desktop</source>
        <translation type="vanished">桌面</translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation type="vanished">桌面</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="567"/>
        <source>Close the peony desktop window</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ᠎ᠤ᠋ᠨ ᠫᠷᠤᠭ᠌ᠷᠠᠮ᠎ᠢ᠋ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="570"/>
        <source>Take over the dbus service.</source>
        <translation>DBusᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ᠎ᠶ᠋ᠢ ᠬᠦᠯᠢᠶᠡᠨ ᠠᠪᠴᠤ ᠬᠠᠮᠢᠶᠠᠷᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="573"/>
        <source>Take over the desktop displaying</source>
        <translation>ᠬᠦᠯᠢᠶᠡᠨ ᠠᠪᠴᠤ ᠬᠠᠮᠢᠶᠠᠷᠬᠤ ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="576"/>
        <source>Setup backgrounds</source>
        <translation>Setup backgrounds</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="579"/>
        <source>Clear standard icons</source>
        <translation>Clear standard icons</translation>
    </message>
    <message>
        <source>set background</source>
        <translation type="vanished">设置背景</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../peony-qt-desktop/desktopbackgroundwindow.cpp" line="78"/>
        <source>set background</source>
        <translation>ᠠᠷᠤ ᠠᠬᠤᠢ᠎ᠶ᠋ᠢ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktopbackgroundwindow.cpp" line="83"/>
        <source>display settings</source>
        <translation>ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯ</translation>
    </message>
    <message>
        <source>set resolution</source>
        <translation type="vanished">设置分辨率</translation>
    </message>
</context>
</TS>
