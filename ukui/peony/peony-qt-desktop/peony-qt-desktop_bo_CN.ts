<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Peony::DesktopIconView</name>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.h" line="81"/>
        <source>Desktop Icon View</source>
        <translation>ཅོག་ངོས་རི་མོ།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="631"/>
        <source>New Folder</source>
        <translation>ཡིག་སྒམ།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="725"/>
        <source>set background</source>
        <translation>རྒྱབ་ལྗོངས།</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="967"/>
        <source>Open failed</source>
        <translation>ཕམ་ཁ་ཁ་ཕྱེ།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="968"/>
        <source>Open directory failed, you have no permission!</source>
        <translation>ཡིག་ཆའི་ཁ་ཕྱེ་ནས་ཕམ་སོང་།ཁྱེད་ལ་དཀར་ཆག་འདིའི་དབང་ཚད་མེད།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="950"/>
        <source>Open Link failed</source>
        <translation>བདེ་མྱུར་གྱི་ཐབས་ལ་བརྟེན་ནས་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="951"/>
        <source>File not exist, do you want to delete the link file?</source>
        <translation>དམིགས་འབེན་ཡིག་ཆ་མེད་པས།བསུབ་ན་ནུས་པ་མེད་པར་གཏོང་ཐུབ་བམ།</translation>
    </message>
</context>
<context>
    <name>Peony::DesktopItemModel</name>
    <message>
        <location filename="../../peony-qt-desktop/desktop-item-model.cpp" line="624"/>
        <source>Computer</source>
        <translation>རྩིས་འཁོར།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-item-model.cpp" line="626"/>
        <source>Trash</source>
        <translation>ཕྱིར་སྡུད་ས་ཚིགས།</translation>
    </message>
</context>
<context>
    <name>Peony::DesktopMenu</name>
    <message>
        <source>&amp;Open in new Window</source>
        <translation type="vanished">在新窗口中打开(&amp;N)</translation>
    </message>
    <message>
        <source>Select &amp;All</source>
        <translation type="vanished">全选(&amp;A)</translation>
    </message>
    <message>
        <source>&amp;Open &quot;%1&quot;</source>
        <translation type="vanished">打开 &quot;%1&quot;(&amp;O)</translation>
    </message>
    <message>
        <source>Open &quot;%1&quot; &amp;with...</source>
        <translation type="vanished">打开方式(&amp;W)...</translation>
    </message>
    <message>
        <source>&amp;More applications...</source>
        <translation type="vanished">更多应用(&amp;M)...</translation>
    </message>
    <message>
        <source>Open &quot;%1&quot; with...</source>
        <translation type="vanished">打开方式...</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation type="vanished">打开(&amp;O)</translation>
    </message>
    <message>
        <source>Open &amp;with...</source>
        <translation type="vanished">打开方式(&amp;W)...</translation>
    </message>
    <message>
        <source>&amp;Open %1 selected files</source>
        <translation type="vanished">打开%1个选中文件(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="237"/>
        <source>Reverse Select</source>
        <translation>ལྡོག་འདེམས།</translation>
    </message>
    <message>
        <source>&amp;New...</source>
        <translation type="vanished">新建(&amp;N)</translation>
    </message>
    <message>
        <source>New...</source>
        <translation type="vanished">新建...</translation>
    </message>
    <message>
        <source>Empty &amp;File</source>
        <translation type="vanished">空文件(&amp;E)</translation>
    </message>
    <message>
        <source>&amp;Folder</source>
        <translation type="vanished">文件夹(&amp;F)</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="338"/>
        <source>New Folder</source>
        <translation>ཡིག་སྒམ།</translation>
    </message>
    <message>
        <source>View Type...</source>
        <translation type="vanished">视图类型...</translation>
    </message>
    <message>
        <source>&amp;Small</source>
        <translation type="vanished">小图标(&amp;S)</translation>
    </message>
    <message>
        <source>&amp;Normal</source>
        <translation type="vanished">中图标(&amp;N)</translation>
    </message>
    <message>
        <source>&amp;Large</source>
        <translation type="vanished">大图标(&amp;L)</translation>
    </message>
    <message>
        <source>&amp;Huge</source>
        <translation type="vanished">超大图标(&amp;H)</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="109"/>
        <source>Open in new Window</source>
        <translation>སྒེའུ་ཁུང་གསར་བའི་ནང་ནས་ཁ་ཕྱེས།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="114"/>
        <source>Select All</source>
        <translation>ཚང་མ་འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="137"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="165"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="199"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="210"/>
        <source>Open</source>
        <translation>ཁ་ཕྱེ།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="145"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="170"/>
        <source>Open with...</source>
        <translation>ཁ་འབྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="158"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="193"/>
        <source>More applications...</source>
        <translation>དེ་ལས་མང་བའི་བེད་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="213"/>
        <source>Open %1 selected files</source>
        <translation>གདམ་ཐོན་བྱུང་བའི་ཡིག་ཆ་1གི་ཁ་ཕྱེས།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="251"/>
        <source>New</source>
        <translation>གསར་འཛུགས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="322"/>
        <source>Empty File</source>
        <translation>ཡིག་རྐྱང་།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="334"/>
        <source>Folder</source>
        <translation>ཡིག་སྒམ།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="357"/>
        <source>View Type</source>
        <translation>མཐོང་རིས།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="363"/>
        <source>Small</source>
        <translation>རི་མོ།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="366"/>
        <source>Normal</source>
        <translation>རི་མོ།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="369"/>
        <source>Large</source>
        <translation>རི་མོ།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="372"/>
        <source>Huge</source>
        <translation>རིས་དབྱིབས་ཆེན་པོ།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="400"/>
        <source>Sort By</source>
        <translation>གོ་རིམ་སྒྲིག་སྟངས།</translation>
    </message>
    <message>
        <source>Sort By...</source>
        <translation type="vanished">排序方式...</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="405"/>
        <source>Name</source>
        <translation>ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="407"/>
        <source>File Type</source>
        <translation>ཡིག་ཆའི་རིགས།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="408"/>
        <source>File Size</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="480"/>
        <source>Clean the trash</source>
        <translation>བར་སྣང་ཕྱིར་སྡུད་ས་ཚིགས།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="501"/>
        <source>Copy</source>
        <translation>འདྲ་བཟོ།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="526"/>
        <source>Delete to trash</source>
        <translation>ཕྱིར་སྡུད་ས་ཚིགས་སུ་སུབ་པ་ཡིན།</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>Can not show trash properties with other files together!</source>
        <translation type="vanished">不能将回收站与其他文件一起查看属性!</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="406"/>
        <source>Modified Date</source>
        <translation>བཟོ་བཅོས་བརྒྱབ་པའི་ཚེས་གྲངས།</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="505"/>
        <source>Cut</source>
        <translation>དྲས་གཏུབ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>&amp;Delete to trash</source>
        <translation type="vanished">删除到回收站(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="535"/>
        <source>Delete forever</source>
        <translation>ཡུན་རིང་བསུབ་རྒྱུ།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="543"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="548"/>
        <source>Rename</source>
        <translation>མིང་བཏགས་པ།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="555"/>
        <source>Paste</source>
        <translation>སྦྱར་པ།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="562"/>
        <source>Refresh</source>
        <translation>གསར་འདོན།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="576"/>
        <source>Properties</source>
        <translation>གཏོགས་གཤིས།</translation>
    </message>
    <message>
        <source>P&amp;roperties</source>
        <translation type="vanished">属性(&amp;R)</translation>
    </message>
    <message>
        <source>Sort Order...</source>
        <translation type="vanished">排列顺序...</translation>
    </message>
    <message>
        <source>Ascending Order</source>
        <translation type="vanished">升序</translation>
    </message>
    <message>
        <source>Descending Order</source>
        <translation type="vanished">降序</translation>
    </message>
    <message>
        <source>Zoom &amp;In</source>
        <translation type="vanished">放大(&amp;I)</translation>
    </message>
    <message>
        <source>Zoom &amp;Out</source>
        <translation type="vanished">缩小(&amp;O)</translation>
    </message>
    <message>
        <source>&amp;Restore all</source>
        <translation type="vanished">恢复全部(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Clean the trash</source>
        <translation type="vanished">清空回收站(&amp;C)</translation>
    </message>
    <message>
        <source>Delete Permanently</source>
        <translation type="vanished">永久删除</translation>
    </message>
    <message>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="vanished">您确定要删除这些文件吗？一旦开始删除，这些文件将不可再恢复。</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="vanished">复制(&amp;C)</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="vanished">剪切(&amp;T)</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">删除(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="vanished">重命名(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="vanished">粘贴(&amp;P)</translation>
    </message>
    <message>
        <source>&amp;Refresh</source>
        <translation type="vanished">刷新(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Properties</source>
        <translation type="vanished">属性(&amp;P)</translation>
    </message>
</context>
<context>
    <name>Peony::DesktopWindow</name>
    <message>
        <source>Desktop</source>
        <translation type="vanished">桌面</translation>
    </message>
    <message>
        <source>set background</source>
        <translation type="vanished">设置壁纸</translation>
    </message>
    <message>
        <source>New Folder</source>
        <translation type="vanished">新建文件夹</translation>
    </message>
</context>
<context>
    <name>PeonyDesktopApplication</name>
    <message>
        <source>Close the peony-qt desktop window</source>
        <translation type="vanished">关闭桌面并退出</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="233"/>
        <source>peony-qt-desktop</source>
        <translation>ཅོག་ངོས།</translation>
    </message>
    <message>
        <source>Peony-Qt Desktop</source>
        <translation type="vanished">桌面</translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation type="vanished">桌面</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="567"/>
        <source>Close the peony desktop window</source>
        <translation>ཅོག་ངོས་སྒོ་རྒྱག་པའི་བྱ་རིམ།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="570"/>
        <source>Take over the dbus service.</source>
        <translation>DBusཞབས་ཞུ་རྩིས་ལེན་བདག་སྤྲོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="573"/>
        <source>Take over the desktop displaying</source>
        <translation>ཅོག་ངོས་རྩིས་ལེན་བདག་སྤྲོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="576"/>
        <source>Setup backgrounds</source>
        <translation>Setupbackgrounds</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="579"/>
        <source>Clear standard icons</source>
        <translation>Clearstandardicons</translation>
    </message>
    <message>
        <source>set background</source>
        <translation type="vanished">设置背景</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../peony-qt-desktop/desktopbackgroundwindow.cpp" line="78"/>
        <source>set background</source>
        <translation>རྒྱབ་ལྗོངས།</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktopbackgroundwindow.cpp" line="83"/>
        <source>display settings</source>
        <translation>མངོན་འཆར་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <source>set resolution</source>
        <translation type="vanished">设置分辨率</translation>
    </message>
</context>
</TS>
