<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>Peony::DesktopIconView</name>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.h" line="81"/>
        <source>Desktop Icon View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="648"/>
        <source>New Folder</source>
        <translation type="unfinished">Yeni Klasör</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="740"/>
        <source>set background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="957"/>
        <source>Open Link failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="958"/>
        <source>File not exist, do you want to delete the link file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="973"/>
        <source>Open failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="974"/>
        <source>Open directory failed, you have no permission!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DesktopItemModel</name>
    <message>
        <location filename="../../peony-qt-desktop/desktop-item-model.cpp" line="582"/>
        <source>Computer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-item-model.cpp" line="584"/>
        <source>Trash</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DesktopMenu</name>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="235"/>
        <source>Reverse Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;New...</source>
        <translation type="vanished">&amp;Yeni...</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="249"/>
        <source>New...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="332"/>
        <source>New Folder</source>
        <translation type="unfinished">Yeni Klasör</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="349"/>
        <source>View Type...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="392"/>
        <source>Sort By...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="397"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="399"/>
        <source>File Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="400"/>
        <source>File Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="398"/>
        <source>Modified Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="107"/>
        <source>Open in new Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="112"/>
        <source>Select All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="135"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="163"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="197"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="208"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="143"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="168"/>
        <source>Open with...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="156"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="191"/>
        <source>More applications...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="211"/>
        <source>Open %1 selected files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="318"/>
        <source>Empty File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="328"/>
        <source>Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="355"/>
        <source>Small</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="358"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="361"/>
        <source>Large</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="364"/>
        <source>Huge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="472"/>
        <source>Clean the trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="486"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="490"/>
        <source>Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="511"/>
        <source>Delete to trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="520"/>
        <source>Delete forever</source>
        <translation>Kalıcı olarak sil</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="528"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="535"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="541"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="555"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>P&amp;roperties</source>
        <translation type="obsolete">Özellikler</translation>
    </message>
    <message>
        <source>Sort Order...</source>
        <translation type="vanished">Sıralama Düzeni...</translation>
    </message>
    <message>
        <source>Ascending Order</source>
        <translation type="vanished">Artan Düzen</translation>
    </message>
    <message>
        <source>Descending Order</source>
        <translation type="vanished">Azalan Düzen</translation>
    </message>
    <message>
        <source>Zoom &amp;In</source>
        <translation type="vanished">Yakınlaştır</translation>
    </message>
    <message>
        <source>Zoom &amp;Out</source>
        <translation type="vanished">Uzaklaştır</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="474"/>
        <source>Delete Permanently</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="474"/>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="vanished">Kes</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">Sil</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="vanished">Yeniden Adlandır</translation>
    </message>
    <message>
        <source>&amp;Properties</source>
        <translation type="vanished">Özellikler</translation>
    </message>
</context>
<context>
    <name>Peony::DesktopWindow</name>
    <message>
        <source>Desktop</source>
        <translation type="obsolete">Masaüstü</translation>
    </message>
    <message>
        <source>New Folder</source>
        <translation type="vanished">Yeni Klasör</translation>
    </message>
</context>
<context>
    <name>PeonyDesktopApplication</name>
    <message>
        <source>Close the peony-qt desktop window</source>
        <translation type="vanished">Masaüstünü kapatın ve çıkın</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="180"/>
        <source>peony-qt-desktop</source>
        <translation>Masaüstü</translation>
    </message>
    <message>
        <source>Peony-Qt Desktop</source>
        <translation type="vanished">Masaüstü</translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation type="vanished">Masaüstü</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="454"/>
        <source>Close the peony desktop window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="457"/>
        <source>Take over the dbus service.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="460"/>
        <source>Take over the desktop displaying</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="463"/>
        <source>Setup backgrounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="466"/>
        <source>Clear standard icons</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../peony-qt-desktop/desktopbackgroundwindow.cpp" line="70"/>
        <source>set background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktopbackgroundwindow.cpp" line="75"/>
        <source>set resolution</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
