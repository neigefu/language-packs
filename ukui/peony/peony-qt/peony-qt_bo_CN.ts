<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="32"/>
        <source>Dialog</source>
        <translation>དཀར་ཁུང་།</translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="88"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;DOCTYPEHTMLPUBLIC་/W3C/DTDHTML་4.0/EN&quot;htp：/wW.w.w.30.org/TRav-html40/strict.dtml&gt;&lt;head&gt;&lt;metanam=qurichtrichtexter&apos;cncnnn=1styy&apos;&apos;&apos;&apos;&apos;&apos;&apos;&apos;&apos;&apos;styy&apos;&apos;&apos;&apos;&apos;&apos;&apos;&apos;&apos;&apos;&apos;&apos;&apos;&apos;&apos;&apos;&apos;&apos;xtmain/pachrit/sachrit-ain-nafrafafa-st-st-st-hrin-Sain-st-Sain-st-hrnnnno</translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="115"/>
        <source>TextLabel</source>
        <translation>ཡིག་བྱང་།</translation>
    </message>
    <message>
        <source>Offical Website: </source>
        <translation type="vanished">官方网站: </translation>
    </message>
    <message>
        <source>Service &amp; Technology Support: </source>
        <translation type="vanished">服务与技术支持: </translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="39"/>
        <location filename="../../src/windows/about-dialog.cpp" line="141"/>
        <source>Service &amp; Support: </source>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར་ཚོགས་པ། </translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="38"/>
        <location filename="../../src/windows/about-dialog.cpp" line="71"/>
        <location filename="../../src/windows/about-dialog.cpp" line="87"/>
        <source>Peony</source>
        <translation>ཡིག་ཆའི་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <source>peony</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="43"/>
        <location filename="../../src/windows/about-dialog.cpp" line="102"/>
        <source>Peony is a graphical software to help users manage system files. It provides common file operation functions for users, such as file viewing, file copy, paste, cut, delete, rename, file selection, application opening, file search, file sorting, file preview, etc. it is convenient for users to manage system files intuitively on the interface.</source>
        <translation>ཡིག་ཆ་དོ་དམ་ཡོ་བྱད་ནི་སྤྱོད་མཁན་དོ་དམ་མ་ལག་གི་ཡིག་ཆའི་དཔེ་རིས་ཅན་གྱི་མཉེན་ཆས་ཤིག་ཡིན་ཞིང་།སྤྱོད་མཁན་ལ་རྒྱུན་བཀོལ་གྱི་ཡིག་ཆའི་བཀོལ་སྤྱོད་བྱེད་ནུས་འདོན་སྤྲོད་བྱེད་པ་དཔེར་ན།ཡིག་ཆར་ལྟ་ཞིབ་བྱེད་པ་དང་།ཡིག་ཆ་འདྲ་བཟོ་དང་།སྦྱར་སྦྱར།གཏུབ་པ།བསུབ་པ།མིང་བཏགས་པ།ཡིག་ཆའི་ཁ་ཕྱེ་སྟངས་འདེམ་པ།ཡིག་ཆ་འཚོལ་བཤེར།ཡིག་ཆའི་གོ་རིམ་སྒྲིག་པ།ཡིག་ཆའི་སྔོན་ལྟ་སོགས་བྱས་ཏེ།སྤྱོད་མཁན་གྱིས་ཐད་ཀར་དོ་དམ་མ་ལག་གི་ཡིག་ཆར་སྟབས་བདེ་བསྐྲུན་དགོས།</translation>
    </message>
    <message>
        <source>Hot Service: </source>
        <translation type="vanished">服务热线: </translation>
    </message>
    <message>
        <source>File Manager</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="42"/>
        <location filename="../../src/windows/about-dialog.cpp" line="90"/>
        <source>Version number: %1</source>
        <translation>པར་དེབ།%</translation>
    </message>
    <message>
        <source>File manager is a graphical software to help users manage system files. It provides common file operation functions for users, such as file viewing, file copy, paste, cut, delete, rename, file selection, application opening, file search, file sorting, file preview, etc. it is convenient for users to manage system files intuitively on the interface.</source>
        <translation type="vanished">文件管理器是一款帮助用户管理系统文件的图形化的软件，为用户提供常用的文件操作功能，比如文件查看，文件复制、粘贴、剪切、删除、重命名，文件打开方式选择，文件搜索，文件排序，文件预览等，方便用户在界面上直观地管理系统文件。</translation>
    </message>
    <message>
        <source>none</source>
        <translation type="vanished">无</translation>
    </message>
</context>
<context>
    <name>FileLabelBox</name>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="68"/>
        <source>Rename</source>
        <translation>མིང་བཏགས་པ།</translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="73"/>
        <source>Edit Color</source>
        <translation>ཁ་མདོག་སྒྲིག་བཟོ།</translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="84"/>
        <source>Delete</source>
        <translation>མཚོན་རྟགས།</translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="89"/>
        <source>Create New Label</source>
        <translation>མཚོན་རྟགས།</translation>
    </message>
</context>
<context>
    <name>HeaderBar</name>
    <message>
        <source>Create Folder</source>
        <translation type="vanished">新建文件夹</translation>
    </message>
    <message>
        <source>Open Terminal</source>
        <translation type="vanished">打开终端</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="139"/>
        <source>Go Back</source>
        <translation>ཕྱི་ནུར་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="146"/>
        <source>Go Forward</source>
        <translation>མདུན་དུ་སྐྱོད།</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="205"/>
        <source>Search</source>
        <translation>བཤེར་འཚོལ།</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="370"/>
        <source>View Type</source>
        <translation>མཐོང་རིས།</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="395"/>
        <source>Sort Type</source>
        <translation>གོ་རིམ་རིགས།</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="429"/>
        <source>Option</source>
        <translation>གདམ་ག</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="270"/>
        <source>Operate Tips</source>
        <translation>བཀོལ་སྤྱོད་སྣེ་སྟོན།</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="271"/>
        <source>Don&apos;t find any terminal, please install at least one terminal!</source>
        <translation>མཐའ་སྣེའི་བར་འཇུག་ཅི་ཡང་མ་རྙེད་པ་རེད།ཁྱེད་ཀྱིས་མ་མཐར་ཡང་གཅིག་སྒྲིག་སྦྱོར་བྱས་ཡོད་པ་གཏན་ཁེལ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="596"/>
        <source>Restore</source>
        <translation>སྔར་གྱི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="599"/>
        <source>Maximize</source>
        <translation>ཆེས་ཆེ་བ།</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">最小化</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
</context>
<context>
    <name>HeaderBarContainer</name>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="756"/>
        <source>Minimize</source>
        <translation>ཆེས་ཆུང་བ།</translation>
    </message>
    <message>
        <source>Maximize/Restore</source>
        <translation type="vanished">最大化/还原</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation type="vanished">还原</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="vanished">最大化</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="777"/>
        <source>Close</source>
        <translation>སྒོ་གཏན་རོགས།</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="835"/>
        <source>File Manager</source>
        <translation>ཡིག་ཆའི་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="362"/>
        <source>Undo</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="369"/>
        <source>Redo</source>
        <translation>བསྐྱར་སྒྲུབ།</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="744"/>
        <source>warn</source>
        <translation>ཉེན་བརྡ།</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="744"/>
        <source>This operation is not supported.</source>
        <translation>བཀོལ་སྤྱོད་འདི་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="833"/>
        <source>Search</source>
        <translation>བཤེར་འཚོལ།</translation>
    </message>
    <message>
        <source>Tips info</source>
        <translation type="vanished">温馨提示</translation>
    </message>
    <message>
        <source>Trash has no file need to be cleaned.</source>
        <translation type="vanished">回收站没有文件需要被清空！</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <source>Delete Permanently</source>
        <translation type="vanished">永久删除</translation>
    </message>
    <message>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="vanished">您确定要删除这些文件吗？一旦开始删除，这些文件将不可再恢复。</translation>
    </message>
    <message>
        <source>Peony Qt</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="848"/>
        <source>New Folder</source>
        <translation>ཡིག་སྒམ།</translation>
    </message>
</context>
<context>
    <name>NavigationSideBar</name>
    <message>
        <source>All tags...</source>
        <translation type="vanished">所有标记...</translation>
    </message>
    <message>
        <source>Open In &amp;New Window</source>
        <translation type="vanished">在新窗口中打开(&amp;N)</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="470"/>
        <source>warn</source>
        <translation>ཉེན་བརྡ།</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="470"/>
        <source>This operation is not supported.</source>
        <translation>བཀོལ་སྤྱོད་འདི་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="226"/>
        <location filename="../../src/control/navigation-side-bar.cpp" line="508"/>
        <location filename="../../src/control/navigation-side-bar.cpp" line="526"/>
        <source>Tips</source>
        <translation>སྣེ་སྟོན།</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="226"/>
        <source>The device is in busy state, please perform this operation later.</source>
        <translation>སྒྲིག་ཆས་བྲེལ་འཚུབ་ཀྱི་རྣམ་པར་གནས་པས་ཅུང་ཙམ་འགོར་རྗེས་བཀོལ་སྤྱོད་འདི་ལག་བསྟར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="508"/>
        <source>This is an abnormal Udisk, please fix it or format it</source>
        <translation>འདི་ནི་ཧ་ཅང་རྒྱུན་ལྡན་མིན་པའི་Uསྡེར་ཞིག་རེད།དེ་ཉམས་གསོ་དང་རྣམ་གཞག་ཅན་དུ་གཏོང་རོགས།</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="526"/>
        <source>This is an empty drive, please insert a Disc.</source>
        <translation>འདི་ནི་འོད་སྡེར་སྟོང་བ་ཞིག་རེད།འོད་སྡེར་ནང་དུ་འཇོག་རོགས།</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="254"/>
        <source>Open In New Window</source>
        <translation>སྒེའུ་ཁུང་གསར་བའི་ནང་ནས་ཁ་ཕྱེས།</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="271"/>
        <location filename="../../src/control/navigation-side-bar.cpp" line="307"/>
        <source>Can not open %1, %2</source>
        <translation>བརྒྱ་ཆ་12ཁ་ཕྱེ་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="290"/>
        <source>Open In New Tab</source>
        <translation>ཤོག་བྱང་གསར་བའི་ངོས་སུ་ཁ་ཕྱེས།</translation>
    </message>
    <message>
        <source>Open In New &amp;Tab</source>
        <translation type="vanished">在新标签页中打开(&amp;T)</translation>
    </message>
</context>
<context>
    <name>NavigationSideBarContainer</name>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="686"/>
        <source>All tags...</source>
        <translation>རྟགས་ཡོད་ཚད།</translation>
    </message>
</context>
<context>
    <name>NavigationTabBar</name>
    <message>
        <source>Computer</source>
        <translation type="vanished">计算机</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-tab-bar.cpp" line="127"/>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation>བརྒྱ་ཆ2ནང་བཤེར་འཚོལ་བྱས་པ།</translation>
    </message>
</context>
<context>
    <name>OperationMenu</name>
    <message>
        <source>Advance Search</source>
        <translation type="vanished">高级搜索</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="77"/>
        <source>Keep Allow</source>
        <translation>རྩེ་མོའི་སྐར་ཁུངས།</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="86"/>
        <source>Show Hidden</source>
        <translation>མངོན་མེད་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="94"/>
        <source>Show File Extension</source>
        <translation>ཡིག་ཆའི་འགྲེམས་མིང་མངོན་པ།</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="99"/>
        <source>Show Create Time</source>
        <translation>གསར་འཛུགས་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="105"/>
        <source>Forbid thumbnailing</source>
        <translation>བཀག་སྡོམ་བསྡུས་རིས།</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="114"/>
        <source>Resident in Backend</source>
        <translation>རྒྱུན་སྡོད་རྒྱབ་རྟེན།</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="123"/>
        <source>Parallel Operations</source>
        <translation>བཀོལ་སྤྱོད་གཤིབ་བགྲོད་བྱས་ཆོག</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="129"/>
        <source>Set samba password</source>
        <translation>sambaགསང་གྲངས་བཞག</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="143"/>
        <source>Tips</source>
        <translation>སྣེ་སྟོན།</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="143"/>
        <source>The user already has a samba password, do you need to reset the samba password?</source>
        <translation>སྤྱོད་མཁན་འདི་ལ་sambaགསང་གྲངས་ཡོད་ལ།sambaགསང་གྲངས་བསྐྱར་དུ་འཇོག་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="154"/>
        <source>Samba set user password</source>
        <translation>Sambaཡིས་མཛད་སྤྱོད་པའི་གསང་གྲངས་བཞག་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="152"/>
        <source>Samba password:</source>
        <translation>Sambaགསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="162"/>
        <location filename="../../src/control/operation-menu.cpp" line="173"/>
        <source>Warning</source>
        <translation>ཉེན་བརྡ།</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="162"/>
        <source>Samba set password failed, Please re-enter!</source>
        <translation>Sambaཡིས་གསང་གྲངས་བཞག་ནས་ཕམ་སོང་།ཡང་བསྐྱར་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="173"/>
        <source>Shared configuration service exception, please confirm if there is an ongoing shared configuration operation, or please reset the share!</source>
        <translation>མཉམ་སྤྱོད་སྒྲིག་ཆས་ཀྱི་ཞབས་ཞུ་རྒྱུན་ལྡན་མིན་པས།ད་ལྟ་སྒྲུབ་བཞིན་པའི་མཉམ་སྤྱོད་སྒྲིག་ཆས་ཡོད་མེད་དང་།ཡང་ན་བསྐྱར་དུ་བཀོད་སྒྲིག་དང་མཉམ་སྤྱོད་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="182"/>
        <source>Open each folder in a new window</source>
        <translation>སྒེའུ་ཁུང་གསར་བ་ནས་ཡིག་ཆའི་སྒམ་ཕྱེ།</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="188"/>
        <source>Plugin manager Settings</source>
        <translation>བསྒར་ལྷུ་དོ་དམ་སྒྲིག་འགོད།</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="197"/>
        <source>Help</source>
        <translation>རོགས་རམ།</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="201"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི།</translation>
    </message>
</context>
<context>
    <name>OperationMenuEditWidget</name>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="248"/>
        <source>Edit</source>
        <translation>རྩོམ་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="259"/>
        <source>copy</source>
        <translation>འདྲ་བཟོ།</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="268"/>
        <source>paste</source>
        <translation>སྦྱར་པ།</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="277"/>
        <source>cut</source>
        <translation>དྲས་གཏུབ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="286"/>
        <source>trash</source>
        <translation>སུབ་པ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>PeonyApplication</name>
    <message>
        <source>Peony-Qt</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="162"/>
        <source>peony-qt</source>
        <translation>ཡིག་ཆའི་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="169"/>
        <source>Files or directories to open</source>
        <translation>ཁ་ཕྱེ་དགོས་པའི་ཡིག་ཆའམ་ཡིག་སྒམ།</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="169"/>
        <source>[FILE1, FILE2,...]</source>
        <translation>ཡིག་ཆ1ཡིག་ཆ2</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="209"/>
        <source>Warning</source>
        <translation>ཉེན་བརྡ།</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="209"/>
        <source>Peony-Qt can not get the system&apos;s icon theme. There are 2 reasons might lead to this problem:

1. Peony-Qt might be running as root, that means you have the higher permission and can do some things which normally forbidden. But, you should learn that if you were in a root, the virtual file system will lose some featrue such as you can not use &quot;My Computer&quot;, the theme and icons might also went wrong. So, run peony-qt in a root is not recommended.

2. You are using a non-qt theme for your system but you didn&apos;t install the platform theme plugin for qt&apos;s applications. If you are using gtk-theme, try installing the qt5-gtk2-platformtheme package to resolve this problem.</source>
        <translation>ཡིག་ཆའི་དོ་དམ་ཆས་ཀྱིས་མ་ལག་གི་རི་མོའི་བརྗོད་བྱ་ལེན་མི་ཐུབ།དེའི་རྒྱུ་རྐྱེན་ནི།1.དོ་དམ་པས་ཡིག་ཆ་དོ་དམ་ཆས་བཀོལ་བཞིན་ཡོད།དེ་ལས་ཁྱོད་ལ་དེ་ལས་མཐོ་བའི་དབང་ཆ་ཡོད་པ་མཚོན་ཡོད།འོན་ཀྱང་ཁྱོད་ཀྱིས་ངེས་པར་དུ་ཁྱོད་ལ་དུས་མཉམ་དུ་སྤྱོད་མཁན་དཀྱུས་མའི་འོག་ཏུ་ད་གཟོད་ཡོད་པའི་ཁྱད་ཆོས་འགའ་ཤོར་སོང་བ་རེད།དཔེར་ན།“ངའི་གློག་ཀླད”དང་མ་ལག་གི་བརྗོད་བྱ་གཙོ་བོ་རེད།གལ་ཏེ་དམིགས་བསལ་གྱི་གནས་ཚུལ་མིན་ན།དོ་དམ་པའི་སྤྱོད་མཁན་གྱིས་ཡིག་ཆ་དོ་དམ་ཆས་ཀྱི་ཁ་ཕྱེ་མི་དགོས།2.ཁྱོད་ཀྱིས་བཀོལ་སྤྱོད་བྱེད་པའི་མ་ལག་གི་བརྗོད་བྱ་གཙོ་བོ་ནི་qutཁས་ལེན་རྒྱབ་སྐྱོར་བྱེད་པའི་བརྗོད་བྱ་གཙོ་བོ་མིན་པར་མ་ཟད།ཁྱོད་ཀྱིས་འབྲེལ་ཡོད་ཀྱི་སྟེགས་བུ་བསྒར་མེད་པ་རེད།གལ་ཏེ་ཁྱོད་ཀྱིས་Gtk—mt-fatoreབརྗོད་བྱ་གཙོ་བོ་བྱས་ནས་ཚོད་ལྟ་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="470"/>
        <source>Peony Qt</source>
        <translation>ཡིག་ཆའི་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="471"/>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2020, KylinSoft Co., Ltd.</source>
        <translation>རྩོམ་པ་པོ།YueLan〈lanyue.kylinos.cn〉MeihongHe〈hemeihong.kylinos.cn〉པར་དབང་ཡོད་ཚད།（C:2020ལོ།）ཆི་ལིན་མཉེན་ཆས་ཚད་ཡོད་ཀུང་སི།</translation>
    </message>
    <message>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,麒麟软件有限公司.</translation>
    </message>
    <message>
        <source>Author: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,麒麟软件有限公司.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,麒麟软件有限公司.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="67"/>
        <source>Close all peony-qt windows and quit</source>
        <translation>སྒེའུ་ཁུང་ཚང་མ་བརྒྱབ་ནས་ཕྱིར་བུད།</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="68"/>
        <source>Show items</source>
        <translation>ཡིག་ཆ་ཡོད་སའི་དཀར་ཆག་གི་ཁ་ཕྱེ་ནས་དེ་ཚོ་འདེམས་དགོས།</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="69"/>
        <source>Show folders</source>
        <translation>ཡིག་ཆའི་འོག་གི་ནང་དོན་མངོན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="70"/>
        <source>Show properties</source>
        <translation>ཡིག་ཆའི་ངོ་བོའི་སྒེའུ་ཁུང་ཕྱེ།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>Can not open %1.</source>
        <translation type="vanished">无法打开 %1.</translation>
    </message>
</context>
<context>
    <name>SortTypeMenu</name>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="34"/>
        <source>File Name</source>
        <translation>ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="46"/>
        <source>File Size</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="50"/>
        <source>Original Path</source>
        <translation>གདོད་མའི་ཐབས་ལམ།</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="82"/>
        <source>Use global sorting</source>
        <translation>ཁྱོན་ཡོངས་ཀྱི་གོ་རིམ་བེད་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="42"/>
        <source>File Type</source>
        <translation>ཡིག་ཆའི་རིགས།</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="38"/>
        <source>Modified Date</source>
        <translation>བཟོ་བཅོས་བརྒྱབ་པའི་ཚེས་གྲངས།</translation>
    </message>
    <message>
        <source>Modified Data</source>
        <translation type="vanished">修改日期</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="71"/>
        <source>Ascending</source>
        <translation>རིམ་འཕར།</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="66"/>
        <source>Descending</source>
        <translation>རིམ་ཕབ།</translation>
    </message>
</context>
<context>
    <name>TabStatusBar</name>
    <message>
        <source>Current path has:</source>
        <translation type="vanished">当前路径包含：</translation>
    </message>
    <message>
        <source>%1 folders, %2 files</source>
        <translation type="vanished">%1 文件夹，%2 文件</translation>
    </message>
    <message>
        <source>%1 folders</source>
        <translation type="vanished">%1 文件夹</translation>
    </message>
    <message>
        <source>%1 files</source>
        <translation type="vanished">%1 文件</translation>
    </message>
    <message>
        <source>; %1 folders</source>
        <translation type="vanished">; %1 个文件夹</translation>
    </message>
    <message>
        <source>; %1 files, %2 total</source>
        <translation type="vanished">; %1 个文件, 共%2</translation>
    </message>
    <message>
        <source>; %1 folder</source>
        <translation type="vanished">; %1 个文件夹</translation>
    </message>
    <message>
        <source>; %1 file, %2</source>
        <translation type="vanished">; %1 个文件, %2</translation>
    </message>
    <message>
        <source>%1 selected</source>
        <translation type="vanished">选中%1个</translation>
    </message>
    <message>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation type="vanished">在%2中搜索%1</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="109"/>
        <location filename="../../src/control/tab-status-bar.cpp" line="218"/>
        <source>Searching for files ...</source>
        <translation>འཚོལ་ཞིབ་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="122"/>
        <source> %1 items </source>
        <translation> %རྣམ་གྲངས། </translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="159"/>
        <source> selected %1 items    %2</source>
        <translation> %རྣམ་གྲངས་2འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="161"/>
        <source> selected %1 items</source>
        <translation> རྣམ་གྲངས་1འདེམས་པ།</translation>
    </message>
</context>
<context>
    <name>TabWidget</name>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="302"/>
        <source>Trash</source>
        <translation>ཕྱིར་སྡུད་ས་ཚིགས།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="306"/>
        <source>Clear</source>
        <translation>བར་སྣང་གཙང་བཤེར།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="311"/>
        <source>Recover</source>
        <translation>སྔར་གྱི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="471"/>
        <source>Close Filter.</source>
        <translation>སྒོ་བརྒྱབ་ནས་གདམ་གསེས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="483"/>
        <source>Filter</source>
        <translation>གདམ་གསེས།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="580"/>
        <source>Select Path</source>
        <translation>གདམ་གའི་ཐབས་ལམ།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1109"/>
        <source>Current path: %1, %2</source>
        <translation>མིག་སྔའི་ཐབས་ལམ།%%%2</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1109"/>
        <source>click to select other search path.</source>
        <translation>མནན་ན་འཚོལ་བཤེར་ཐབས་ལམ་གཞན་པ་འདེམས་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1696"/>
        <source>Opening such files is not currently supported</source>
        <translation>གནས་སྐབས་ཡིག་ཆ་འདིའི་རིགས་ཀྱི་ཁ་འབྱེད་པར་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1709"/>
        <source>Open failed</source>
        <translation>ཕམ་ཁ་ཁ་ཕྱེ།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1710"/>
        <source>Open directory failed, you have no permission!</source>
        <translation>ཡིག་ཆའི་ཁ་ཕྱེ་ནས་ཕམ་སོང་།ཁྱེད་ལ་དཀར་ཆག་འདིའི་དབང་ཚད་མེད།</translation>
    </message>
    <message>
        <source>Close advance search.</source>
        <translation type="vanished">关闭高级搜索。</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="494"/>
        <source>Choose other path to search.</source>
        <translation>བཤེར་འཚོལ་ཐབས་ལམ་གཞན་པ་འདེམ་པ།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="503"/>
        <source>Search recursively</source>
        <translation>རིམ་ལོག་བཤེར་འཚོལ།</translation>
    </message>
    <message>
        <source>more options</source>
        <translation type="vanished">更多选项</translation>
    </message>
    <message>
        <source>Show/hide advance search</source>
        <translation type="vanished">显示/隐藏高级搜索</translation>
    </message>
    <message>
        <source>Select path</source>
        <translation type="vanished">选择路径</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="640"/>
        <location filename="../../src/control/tab-widget.cpp" line="795"/>
        <source>is</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="675"/>
        <source>Please input key words...</source>
        <translation>གནད་ཚིག་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Please input kay words...</source>
        <translation type="vanished">请输入关键词...</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="756"/>
        <location filename="../../src/control/tab-widget.cpp" line="779"/>
        <source>contains</source>
        <translation>དེའི་ནང་དུ།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="280"/>
        <source>name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="280"/>
        <source>type</source>
        <translation>རིགས་དབྱེ།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="280"/>
        <source>modify time</source>
        <translation>བཟོ་བཅོས་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="280"/>
        <source>file size</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="281"/>
        <location filename="../../src/control/tab-widget.h" line="283"/>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>all</source>
        <translation>ཚང་མ་ནི།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="281"/>
        <source>file folder</source>
        <translation>ཡིག་སྒམ།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="281"/>
        <source>image</source>
        <translation>པར་རིས།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="281"/>
        <source>video</source>
        <translation>བརྙན་རིས།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="282"/>
        <source>text file</source>
        <translation>ཡིག་རྐྱང་ཡིག་ཚགས།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="282"/>
        <source>audio</source>
        <translation>སྒྲ་གདངས།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="282"/>
        <source>others</source>
        <translation>གཞན་དག</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="282"/>
        <source>wps file</source>
        <translation>WPSཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="283"/>
        <source>today</source>
        <translation>དེ་རིང་།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="283"/>
        <source>this week</source>
        <translation>གཟའ་འཁོར་འདིར།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="283"/>
        <source>this month</source>
        <translation>ཟླ་འདིར།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="283"/>
        <source>this year</source>
        <translation>ད་ལོར།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="283"/>
        <source>year ago</source>
        <translation>ལོ་གཅིག་གི་སྔོན།</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>tiny(0-16K)</source>
        <translation>ཤིན་ཏུ་ཆུང་།(0-16K)</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>small(16k-1M)</source>
        <translation>ཧ་ཅང་ཆུང་།(16k-1M)</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>empty(0K)</source>
        <translation>སྟོང་།（0K）</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>medium(1M-128M)</source>
        <translation>འབྲིང་རིམ་(1M—128M)</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>big(128M-1G)</source>
        <translation>ཆེ།(128M—1G)</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>large(1-4G)</source>
        <translation>ཤིན་ཏུ་ཆེ་བ།(1-4G)</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>great(&gt;4G)</source>
        <translation>ཆེ།4G</translation>
    </message>
    <message>
        <source>medium(1M-100M)</source>
        <translation type="vanished">中等(1M-100M)</translation>
    </message>
    <message>
        <source>big(100M-1G)</source>
        <translation type="vanished">很大(100M-1G)</translation>
    </message>
    <message>
        <source>large(&gt;1G)</source>
        <translation type="vanished">极大(&gt;1G)</translation>
    </message>
</context>
</TS>
