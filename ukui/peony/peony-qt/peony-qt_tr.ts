<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr_TR">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="32"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="88"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="115"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="132"/>
        <source>Service &amp; Support: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="93"/>
        <source>Peony is a graphical software to help users manage system files. It provides common file operation functions for users, such as file viewing, file copy, paste, cut, delete, rename, file selection, application opening, file search, file sorting, file preview, etc. it is convenient for users to manage system files intuitively on the interface.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File Manager</source>
        <translation type="obsolete">Dosya Yöneticisi</translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="62"/>
        <location filename="../../src/windows/about-dialog.cpp" line="78"/>
        <source>Peony</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="81"/>
        <source>Version number: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="159"/>
        <source>none</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileLabelBox</name>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="68"/>
        <source>Rename</source>
        <translation>Yeniden Adlandır</translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="73"/>
        <source>Edit Color</source>
        <translation>Rengi Düzenle</translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="81"/>
        <source>Delete</source>
        <translation>Sil</translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="86"/>
        <source>Create New Label</source>
        <translation>Yeni Etiket Oluştur</translation>
    </message>
</context>
<context>
    <name>HeaderBar</name>
    <message>
        <source>Create Folder</source>
        <translation type="vanished">Klasör Oluştur</translation>
    </message>
    <message>
        <source>Open Terminal</source>
        <translation type="vanished">Uçbirim Aç</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="139"/>
        <source>Go Back</source>
        <translation>Geri</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="148"/>
        <source>Go Forward</source>
        <translation>İleri</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="210"/>
        <source>Search</source>
        <translation>Ara</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="373"/>
        <source>View Type</source>
        <translation>Görünüm Türü</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="399"/>
        <source>Sort Type</source>
        <translation>Sıralama Türü</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="426"/>
        <source>Option</source>
        <translation>Seçenek</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="276"/>
        <source>Operate Tips</source>
        <translation>İşlet İpuçları</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="277"/>
        <source>Don&apos;t find any terminal, please install at least one terminal!</source>
        <translation>Herhangi bir uçbirim bulunamadı, lütfen en az bir uçbirim kurun!</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="584"/>
        <source>Restore</source>
        <translation type="unfinished">Onar</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="587"/>
        <source>Maximize</source>
        <translation type="unfinished">Büyüt</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">Küçült</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Kapat</translation>
    </message>
</context>
<context>
    <name>HeaderBarContainer</name>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="744"/>
        <source>Minimize</source>
        <translation>Küçült</translation>
    </message>
    <message>
        <source>Maximize/Restore</source>
        <translation type="vanished">Büyüt/Onar</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation type="vanished">Onar</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="vanished">Büyüt</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="769"/>
        <source>Close</source>
        <translation>Kapat</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="734"/>
        <source>File Manager</source>
        <translation>Dosya Yöneticisi</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="318"/>
        <source>Undo</source>
        <translation>Geri</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="325"/>
        <source>Redo</source>
        <translation>İleri</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="668"/>
        <source>warn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="668"/>
        <source>This operation is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="732"/>
        <source>Search</source>
        <translation type="unfinished">Ara</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="1601"/>
        <source>Tips info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="1602"/>
        <source>Trash has no file need to be cleaned.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="obsolete">Dosya Silme Uyarısı</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="1590"/>
        <source>Delete Permanently</source>
        <translation>Kalıcı Olarak Sil</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="1591"/>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation>Bu dosyaları silmek istediğinizden emin misiniz? Bir silme işlemini başlattığınızda, silinen dosyalar bir daha geri yüklenmeyecektir.</translation>
    </message>
    <message>
        <source>Peony Qt</source>
        <translation type="vanished">Peony Qt</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="747"/>
        <source>New Folder</source>
        <translation>Yeni Klasör</translation>
    </message>
</context>
<context>
    <name>NavigationSideBar</name>
    <message>
        <source>All tags...</source>
        <translation type="vanished">Tüm etiketler...</translation>
    </message>
    <message>
        <source>Open In &amp;New Window</source>
        <translation type="obsolete">Yeni Pencerede Aç</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="358"/>
        <source>warn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="358"/>
        <source>This operation is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="396"/>
        <location filename="../../src/control/navigation-side-bar.cpp" line="409"/>
        <source>Tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="396"/>
        <source>This is an abnormal Udisk, please fix it or format it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="409"/>
        <source>This is an empty drive, please insert a Disc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="216"/>
        <source>Open In New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="232"/>
        <location filename="../../src/control/navigation-side-bar.cpp" line="266"/>
        <source>Can not open %1, %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="250"/>
        <source>Open In New Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open In New &amp;Tab</source>
        <translation type="obsolete">Yeni Sekmede Aç</translation>
    </message>
</context>
<context>
    <name>NavigationSideBarContainer</name>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="553"/>
        <source>All tags...</source>
        <translation>Tüm etiketler...</translation>
    </message>
</context>
<context>
    <name>NavigationTabBar</name>
    <message>
        <location filename="../../src/control/navigation-tab-bar.cpp" line="126"/>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation>&quot;%2&quot; de &quot;%1&quot; ara</translation>
    </message>
</context>
<context>
    <name>OperationMenu</name>
    <message>
        <source>Advance Search</source>
        <translation type="vanished">Gelişmiş Arama</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="69"/>
        <source>Keep Allow</source>
        <translation>İzin Ver</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="78"/>
        <source>Show Hidden</source>
        <translation>Gizlileri Göster</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="86"/>
        <source>Show File Extension</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="91"/>
        <source>Forbid thumbnailing</source>
        <translation>Küçük Resimleri Yasakla</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="100"/>
        <source>Resident in Backend</source>
        <translation>Arka Uçta Yerleşik</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="109"/>
        <source>Parallel Operations</source>
        <translation>Paralel İşlemler</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="118"/>
        <source>Help</source>
        <translation>Yardım</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="122"/>
        <source>About</source>
        <translation>Hakkında</translation>
    </message>
</context>
<context>
    <name>OperationMenuEditWidget</name>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="169"/>
        <source>Edit</source>
        <translation>Düzenle</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="180"/>
        <source>copy</source>
        <translation type="unfinished">Kopyala</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="189"/>
        <source>paste</source>
        <translation type="unfinished">Yapıştır</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="198"/>
        <source>cut</source>
        <translation type="unfinished">Kes</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="207"/>
        <source>trash</source>
        <translation type="unfinished">Çöp</translation>
    </message>
</context>
<context>
    <name>PeonyApplication</name>
    <message>
        <source>Peony-Qt</source>
        <translation type="vanished">Dosya Yöneticisi</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="144"/>
        <source>peony-qt</source>
        <translation>Dosya Yöneticisi</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="151"/>
        <source>Files or directories to open</source>
        <translation>Açılacak dosyalar veya dizinler</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="151"/>
        <source>[FILE1, FILE2,...]</source>
        <translation>[DOSYA1, DOSYA2,...]</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="191"/>
        <source>Warning</source>
        <translation>Uyarı</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="191"/>
        <source>Peony-Qt can not get the system&apos;s icon theme. There are 2 reasons might lead to this problem:

1. Peony-Qt might be running as root, that means you have the higher permission and can do some things which normally forbidden. But, you should learn that if you were in a root, the virtual file system will lose some featrue such as you can not use &quot;My Computer&quot;, the theme and icons might also went wrong. So, run peony-qt in a root is not recommended.

2. You are using a non-qt theme for your system but you didn&apos;t install the platform theme plugin for qt&apos;s applications. If you are using gtk-theme, try installing the qt5-gtk2-platformtheme package to resolve this problem.</source>
        <translation>Peony-Qt sistem simgesi teması alınamıyor. Bu soruna yol açabilecek 2 neden vardır:

1. Peony-Qt root olarak çalışıyor olabilir, bu da daha yüksek izne sahip olduğunuz ve normalde yasak olan bazı şeyleri yapabileceğiniz anlamına gelir. Ancak, eğer root iseniz, sanal dosya sisteminin Bilgisayarımı kullanamayacağınız gibi bazı özellikleri kaybedeceğini, temanın ve simgelerin de yanlış gidebileceğini öğrenmelisiniz. Yani, bir root olarak Peony-qt çalıştırmak önerilmez.

2. Sisteminiz için qt olmayan bir tema kullanıyorsunuz ancak qt uygulamaları için platform tema eklentisini yüklemediniz. Gtk-theme kullanıyorsanız, bu sorunu çözmek için qt5-gtk2-platformtheme paketini yüklemeyi deneyin.</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="450"/>
        <source>Peony Qt</source>
        <translation>Dosya Yöneticisi</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="451"/>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2020, KylinSoft Co., Ltd.</source>
        <translation type="unfinished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Telif Hakkı (C): 2020, KylinSoft Co., Ltd.</translation>
    </message>
    <message>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Telif Hakkı(C): 2019-2020, KYLIN Software Co., Ltd.</translation>
    </message>
    <message>
        <source>Author: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Telif (C): 2019-2020, KYLIN Software Co., Ltd.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Telif (C): 2019-2020, KYLIN Software Co., Ltd.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Telif (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="67"/>
        <source>Close all peony-qt windows and quit</source>
        <translation>Tüm peony-qt pencerelerini kapatın ve çıkın</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="68"/>
        <source>Show items</source>
        <translation>Öğeleri göster</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="69"/>
        <source>Show folders</source>
        <translation>Klasörleri göster</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="70"/>
        <source>Show properties</source>
        <translation>Seçenekleri göster</translation>
    </message>
</context>
<context>
    <name>SortTypeMenu</name>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="34"/>
        <source>File Name</source>
        <translation>Dosya Adı</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="46"/>
        <source>File Size</source>
        <translation>Dosya Boyutu</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="50"/>
        <source>Original Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="82"/>
        <source>Use global sorting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="42"/>
        <source>File Type</source>
        <translation>Dosya Türü</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="38"/>
        <source>Modified Date</source>
        <translation>Düzenleme Tarihi</translation>
    </message>
    <message>
        <source>Modified Data</source>
        <translation type="vanished">Düzenlenen Veri</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="71"/>
        <source>Ascending</source>
        <translation>Artan</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="66"/>
        <source>Descending</source>
        <translation>Azalan</translation>
    </message>
</context>
<context>
    <name>TabStatusBar</name>
    <message>
        <source>; %1 folders</source>
        <translation type="vanished">; %1 klasör</translation>
    </message>
    <message>
        <source>; %1 files, %2 total</source>
        <translation type="vanished">; %1 dosya, %2 toplam</translation>
    </message>
    <message>
        <source>; %1 folder</source>
        <translation type="vanished">; %1 klasör</translation>
    </message>
    <message>
        <source>; %1 file, %2</source>
        <translation type="vanished">; %1 dosya, %2</translation>
    </message>
    <message>
        <source>%1 selected</source>
        <translation type="vanished">%1 seçildi</translation>
    </message>
    <message>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation type="vanished">%1 de %1 bul</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="102"/>
        <source> %1 items </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="138"/>
        <source> selected %1 items    %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="140"/>
        <source> selected %1 items</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabWidget</name>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="297"/>
        <source>Trash</source>
        <translation>Çöp</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="301"/>
        <source>Clear</source>
        <translation>Temizle</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="306"/>
        <source>Recover</source>
        <translation>Geri Yükle</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="418"/>
        <source>Close Filter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="430"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="527"/>
        <source>Select Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1574"/>
        <source>Open failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1575"/>
        <source>Open directory failed, you have no permission!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close advance search.</source>
        <translation type="vanished">Gelişmiş aramayı kapat.</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Ara</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="441"/>
        <source>Choose other path to search.</source>
        <translation>Aramak için başka bir yol seçin.</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="450"/>
        <source>Search recursively</source>
        <translation>Yinelemeli olarak ara</translation>
    </message>
    <message>
        <source>more options</source>
        <translation type="vanished">Daha fazla seçenek</translation>
    </message>
    <message>
        <source>Show/hide advance search</source>
        <translation type="vanished">Gelişmiş aramayı göster/gizle</translation>
    </message>
    <message>
        <source>Select path</source>
        <translation type="obsolete">Yol seç</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="584"/>
        <location filename="../../src/control/tab-widget.cpp" line="720"/>
        <source>is</source>
        <translation>de</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="602"/>
        <source>Please input key words...</source>
        <translation>Lütfen anahtar kelimeler girin ...</translation>
    </message>
    <message>
        <source>Please input kay words...</source>
        <translation type="vanished">Lütfen anahtar kelimeler girin ...</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="681"/>
        <location filename="../../src/control/tab-widget.cpp" line="704"/>
        <source>contains</source>
        <translation>İçerik</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="273"/>
        <source>name</source>
        <translation>İsim</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="273"/>
        <source>type</source>
        <translation>Tür</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="273"/>
        <source>modify time</source>
        <translation>Değiştirme Zamanı</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="273"/>
        <source>file size</source>
        <translation>Dosya Boyutu</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="274"/>
        <location filename="../../src/control/tab-widget.h" line="276"/>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>all</source>
        <translation>Tümü</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="274"/>
        <source>file folder</source>
        <translation>Dosya klasör</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="274"/>
        <source>image</source>
        <translation>Resim</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="274"/>
        <source>video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="275"/>
        <source>text file</source>
        <translation>Metin dosyası</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="275"/>
        <source>audio</source>
        <translation>Ses</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="275"/>
        <source>others</source>
        <translation>Diğer</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="275"/>
        <source>wps file</source>
        <translation type="unfinished">Wps Dosyası</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="276"/>
        <source>today</source>
        <translation>Bugün</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="276"/>
        <source>this week</source>
        <translation>Bu hafta</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="276"/>
        <source>this month</source>
        <translation>Bu ay</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="276"/>
        <source>this year</source>
        <translation>Bu yıl</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="276"/>
        <source>year ago</source>
        <translation>Geçen yıl</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>tiny(0-16K)</source>
        <translation>Çok küçük(0-16K)</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>small(16k-1M)</source>
        <translation>Küçük(16k-1M)</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>empty(0K)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>medium(1M-128M)</source>
        <translation type="unfinished">Orta(1M-100M) {1M?} {128M?}</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>big(128M-1G)</source>
        <translation type="unfinished">Büyük(100M-1G) {128M?} {1G?}</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>large(1-4G)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="277"/>
        <source>great(&gt;4G)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>medium(1M-100M)</source>
        <translation type="vanished">Orta(1M-100M)</translation>
    </message>
    <message>
        <source>big(100M-1G)</source>
        <translation type="vanished">Büyük(100M-1G)</translation>
    </message>
    <message>
        <source>large(&gt;1G)</source>
        <translation type="vanished">Daha büyük(&gt;1G)</translation>
    </message>
</context>
</TS>
