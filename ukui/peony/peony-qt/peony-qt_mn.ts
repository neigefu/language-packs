<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="32"/>
        <source>Dialog</source>
        <translation>ᠴᠣᠩᠬᠣ</translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="88"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="115"/>
        <source>TextLabel</source>
        <translation>ᠱᠣᠰᠢᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>Offical Website: </source>
        <translation type="vanished">官方网站: </translation>
    </message>
    <message>
        <source>Service &amp; Technology Support: </source>
        <translation type="vanished">服务与技术支持: </translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="39"/>
        <location filename="../../src/windows/about-dialog.cpp" line="141"/>
        <source>Service &amp; Support: </source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠬᠢᠭᠡᠳ ᠳᠡᠮᠵᠢᠬᠦ ᠪᠦᠯᠬᠦᠮ ᠄ </translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="38"/>
        <location filename="../../src/windows/about-dialog.cpp" line="71"/>
        <location filename="../../src/windows/about-dialog.cpp" line="87"/>
        <source>Peony</source>
        <translation>ᠹᠧᠢᠯ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠤᠷ</translation>
    </message>
    <message>
        <source>peony</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="43"/>
        <location filename="../../src/windows/about-dialog.cpp" line="102"/>
        <source>Peony is a graphical software to help users manage system files. It provides common file operation functions for users, such as file viewing, file copy, paste, cut, delete, rename, file selection, application opening, file search, file sorting, file preview, etc. it is convenient for users to manage system files intuitively on the interface.</source>
        <translation>ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ᠎ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠮᠠᠰᠢᠨ ᠪᠣᠯ ᠨᠢᠭᠡ ᠵᠦᠢᠯ᠎ᠦ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ᠎ᠢ᠋ ᠬᠠᠪᠰᠤᠷᠤᠨ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ᠎ᠢ᠋ ᠳᠦᠷᠰᠦ ᠬᠡᠯᠪᠡᠷᠢᠵᠢᠭᠦᠯᠬᠦ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ ᠪᠣᠯᠤᠨ᠎ᠠ ᠂ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ᠎ᠲᠦ᠍ ᠪᠠᠢᠩᠭᠤ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ᠎ᠤ᠋ᠨ ᠠᠵᠢᠯᠯᠠᠬᠤ ᠴᠢᠳᠠᠮᠵᠢ᠎ᠶ᠋ᠢ ᠬᠠᠩᠭᠠᠨ᠎ᠠ ᠂ ᠵᠢᠱ᠌ᠢᠶᠡᠯᠡᠪᠡᠯ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ᠎ᠢ᠋ ᠪᠠᠢᠴᠠᠭᠠᠨ ᠦᠵᠡᠬᠦ ᠂ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠂ ᠨᠠᠭᠠᠬᠤ ᠂ ᠡᠰᠬᠡᠬᠦ ᠂ ᠬᠠᠰᠤᠬᠤ ᠂ ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠯᠬᠡᠬᠦ ᠂ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ᠎ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠠᠷᠭ᠎ᠠ ᠮᠠᠶᠢᠭ᠎ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ ᠂ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ᠎ᠢ᠋ ᠬᠠᠢᠬᠤ ᠂ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ᠎ᠢ᠋ ᠤᠷᠢᠳᠴᠢᠯᠠᠨ ᠦᠵᠡᠬᠦ ᠵᠡᠷᠭᠡ ᠂ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ᠎ᠦ᠋ᠨ ᠵᠠᠭᠠᠭ ᠨᠢᠭᠤᠷ ᠳᠡᠭᠡᠷ᠎ᠡ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ᠎ᠢ᠋ ᠰᠢᠭᠤᠳ ᠠᠵᠢᠭᠯᠠᠬᠤ᠎ᠳ᠋ᠤ᠌ ᠳᠥᠭᠥᠮ ᠦᠵᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Hot Service: </source>
        <translation type="vanished">服务热线: </translation>
    </message>
    <message>
        <source>File Manager</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="42"/>
        <location filename="../../src/windows/about-dialog.cpp" line="90"/>
        <source>Version number: %1</source>
        <translation>ᠬᠡᠪᠯᠡᠯ᠎ᠦ᠋ᠨ ᠨᠣᠮᠧᠷ ᠄ %1</translation>
    </message>
    <message>
        <source>File manager is a graphical software to help users manage system files. It provides common file operation functions for users, such as file viewing, file copy, paste, cut, delete, rename, file selection, application opening, file search, file sorting, file preview, etc. it is convenient for users to manage system files intuitively on the interface.</source>
        <translation type="vanished">文件管理器是一款帮助用户管理系统文件的图形化的软件，为用户提供常用的文件操作功能，比如文件查看，文件复制、粘贴、剪切、删除、重命名，文件打开方式选择，文件搜索，文件排序，文件预览等，方便用户在界面上直观地管理系统文件。</translation>
    </message>
    <message>
        <source>none</source>
        <translation type="vanished">无</translation>
    </message>
</context>
<context>
    <name>FileLabelBox</name>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="68"/>
        <source>Rename</source>
        <translation>ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="73"/>
        <source>Edit Color</source>
        <translation>ᠥᠩᠭᠡ ᠨᠠᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="84"/>
        <source>Delete</source>
        <translation>ᠲᠡᠮᠳᠡᠭ᠎ᠢ᠋ ᠤᠰᠠᠳᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="89"/>
        <source>Create New Label</source>
        <translation>ᠲᠡᠮᠳᠡᠭ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>HeaderBar</name>
    <message>
        <source>Create Folder</source>
        <translation type="vanished">新建文件夹</translation>
    </message>
    <message>
        <source>Open Terminal</source>
        <translation type="vanished">打开终端</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="139"/>
        <source>Go Back</source>
        <translation>ᠤᠬᠤᠷᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="146"/>
        <source>Go Forward</source>
        <translation>ᠤᠷᠤᠭᠰᠢᠯᠠᠶ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="205"/>
        <source>Search</source>
        <translation>ᠨᠡᠩᠵᠢᠬᠦ ᠂ ᠨᠡᠩᠵᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="370"/>
        <source>View Type</source>
        <translation>ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ᠎ᠤ᠋ᠨ ᠲᠥᠷᠥᠯ</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="395"/>
        <source>Sort Type</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠬᠤ ᠲᠥᠷᠥᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="429"/>
        <source>Option</source>
        <translation>ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="270"/>
        <source>Operate Tips</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="271"/>
        <source>Don&apos;t find any terminal, please install at least one terminal!</source>
        <translation>ᠶᠠᠮᠠᠷᠴᠤ ᠦᠵᠦᠭᠦᠷ᠎ᠦ᠋ᠨ ᠵᠠᠯᠭᠠᠰᠤ᠎ᠶ᠋ᠢ ᠡᠷᠢᠵᠦ ᠣᠯᠤᠭᠰᠠᠨ ᠥᠬᠡᠢ ᠂ ᠲᠠ ᠠᠳᠠᠭ᠎ᠲᠠᠭᠠᠨ ᠨᠢᠭᠡ᠎ᠶ᠋ᠢ ᠰᠠᠭᠤᠯᠭᠠᠭᠰᠠᠨ᠎ᠢ᠋ᠶ᠋ᠠᠨ ᠪᠠᠲᠤᠯᠠᠭᠠᠷᠠᠢ !︕</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="596"/>
        <source>Restore</source>
        <translation>ᠠᠩᠭᠢᠵᠢᠷᠠᠭᠤᠯᠤᠯ</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="599"/>
        <source>Maximize</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤ᠋ᠨ ᠶᠡᠭᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">最小化</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
</context>
<context>
    <name>HeaderBarContainer</name>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="756"/>
        <source>Minimize</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤ᠋ᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <source>Maximize/Restore</source>
        <translation type="vanished">最大化/还原</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation type="vanished">还原</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="vanished">最大化</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="777"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ ᠂ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="835"/>
        <source>File Manager</source>
        <translation>ᠹᠧᠢᠯ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="362"/>
        <source>Undo</source>
        <translation>ᠬᠦᠴᠦᠨ ᠬᠡᠢ ᠪᠣᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="369"/>
        <source>Redo</source>
        <translation>ᠳᠠᠬᠢᠨ ᠬᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="744"/>
        <source>warn</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢ ᠥᠭᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="744"/>
        <source>This operation is not supported.</source>
        <translation>ᠲᠤᠰ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ᠎ᠶ᠋ᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠥᠬᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="833"/>
        <source>Search</source>
        <translation>ᠨᠡᠩᠵᠢᠬᠦ ᠂ ᠨᠡᠩᠵᠢᠬᠦ</translation>
    </message>
    <message>
        <source>Tips info</source>
        <translation type="vanished">温馨提示</translation>
    </message>
    <message>
        <source>Trash has no file need to be cleaned.</source>
        <translation type="vanished">回收站没有文件需要被清空！</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <source>Delete Permanently</source>
        <translation type="vanished">永久删除</translation>
    </message>
    <message>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="vanished">您确定要删除这些文件吗？一旦开始删除，这些文件将不可再恢复。</translation>
    </message>
    <message>
        <source>Peony Qt</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="848"/>
        <source>New Folder</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠴᠣᠮᠣᠭ</translation>
    </message>
</context>
<context>
    <name>NavigationSideBar</name>
    <message>
        <source>All tags...</source>
        <translation type="vanished">所有标记...</translation>
    </message>
    <message>
        <source>Open In &amp;New Window</source>
        <translation type="vanished">在新窗口中打开(&amp;N)</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="470"/>
        <source>warn</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢ ᠥᠭᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="470"/>
        <source>This operation is not supported.</source>
        <translation>ᠲᠤᠰ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ᠎ᠶ᠋ᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠥᠬᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="226"/>
        <location filename="../../src/control/navigation-side-bar.cpp" line="508"/>
        <location filename="../../src/control/navigation-side-bar.cpp" line="526"/>
        <source>Tips</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="226"/>
        <source>The device is in busy state, please perform this operation later.</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠠᠭᠠᠷᠠᠤ ᠪᠠᠢᠳᠠᠯ᠎ᠳ᠋ᠤ᠌ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠲᠤᠰ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ᠎ᠶ᠋ᠢ ᠬᠡᠷᠡᠭᠵᠢᠭᠦᠯᠦᠭᠡᠷᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="508"/>
        <source>This is an abnormal Udisk, please fix it or format it</source>
        <translation>ᠡᠨᠡ ᠪᠣᠯ ᠨᠢᠭᠡᠨ ᠬᠡᠪ᠎ᠦ᠋ᠨ ᠪᠤᠰᠤ U ᠳ᠋ᠢᠰᠺ ᠂ ᠲᠡᠭᠦᠨ᠎ᠢ᠋ ᠵᠠᠰᠠᠬᠤ ᠪᠤᠶᠤ ᠹᠤᠷᠮᠠᠲ᠋ᠯᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="526"/>
        <source>This is an empty drive, please insert a Disc.</source>
        <translation>ᠡᠨᠡ ᠪᠣᠯ ᠨᠢᠭᠡᠨ ᠬᠣᠭᠣᠰᠣᠨ ᠭᠡᠷᠡᠯᠢᠭ ᠬᠥᠳᠡᠯᠭᠡᠭᠦᠷ ᠂ ᠭᠡᠷᠡᠯᠢᠭ ᠳ᠋ᠢᠰᠺ᠎ᠢ᠋ ᠬᠠᠪᠴᠢᠭᠤᠯ</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="254"/>
        <source>Open In New Window</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠴᠣᠩᠬᠣᠨ᠎ᠳ᠋ᠤ᠌ ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="271"/>
        <location filename="../../src/control/navigation-side-bar.cpp" line="307"/>
        <source>Can not open %1, %2</source>
        <translation>%1᠎ᠶ᠋ᠢ ᠨᠡᠭᠡᠭᠡᠬᠦ᠎ᠶ᠋ᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ %2</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="290"/>
        <source>Open In New Tab</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠱᠣᠰᠢᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠨᠢᠭᠤᠷ ᠳᠤᠮᠳᠠ ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Open In New &amp;Tab</source>
        <translation type="vanished">在新标签页中打开(&amp;T)</translation>
    </message>
</context>
<context>
    <name>NavigationSideBarContainer</name>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="686"/>
        <source>All tags...</source>
        <translation>ᠪᠦᠬᠦ ᠲᠡᠮᠳᠡᠭ ᠁</translation>
    </message>
</context>
<context>
    <name>NavigationTabBar</name>
    <message>
        <source>Computer</source>
        <translation type="vanished">计算机</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-tab-bar.cpp" line="127"/>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation>%2᠎ᠠ᠋ᠴᠠ %1᠎ᠶ᠋ᠢ ᠰᠢᠭᠦᠬᠦ</translation>
    </message>
</context>
<context>
    <name>OperationMenu</name>
    <message>
        <source>Advance Search</source>
        <translation type="vanished">高级搜索</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="77"/>
        <source>Keep Allow</source>
        <translation>ᠣᠷᠣᠢ᠎ᠶ᠋ᠢᠨ ᠴᠣᠩᠬᠣ</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="86"/>
        <source>Show Hidden</source>
        <translation>ᠨᠢᠭᠤᠭᠳᠠᠮᠠᠯ ᠹᠠᠢᠯ᠎ᠢ᠋ ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="94"/>
        <source>Show File Extension</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠰᠢᠨᠵᠢ ᠨᠡᠷ᠎ᠡ ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="99"/>
        <source>Show Create Time</source>
        <translation>ᠪᠠᠢᠭᠤᠯᠬᠤ ᠴᠠᠭ᠎ᠢ᠋ ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="105"/>
        <source>Forbid thumbnailing</source>
        <translation>ᠪᠠᠭᠠᠰᠬᠠᠭᠰᠠᠨ ᠵᠢᠷᠤᠭ᠎ᠢ᠋ ᠴᠠᠭᠠᠵᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="114"/>
        <source>Resident in Backend</source>
        <translation>ᠪᠠᠢᠩᠭᠤ ᠬᠦᠯᠢᠶᠡᠬᠦ ᠲᠠᠢᠰᠠ</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="123"/>
        <source>Parallel Operations</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠬᠤᠢ᠎ᠶ᠋ᠢ ᠵᠡᠷᠭᠡᠪᠡᠷ ᠶᠠᠪᠤᠭᠤᠯᠬᠤ᠎ᠶ᠋ᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="129"/>
        <source>Set samba password</source>
        <translation>sambaᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="143"/>
        <source>Tips</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="143"/>
        <source>The user already has a samba password, do you need to reset the samba password?</source>
        <translation>ᠲᠤᠰ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠳ᠋ᠦ᠍ samba ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠨᠢᠭᠡᠨᠲᠡ ᠪᠣᠢ ᠂ samba ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠳᠠᠬᠢᠨ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠤᠤ ?</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="154"/>
        <source>Samba set user password</source>
        <translation>Samba ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="152"/>
        <source>Samba password:</source>
        <translation>Sambaᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ：</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="162"/>
        <location filename="../../src/control/operation-menu.cpp" line="173"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢ ᠥᠭᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="162"/>
        <source>Samba set password failed, Please re-enter!</source>
        <translation>Samba ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠨᠢ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠳᠠᠬᠢᠨ ᠣᠷᠣᠭᠤᠯᠤᠭᠠᠷᠠᠢ !</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="173"/>
        <source>Shared configuration service exception, please confirm if there is an ongoing shared configuration operation, or please reset the share!</source>
        <translation>ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠨᠢ ᠬᠡᠪ᠎ᠦ᠋ᠨ ᠪᠤᠰᠤ ᠂ ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ ᠶᠠᠪᠤᠭᠳᠠᠵᠤ ᠪᠠᠢᠬᠤ ᠡᠰᠡᠬᠦ᠎ᠶ᠋ᠢ ᠨᠤᠲᠠᠯᠠᠭᠠᠷᠠᠢ ᠂ ᠡᠰᠡᠬᠦᠯ᠎ᠡ ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ᠎ᠶ᠋ᠢ ᠳᠠᠬᠢᠨ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠭᠠᠷᠠᠢ !︕</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="182"/>
        <source>Open each folder in a new window</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠴᠣᠩᠬᠣᠨ᠎ᠳ᠋ᠤ᠌ ᠹᠠᠢᠯ ᠬᠠᠪᠴᠢᠭᠤᠷ᠎ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="188"/>
        <source>Plugin manager Settings</source>
        <translation>ᠤᠭᠯᠠᠭᠤᠷᠭ᠎ᠠ ᠲᠣᠨᠣᠭ᠎ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯ</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="197"/>
        <source>Help</source>
        <translation>ᠬᠠᠪᠰᠤᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="201"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
</context>
<context>
    <name>OperationMenuEditWidget</name>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="248"/>
        <source>Edit</source>
        <translation>ᠨᠠᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="259"/>
        <source>copy</source>
        <translation>ᠺᠣᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="268"/>
        <source>paste</source>
        <translation>ᠨᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="277"/>
        <source>cut</source>
        <translation>ᠬᠠᠢᠴᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="286"/>
        <source>trash</source>
        <translation>ᠬᠠᠰᠤᠬᠤ᠎ᠶ᠋ᠢ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>PeonyApplication</name>
    <message>
        <source>Peony-Qt</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="162"/>
        <source>peony-qt</source>
        <translation>ᠹᠧᠢᠯ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="169"/>
        <source>Files or directories to open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠬᠦ ᠴᠢᠬᠤᠯᠠᠲᠠᠢ ᠹᠠᠢᠯ ᠪᠤᠶᠤ ᠴᠣᠮᠣᠭ</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="169"/>
        <source>[FILE1, FILE2,...]</source>
        <translation>[ ᠹᠠᠢᠯ 1. ᠹᠠᠢᠯ 2. . ]</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="209"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢ ᠥᠭᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="209"/>
        <source>Peony-Qt can not get the system&apos;s icon theme. There are 2 reasons might lead to this problem:

1. Peony-Qt might be running as root, that means you have the higher permission and can do some things which normally forbidden. But, you should learn that if you were in a root, the virtual file system will lose some featrue such as you can not use &quot;My Computer&quot;, the theme and icons might also went wrong. So, run peony-qt in a root is not recommended.

2. You are using a non-qt theme for your system but you didn&apos;t install the platform theme plugin for qt&apos;s applications. If you are using gtk-theme, try installing the qt5-gtk2-platformtheme package to resolve this problem.</source>
        <translation>ᠹᠧᠢᠯ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠤᠷ ᠨᠢ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠢᠺᠤᠨ ᠵᠢᠷᠤᠭ᠎ᠤ᠋ᠨ ᠭᠣᠣᠯ ᠰᠡᠳᠦᠪ᠎ᠢ᠋ ᠣᠯᠬᠤ᠎ᠶ᠋ᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ ᠂ ᠪᠣᠯᠤᠯᠴᠠᠭ᠎ᠠ᠎ᠲᠠᠢ ᠰᠢᠯᠲᠠᠭᠠᠨ ᠨᠢ ᠄
 1. ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠪᠡᠷ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠬᠠᠮᠢᠶᠠᠷᠬᠤ ᠮᠠᠰᠢᠨ᠎ᠢ᠋ ᠠᠵᠢᠯᠯᠠᠭᠤᠯᠵᠤ ᠪᠣᠢ ᠂ ᠬᠡᠳᠦᠢᠪᠡᠷ ᠡᠨᠡ ᠨᠢ ᠴᠢ ᠨᠡᠩ ᠳᠡᠭᠡᠳᠦ ᠡᠷᠬᠡ᠎ᠲᠡᠢ ᠭᠡᠳᠡᠭ᠎ᠢ᠋ ᠬᠠᠷᠠᠭᠤᠯᠵᠤ ᠪᠠᠢᠬᠤ ᠪᠣᠯᠪᠠᠴᠤ ᠂ ᠡᠩ᠎ᠦ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠳᠣᠣᠷ᠎ᠠ ᠰᠠᠶᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠵᠠᠷᠢᠮ ᠣᠨᠴᠠᠯᠢᠭ᠎ᠢ᠋ ᠵᠠᠪᠠᠯ ᠣᠢᠯᠭᠠᠪᠠᠯ ᠵᠣᠬᠢᠨ᠎ᠠ ᠂ ᠵᠢᠱ᠌ᠢᠶᠡᠯᠡᠪᠡᠯ ︽ ᠮᠢᠨᠦ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ︾ ᠵᠢᠴᠢ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠭᠣᠣᠯ ᠰᠡᠳᠦᠪ ᠃ ᠬᠡᠷᠪᠡ ᠣᠨᠴᠠᠭᠠᠢ ᠪᠠᠢᠳᠠᠯ ᠦᠭᠡᠢ ᠪᠣᠯ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠤᠷ᠎ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ ᠃
 2. ᠲᠠᠨ᠎ᠤ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠠᠢᠭ᠎ᠠ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠭᠣᠣᠯ ᠰᠡᠳᠦᠪ ᠴᠢᠨᠢ qtᠠᠶᠠᠳᠠᠯ ᠳᠡᠮᠵᠢᠬᠦ ᠭᠣᠣᠯ ᠰᠡᠳᠦᠪ ᠪᠢᠰᠢ ᠪᠥᠭᠡᠳ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠲᠠᠪᠴᠠᠩ᠎ᠤ᠋ᠨ ᠵᠠᠯᠭᠠᠰᠤ᠎ᠶ᠋ᠢ ᠰᠠᠭᠤᠯᠭᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ ᠃ ᠬᠡᠷᠪᠡ ᠲᠠ ᠶᠠᠭ ᠰᠠᠢᠬᠠᠨ Gtkᠭᠣᠣᠯ ᠰᠡᠳᠦᠪ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠨ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠭᠣᠣᠯ ᠰᠡᠳᠦᠪ ᠪᠣᠯᠭᠠᠵᠤ ᠂ qt5 —gtk2 —platformtheme ᠢ ᠰᠠᠭᠤᠯᠠᠭᠠᠨ ᠲᠤᠰ ᠠᠰᠠᠭᠤᠳᠠᠯ᠎ᠢ᠋ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠵᠦ ᠦᠵᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="470"/>
        <source>Peony Qt</source>
        <translation>ᠹᠧᠢᠯ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="471"/>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2020, KylinSoft Co., Ltd.</source>
        <translation>ᠵᠣᠬᠢᠶᠠᠭᠴᠢ ᠄
 Yue Lan lanyue@kylinos . cn
 Meihong He 《 hemeihong @kylinos . cn 》
 ᠬᠡᠪᠯᠡᠯ᠎ᠦ᠋ᠨ ᠡᠷᠬᠡ᠎ᠶ᠋ᠢᠨ ᠥᠮᠴᠢᠯᠡᠯᠲᠡ ︵ C ︶ ᠄ 20202 ᠪᠢᠯᠢᠭᠲᠦ ᠭᠥᠷᠦᠭᠡᠰᠦᠨ᠎ᠦ᠌ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠤ᠋ᠨ ᠬᠢᠵᠠᠭᠠᠷᠲᠤ ᠺᠣᠮᠫᠠᠨᠢ .</translation>
    </message>
    <message>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,麒麟软件有限公司.</translation>
    </message>
    <message>
        <source>Author: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,麒麟软件有限公司.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,麒麟软件有限公司.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="67"/>
        <source>Close all peony-qt windows and quit</source>
        <translation>ᠪᠦᠬᠦ ᠴᠣᠩᠬᠣ᠎ᠶ᠋ᠢ ᠬᠠᠭᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="68"/>
        <source>Show items</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠪᠠᠢᠬᠤ ᠭᠠᠷᠴᠠᠭ᠎ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠮᠥᠷᠲᠡᠭᠡᠨ ᠲᠡᠳᠡᠭᠡᠷ᠎ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="69"/>
        <source>Show folders</source>
        <translation>ᠴᠣᠮᠣᠭ᠎ᠤ᠋ᠨ ᠠᠭᠤᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠬᠠᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="70"/>
        <source>Show properties</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠰᠢᠨᠵᠢ ᠴᠢᠨᠠᠷ᠎ᠤ᠋ᠨ ᠴᠣᠩᠬᠣ᠎ᠶ᠋ᠢ ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>Can not open %1.</source>
        <translation type="vanished">无法打开 %1.</translation>
    </message>
</context>
<context>
    <name>SortTypeMenu</name>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="34"/>
        <source>File Name</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="46"/>
        <source>File Size</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="50"/>
        <source>Original Path</source>
        <translation>ᠠᠩᠬᠠᠨ᠎ᠤ᠋ ᠵᠠᠮ</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="82"/>
        <source>Use global sorting</source>
        <translation>ᠪᠦᠬᠦ ᠲᠠᠯ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="42"/>
        <source>File Type</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠲᠥᠷᠥᠯ</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="38"/>
        <source>Modified Date</source>
        <translation>ᠵᠠᠰᠠᠭᠰᠠᠨ ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>Modified Data</source>
        <translation type="vanished">修改日期</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="71"/>
        <source>Ascending</source>
        <translation>ᠥᠭᠰᠥᠬᠦ ᠶᠠᠪᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="66"/>
        <source>Descending</source>
        <translation>ᠪᠠᠭᠤᠷᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>TabStatusBar</name>
    <message>
        <source>Current path has:</source>
        <translation type="vanished">当前路径包含：</translation>
    </message>
    <message>
        <source>%1 folders, %2 files</source>
        <translation type="vanished">%1 文件夹，%2 文件</translation>
    </message>
    <message>
        <source>%1 folders</source>
        <translation type="vanished">%1 文件夹</translation>
    </message>
    <message>
        <source>%1 files</source>
        <translation type="vanished">%1 文件</translation>
    </message>
    <message>
        <source>; %1 folders</source>
        <translation type="vanished">; %1 个文件夹</translation>
    </message>
    <message>
        <source>; %1 files, %2 total</source>
        <translation type="vanished">; %1 个文件, 共%2</translation>
    </message>
    <message>
        <source>; %1 folder</source>
        <translation type="vanished">; %1 个文件夹</translation>
    </message>
    <message>
        <source>; %1 file, %2</source>
        <translation type="vanished">; %1 个文件, %2</translation>
    </message>
    <message>
        <source>%1 selected</source>
        <translation type="vanished">选中%1个</translation>
    </message>
    <message>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation type="vanished">在%2中搜索%1</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="109"/>
        <location filename="../../src/control/tab-status-bar.cpp" line="218"/>
        <source>Searching for files ...</source>
        <translation>ᠶᠠᠭ ᠰᠠᠢᠬᠠᠨ ᠰᠢᠭᠦᠵᠦ ᠪᠣᠢ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="122"/>
        <source> %1 items </source>
        <translation> %1 ᠫᠷᠤᠵᠧᠺᠲ </translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="159"/>
        <source> selected %1 items    %2</source>
        <translation> %1 ᠫᠷᠤᠵᠧᠺᠲ %2᠎ᠢ᠋ ᠰᠣᠩᠭᠣᠨ ᠠ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="161"/>
        <source> selected %1 items</source>
        <translation> %1 ᠫᠷᠤᠵᠧᠺᠲ᠎ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
</context>
<context>
    <name>TabWidget</name>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="302"/>
        <source>Trash</source>
        <translation>ᠪᠤᠴᠠᠭᠠᠨ ᠬᠤᠷᠢᠶᠠᠬᠤ ᠥᠷᠲᠡᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="306"/>
        <source>Clear</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="311"/>
        <source>Recover</source>
        <translation>ᠠᠩᠭᠢᠵᠢᠷᠠᠭᠤᠯᠤᠯ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="471"/>
        <source>Close Filter.</source>
        <translation>ᠰᠢᠯᠢᠯᠲᠡ᠎ᠶ᠋ᠢ ᠬᠠᠭᠠᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="483"/>
        <source>Filter</source>
        <translation>ᠰᠢᠭᠰᠢᠨ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="580"/>
        <source>Select Path</source>
        <translation>ᠵᠠᠮ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1109"/>
        <source>Current path: %1, %2</source>
        <translation>ᠣᠳᠣᠬᠢ ᠵᠠᠮ ᠄ %1 %2 n</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1109"/>
        <source>click to select other search path.</source>
        <translation>ᠪᠤᠰᠤᠳ ᠰᠢᠭᠦᠬᠦ ᠵᠠᠮ᠎ᠢ᠋ ᠲᠣᠪᠴᠢᠳᠠᠨ ᠰᠢᠭᠦᠬᠦ ᠵᠠᠮ᠎ᠢ᠋ ᠰᠣᠩᠭᠣᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1696"/>
        <source>Opening such files is not currently supported</source>
        <translation>ᠡᠢᠮᠦ ᠹᠠᠢᠯ᠎ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠬᠦ᠎ᠶ᠋ᠢ ᠲᠦᠷ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1709"/>
        <source>Open failed</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠭᠰᠡᠨ ᠨᠢ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1710"/>
        <source>Open directory failed, you have no permission!</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠬᠠᠪᠴᠢᠭᠤᠷ᠎ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠬᠦ᠎ᠳ᠋ᠦ᠍ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠲᠤᠰ ᠭᠠᠷᠴᠠᠭ᠎ᠤ᠋ᠨ ᠡᠷᠬᠡ ᠥᠬᠡᠢ !︕</translation>
    </message>
    <message>
        <source>Close advance search.</source>
        <translation type="vanished">关闭高级搜索。</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="494"/>
        <source>Choose other path to search.</source>
        <translation>ᠪᠤᠰᠤᠳ ᠰᠢᠭᠦᠬᠦ ᠵᠠᠮ᠎ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="503"/>
        <source>Search recursively</source>
        <translation>ᠡᠭᠡᠭᠦᠯᠬᠦ ᠨᠡᠩᠵᠢᠯᠲᠡ</translation>
    </message>
    <message>
        <source>more options</source>
        <translation type="vanished">更多选项</translation>
    </message>
    <message>
        <source>Show/hide advance search</source>
        <translation type="vanished">显示/隐藏高级搜索</translation>
    </message>
    <message>
        <source>Select path</source>
        <translation type="vanished">选择路径</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="640"/>
        <location filename="../../src/control/tab-widget.cpp" line="795"/>
        <source>is</source>
        <translation>ᠲᠡᠢᠮᠦ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="675"/>
        <source>Please input key words...</source>
        <translation>ᠵᠠᠩᠭᠢᠯᠠᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠦᠭᠡ᠎ᠶ᠋ᠢ ᠣᠷᠣᠭᠤᠯᠤᠭᠠᠷᠠᠢ ᠁</translation>
    </message>
    <message>
        <source>Please input kay words...</source>
        <translation type="vanished">请输入关键词...</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="756"/>
        <location filename="../../src/control/tab-widget.cpp" line="779"/>
        <source>contains</source>
        <translation>ᠪᠠᠭᠲᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="280"/>
        <source>name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="280"/>
        <source>type</source>
        <translation>ᠲᠥᠷᠥᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="280"/>
        <source>modify time</source>
        <translation>ᠵᠠᠰᠠᠭᠰᠠᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="280"/>
        <source>file size</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="281"/>
        <location filename="../../src/control/tab-widget.h" line="283"/>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>all</source>
        <translation>ᠪᠦᠬᠦᠨ᠎ᠢ᠋ ᠪᠦᠭᠦᠳᠡ᠎ᠶ᠋ᠢ ᠠᠪ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="281"/>
        <source>file folder</source>
        <translation>ᠴᠣᠮᠣᠭ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="281"/>
        <source>image</source>
        <translation>ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="281"/>
        <source>video</source>
        <translation>ᠸᠢᠳᠢᠦ᠋ ᠂ ᠸᠢᠳᠢᠣ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="282"/>
        <source>text file</source>
        <translation>ᠲᠧᠺᠰᠲ ᠳ᠋ᠤᠺᠦ᠋ᠮᠧᠨ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="282"/>
        <source>audio</source>
        <translation>ᠳᠠᠭᠤᠨ ᠳᠠᠪᠲᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="282"/>
        <source>others</source>
        <translation>ᠪᠤᠰᠤᠳ ᠪᠤᠰᠤᠳ ᠪᠤᠰᠤᠳ ᠪᠤᠰᠤᠳ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="282"/>
        <source>wps file</source>
        <translation>WPS ᠹᠧᠢᠯ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="283"/>
        <source>today</source>
        <translation>ᠥᠨᠥᠳᠥᠷ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="283"/>
        <source>this week</source>
        <translation>ᠲᠤᠰ ᠭᠠᠷᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="283"/>
        <source>this month</source>
        <translation>ᠲᠤᠰ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="283"/>
        <source>this year</source>
        <translation>ᠡᠨᠡ ᠵᠢᠯ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="283"/>
        <source>year ago</source>
        <translation>ᠨᠢᠭᠡ ᠵᠢᠯ᠎ᠦ᠋ᠨ ᠡᠮᠦᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>tiny(0-16K)</source>
        <translation>ᠲᠤᠢᠯ᠎ᠤ᠋ᠨ ᠪᠠᠭ᠎ᠠ (᠎0 —16K )</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>small(16k-1M)</source>
        <translation>ᠲᠣᠩ ᠪᠠᠭ᠎ᠠ (᠎16k—1M )</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>empty(0K)</source>
        <translation>ᠬᠣᠭᠣᠰᠣᠨ (᠎0K )</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>medium(1M-128M)</source>
        <translation>ᠳᠤᠮᠳᠠ ᠵᠡᠷᠭᠡ (᠎1 M—128M )</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>big(128M-1G)</source>
        <translation>ᠶᠡᠬᠡ (᠎128M—1G )</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>large(1-4G)</source>
        <translation>ᠯᠤᠲᠤ ᠶᠡᠬᠡ (᠎1—4G )</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>great(&gt;4G)</source>
        <translation>ᠲᠤᠢᠯ᠎ᠤ᠋ᠨ ᠶᠡᠬᠡ (᠎&gt; 4G )</translation>
    </message>
    <message>
        <source>medium(1M-100M)</source>
        <translation type="vanished">中等(1M-100M)</translation>
    </message>
    <message>
        <source>big(100M-1G)</source>
        <translation type="vanished">很大(100M-1G)</translation>
    </message>
    <message>
        <source>large(&gt;1G)</source>
        <translation type="vanished">极大(&gt;1G)</translation>
    </message>
</context>
</TS>
