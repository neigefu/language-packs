<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="32"/>
        <source>Dialog</source>
        <translation>視窗</translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="88"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="115"/>
        <source>TextLabel</source>
        <translation>標籤</translation>
    </message>
    <message>
        <source>Offical Website: </source>
        <translation type="vanished">官方网站: </translation>
    </message>
    <message>
        <source>Service &amp; Technology Support: </source>
        <translation type="vanished">服务与技术支持: </translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="39"/>
        <location filename="../../src/windows/about-dialog.cpp" line="141"/>
        <source>Service &amp; Support: </source>
        <translation>服務與支持團隊： </translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="38"/>
        <location filename="../../src/windows/about-dialog.cpp" line="71"/>
        <location filename="../../src/windows/about-dialog.cpp" line="87"/>
        <source>Peony</source>
        <translation>檔管理員</translation>
    </message>
    <message>
        <source>peony</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="43"/>
        <location filename="../../src/windows/about-dialog.cpp" line="102"/>
        <source>Peony is a graphical software to help users manage system files. It provides common file operation functions for users, such as file viewing, file copy, paste, cut, delete, rename, file selection, application opening, file search, file sorting, file preview, etc. it is convenient for users to manage system files intuitively on the interface.</source>
        <translation>檔管理員是一款説明使用者管理系統文件的圖形化的軟體，為使用者提供常用的檔操作功能，比如檔查看，檔複製、粘貼、剪切、刪除、重命名，檔打開方式選擇，檔搜索，檔排序，檔預覽等，方便使用者在介面上直觀地管理系統檔。</translation>
    </message>
    <message>
        <source>Hot Service: </source>
        <translation type="vanished">服务热线: </translation>
    </message>
    <message>
        <source>File Manager</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="42"/>
        <location filename="../../src/windows/about-dialog.cpp" line="90"/>
        <source>Version number: %1</source>
        <translation>版本號： %1</translation>
    </message>
    <message>
        <source>File manager is a graphical software to help users manage system files. It provides common file operation functions for users, such as file viewing, file copy, paste, cut, delete, rename, file selection, application opening, file search, file sorting, file preview, etc. it is convenient for users to manage system files intuitively on the interface.</source>
        <translation type="vanished">文件管理器是一款帮助用户管理系统文件的图形化的软件，为用户提供常用的文件操作功能，比如文件查看，文件复制、粘贴、剪切、删除、重命名，文件打开方式选择，文件搜索，文件排序，文件预览等，方便用户在界面上直观地管理系统文件。</translation>
    </message>
    <message>
        <source>none</source>
        <translation type="vanished">无</translation>
    </message>
</context>
<context>
    <name>FileLabelBox</name>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="68"/>
        <source>Rename</source>
        <translation>重新命名</translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="73"/>
        <source>Edit Color</source>
        <translation>編輯顏色</translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="84"/>
        <source>Delete</source>
        <translation>刪除標記</translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="89"/>
        <source>Create New Label</source>
        <translation>創建標記</translation>
    </message>
</context>
<context>
    <name>HeaderBar</name>
    <message>
        <source>Create Folder</source>
        <translation type="vanished">新建文件夹</translation>
    </message>
    <message>
        <source>Open Terminal</source>
        <translation type="vanished">打开终端</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="139"/>
        <source>Go Back</source>
        <translation>後退</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="146"/>
        <source>Go Forward</source>
        <translation>前進</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="205"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="370"/>
        <source>View Type</source>
        <translation>視圖類型</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="395"/>
        <source>Sort Type</source>
        <translation>排序類型</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="429"/>
        <source>Option</source>
        <translation>選項</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="270"/>
        <source>Operate Tips</source>
        <translation>操作提示</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="271"/>
        <source>Don&apos;t find any terminal, please install at least one terminal!</source>
        <translation>沒有找到任何終端外掛程式，請確認您至少安裝了一個！</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="596"/>
        <source>Restore</source>
        <translation>還原</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="599"/>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">最小化</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
</context>
<context>
    <name>HeaderBarContainer</name>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="756"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <source>Maximize/Restore</source>
        <translation type="vanished">最大化/还原</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation type="vanished">还原</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="vanished">最大化</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="777"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="835"/>
        <source>File Manager</source>
        <translation>檔管理員</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="362"/>
        <source>Undo</source>
        <translation>撤銷</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="369"/>
        <source>Redo</source>
        <translation>重做</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="744"/>
        <source>warn</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="744"/>
        <source>This operation is not supported.</source>
        <translation>不支援此操作。</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="833"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <source>Tips info</source>
        <translation type="vanished">温馨提示</translation>
    </message>
    <message>
        <source>Trash has no file need to be cleaned.</source>
        <translation type="vanished">回收站没有文件需要被清空！</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <source>Delete Permanently</source>
        <translation type="vanished">永久删除</translation>
    </message>
    <message>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="vanished">您确定要删除这些文件吗？一旦开始删除，这些文件将不可再恢复。</translation>
    </message>
    <message>
        <source>Peony Qt</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="848"/>
        <source>New Folder</source>
        <translation>新建資料夾</translation>
    </message>
</context>
<context>
    <name>NavigationSideBar</name>
    <message>
        <source>All tags...</source>
        <translation type="vanished">所有标记...</translation>
    </message>
    <message>
        <source>Open In &amp;New Window</source>
        <translation type="vanished">在新窗口中打开(&amp;N)</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="470"/>
        <source>warn</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="470"/>
        <source>This operation is not supported.</source>
        <translation>不支援此操作。</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="226"/>
        <location filename="../../src/control/navigation-side-bar.cpp" line="508"/>
        <location filename="../../src/control/navigation-side-bar.cpp" line="526"/>
        <source>Tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="226"/>
        <source>The device is in busy state, please perform this operation later.</source>
        <translation>設備處於忙碌狀態，請稍後執行此操作。</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="508"/>
        <source>This is an abnormal Udisk, please fix it or format it</source>
        <translation>這是個異常U盤，請將其修復或格式化</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="526"/>
        <source>This is an empty drive, please insert a Disc.</source>
        <translation>這是一個空光碟機， 請插入光碟.</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="254"/>
        <source>Open In New Window</source>
        <translation>在新視窗中打開</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="271"/>
        <location filename="../../src/control/navigation-side-bar.cpp" line="307"/>
        <source>Can not open %1, %2</source>
        <translation>無法開啟%1， %2</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="290"/>
        <source>Open In New Tab</source>
        <translation>在新標籤頁中打開</translation>
    </message>
    <message>
        <source>Open In New &amp;Tab</source>
        <translation type="vanished">在新标签页中打开(&amp;T)</translation>
    </message>
</context>
<context>
    <name>NavigationSideBarContainer</name>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="686"/>
        <source>All tags...</source>
        <translation>所有標籤...</translation>
    </message>
</context>
<context>
    <name>NavigationTabBar</name>
    <message>
        <source>Computer</source>
        <translation type="vanished">计算机</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-tab-bar.cpp" line="127"/>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation>在%2中搜尋%1</translation>
    </message>
</context>
<context>
    <name>OperationMenu</name>
    <message>
        <source>Advance Search</source>
        <translation type="vanished">高级搜索</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="77"/>
        <source>Keep Allow</source>
        <translation>置頂視窗</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="86"/>
        <source>Show Hidden</source>
        <translation>顯示隱藏檔案</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="94"/>
        <source>Show File Extension</source>
        <translation>顯示檔擴展名</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="99"/>
        <source>Show Create Time</source>
        <translation>顯示創建時間</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="105"/>
        <source>Forbid thumbnailing</source>
        <translation>禁用縮圖</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="114"/>
        <source>Resident in Backend</source>
        <translation>常駐後台</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="123"/>
        <source>Parallel Operations</source>
        <translation>允許操作並行</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="129"/>
        <source>Set samba password</source>
        <translation>設置samba密碼</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="143"/>
        <source>Tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="143"/>
        <source>The user already has a samba password, do you need to reset the samba password?</source>
        <translation>該用戶已經有samba密碼，是否需要重置samba密碼？</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="154"/>
        <source>Samba set user password</source>
        <translation>Samba設置用戶密碼</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="152"/>
        <source>Samba password:</source>
        <translation>Samba密碼：</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="162"/>
        <location filename="../../src/control/operation-menu.cpp" line="173"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="162"/>
        <source>Samba set password failed, Please re-enter!</source>
        <translation>Samba設置密碼失敗，請重新輸入！</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="173"/>
        <source>Shared configuration service exception, please confirm if there is an ongoing shared configuration operation, or please reset the share!</source>
        <translation>共用配置服務異常，請確認是否有正在進行的共用配置操作，或者請重新設置共用！</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="182"/>
        <source>Open each folder in a new window</source>
        <translation>在新視窗打開資料夾</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="188"/>
        <source>Plugin manager Settings</source>
        <translation>外掛程式管理設置</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="197"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="201"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
</context>
<context>
    <name>OperationMenuEditWidget</name>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="248"/>
        <source>Edit</source>
        <translation>編輯</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="259"/>
        <source>copy</source>
        <translation>複製</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="268"/>
        <source>paste</source>
        <translation>粘貼</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="277"/>
        <source>cut</source>
        <translation>剪切</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="286"/>
        <source>trash</source>
        <translation>刪除</translation>
    </message>
</context>
<context>
    <name>PeonyApplication</name>
    <message>
        <source>Peony-Qt</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="162"/>
        <source>peony-qt</source>
        <translation>檔管理員</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="169"/>
        <source>Files or directories to open</source>
        <translation>需要打開的檔或資料夾</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="169"/>
        <source>[FILE1, FILE2,...]</source>
        <translation>[檔1，檔2...]</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="209"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="209"/>
        <source>Peony-Qt can not get the system&apos;s icon theme. There are 2 reasons might lead to this problem:

1. Peony-Qt might be running as root, that means you have the higher permission and can do some things which normally forbidden. But, you should learn that if you were in a root, the virtual file system will lose some featrue such as you can not use &quot;My Computer&quot;, the theme and icons might also went wrong. So, run peony-qt in a root is not recommended.

2. You are using a non-qt theme for your system but you didn&apos;t install the platform theme plugin for qt&apos;s applications. If you are using gtk-theme, try installing the qt5-gtk2-platformtheme package to resolve this problem.</source>
        <translation>檔案管理員無法取得系統圖示主題，可能的原因是：

1.正在以管理員使用者運行檔管理器，雖然這意味著你擁有更高的許可權，但是你必須瞭解你同時也失去了一些在普通使用者下才能夠擁有的特徵，例如“我的電腦”以及系統主題。 如果不是特殊情況，請不要使用管理員使用者打開檔管理員。

2.你使用的系統主題不是qt默認支援的主題，並且你沒有安裝相關的平臺外掛程式。 如果你正在使用Gtk主題作為系統主題，嘗試安裝qt5-gtk2-platformtheme以解決此問題。</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="470"/>
        <source>Peony Qt</source>
        <translation>檔管理員</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="471"/>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2020, KylinSoft Co., Ltd.</source>
        <translation>作者：
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版權所有（C）： 2020， 麒麟軟體有限公司.</translation>
    </message>
    <message>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,麒麟软件有限公司.</translation>
    </message>
    <message>
        <source>Author: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,麒麟软件有限公司.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,麒麟软件有限公司.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="67"/>
        <source>Close all peony-qt windows and quit</source>
        <translation>關閉所有視窗並退出</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="68"/>
        <source>Show items</source>
        <translation>打開檔所在目錄並選取中它們</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="69"/>
        <source>Show folders</source>
        <translation>顯示資料夾下的內容</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="70"/>
        <source>Show properties</source>
        <translation>打開檔案屬性視窗</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>Can not open %1.</source>
        <translation type="vanished">无法打开 %1.</translation>
    </message>
</context>
<context>
    <name>SortTypeMenu</name>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="34"/>
        <source>File Name</source>
        <translation>檔名稱</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="46"/>
        <source>File Size</source>
        <translation>檔大小</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="50"/>
        <source>Original Path</source>
        <translation>原始路徑</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="82"/>
        <source>Use global sorting</source>
        <translation>使用全域排序</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="42"/>
        <source>File Type</source>
        <translation>檔案類型</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="38"/>
        <source>Modified Date</source>
        <translation>修改日期</translation>
    </message>
    <message>
        <source>Modified Data</source>
        <translation type="vanished">修改日期</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="71"/>
        <source>Ascending</source>
        <translation>升序</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="66"/>
        <source>Descending</source>
        <translation>降序</translation>
    </message>
</context>
<context>
    <name>TabStatusBar</name>
    <message>
        <source>Current path has:</source>
        <translation type="vanished">当前路径包含：</translation>
    </message>
    <message>
        <source>%1 folders, %2 files</source>
        <translation type="vanished">%1 文件夹，%2 文件</translation>
    </message>
    <message>
        <source>%1 folders</source>
        <translation type="vanished">%1 文件夹</translation>
    </message>
    <message>
        <source>%1 files</source>
        <translation type="vanished">%1 文件</translation>
    </message>
    <message>
        <source>; %1 folders</source>
        <translation type="vanished">; %1 个文件夹</translation>
    </message>
    <message>
        <source>; %1 files, %2 total</source>
        <translation type="vanished">; %1 个文件, 共%2</translation>
    </message>
    <message>
        <source>; %1 folder</source>
        <translation type="vanished">; %1 个文件夹</translation>
    </message>
    <message>
        <source>; %1 file, %2</source>
        <translation type="vanished">; %1 个文件, %2</translation>
    </message>
    <message>
        <source>%1 selected</source>
        <translation type="vanished">选中%1个</translation>
    </message>
    <message>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation type="vanished">在%2中搜索%1</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="109"/>
        <location filename="../../src/control/tab-status-bar.cpp" line="218"/>
        <source>Searching for files ...</source>
        <translation>搜尋中...</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="122"/>
        <source> %1 items </source>
        <translation> %1 個專案 </translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="159"/>
        <source> selected %1 items    %2</source>
        <translation> 選取 %1 個專案 %2</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="161"/>
        <source> selected %1 items</source>
        <translation> 選取 %1 個專案</translation>
    </message>
</context>
<context>
    <name>TabWidget</name>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="302"/>
        <source>Trash</source>
        <translation>回收站</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="306"/>
        <source>Clear</source>
        <translation>清空</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="311"/>
        <source>Recover</source>
        <translation>還原</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="471"/>
        <source>Close Filter.</source>
        <translation>關閉篩選。</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="483"/>
        <source>Filter</source>
        <translation>篩選</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="580"/>
        <source>Select Path</source>
        <translation>選擇路徑</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1109"/>
        <source>Current path: %1, %2</source>
        <translation>當前路徑：%1， %2</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1109"/>
        <source>click to select other search path.</source>
        <translation>點擊可選擇其他搜尋路徑。</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1696"/>
        <source>Opening such files is not currently supported</source>
        <translation>暫不支持打開此類檔</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1709"/>
        <source>Open failed</source>
        <translation>打開失敗</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="1710"/>
        <source>Open directory failed, you have no permission!</source>
        <translation>打開資料夾失敗，您沒有該目錄的許可權！</translation>
    </message>
    <message>
        <source>Close advance search.</source>
        <translation type="vanished">关闭高级搜索。</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="494"/>
        <source>Choose other path to search.</source>
        <translation>選擇其他搜尋路徑。</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="503"/>
        <source>Search recursively</source>
        <translation>遞歸搜索</translation>
    </message>
    <message>
        <source>more options</source>
        <translation type="vanished">更多选项</translation>
    </message>
    <message>
        <source>Show/hide advance search</source>
        <translation type="vanished">显示/隐藏高级搜索</translation>
    </message>
    <message>
        <source>Select path</source>
        <translation type="vanished">选择路径</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="640"/>
        <location filename="../../src/control/tab-widget.cpp" line="795"/>
        <source>is</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="675"/>
        <source>Please input key words...</source>
        <translation>請輸入關鍵字...</translation>
    </message>
    <message>
        <source>Please input kay words...</source>
        <translation type="vanished">请输入关键词...</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="756"/>
        <location filename="../../src/control/tab-widget.cpp" line="779"/>
        <source>contains</source>
        <translation>包含</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="280"/>
        <source>name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="280"/>
        <source>type</source>
        <translation>類型</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="280"/>
        <source>modify time</source>
        <translation>修改時間</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="280"/>
        <source>file size</source>
        <translation>檔大小</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="281"/>
        <location filename="../../src/control/tab-widget.h" line="283"/>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>all</source>
        <translation>全部</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="281"/>
        <source>file folder</source>
        <translation>資料夾</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="281"/>
        <source>image</source>
        <translation>圖片</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="281"/>
        <source>video</source>
        <translation>視頻</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="282"/>
        <source>text file</source>
        <translation>文本文檔</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="282"/>
        <source>audio</source>
        <translation>音訊</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="282"/>
        <source>others</source>
        <translation>其他</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="282"/>
        <source>wps file</source>
        <translation>WPS檔</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="283"/>
        <source>today</source>
        <translation>今天</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="283"/>
        <source>this week</source>
        <translation>本週</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="283"/>
        <source>this month</source>
        <translation>本月</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="283"/>
        <source>this year</source>
        <translation>今年</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="283"/>
        <source>year ago</source>
        <translation>一年前</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>tiny(0-16K)</source>
        <translation>極小（0-16K）</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>small(16k-1M)</source>
        <translation>很小（16k-1M）</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>empty(0K)</source>
        <translation>空（0K）</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>medium(1M-128M)</source>
        <translation>中等（1M-128M）</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>big(128M-1G)</source>
        <translation>大（128M-1G）</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>large(1-4G)</source>
        <translation>巨大（1-4G）</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="284"/>
        <source>great(&gt;4G)</source>
        <translation>極大（&gt;4G）</translation>
    </message>
    <message>
        <source>medium(1M-100M)</source>
        <translation type="vanished">中等(1M-100M)</translation>
    </message>
    <message>
        <source>big(100M-1G)</source>
        <translation type="vanished">很大(100M-1G)</translation>
    </message>
    <message>
        <source>large(&gt;1G)</source>
        <translation type="vanished">极大(&gt;1G)</translation>
    </message>
</context>
</TS>
