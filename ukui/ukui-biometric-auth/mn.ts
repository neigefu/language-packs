<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>BioAuthWidget</name>
    <message>
        <source>Retry</source>
        <translation type="obsolete">重试</translation>
    </message>
    <message>
        <source>%1 too many unsuccessful attempts,please enter password.</source>
        <translation type="obsolete">%1验证失败达最大次数，请使用密码登录</translation>
    </message>
    <message>
        <source>%1 authentication failure,there are still %2 remaining opportunities</source>
        <translation type="obsolete">%1认证失败，还剩%2次尝试机会</translation>
    </message>
    <message>
        <source>Please use wechat to scan the code</source>
        <translation type="vanished">请使用微信扫码</translation>
    </message>
</context>
<context>
    <name>BioDevices</name>
    <message>
        <source>FingerPrint</source>
        <translation type="obsolete">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="obsolete">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="obsolete">虹膜</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="obsolete">人脸</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="obsolete">声纹</translation>
    </message>
    <message>
        <source>Wechat</source>
        <translation type="vanished">微信</translation>
    </message>
    <message>
        <source>QRCode</source>
        <translation type="vanished">二维码</translation>
    </message>
</context>
<context>
    <name>LoginOptionsWidget</name>
    <message>
        <source>Login Options</source>
        <translation type="vanished">登录选项</translation>
    </message>
    <message>
        <source>Wechat</source>
        <translation type="vanished">微信</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="65"/>
        <source>Authentication</source>
        <translation>ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Fingerprint authentication failed, you still have %1 verification opportunities</source>
        <translation type="vanished">指纹验证失败，您还有%1次尝试机会</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="26"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>More</source>
        <translation type="obsolete">更多</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="obsolete">重新开始</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">密码</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="409"/>
        <location filename="../src/mainwindow.cpp" line="1075"/>
        <location filename="../src/mainwindow.cpp" line="1146"/>
        <source>Biometric</source>
        <translation>ᠠᠮᠢᠳᠤ ᠪᠤᠳᠠᠰ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="480"/>
        <source>use password</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠭᠰᠠᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>DeviceType:</source>
        <translation type="obsolete">设备类型：</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="obsolete">返回</translation>
    </message>
    <message>
        <source>Details</source>
        <translation type="vanished">详细</translation>
    </message>
    <message>
        <source>Action Id:</source>
        <translation type="obsolete">动作:</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation type="vanished">描述：</translation>
    </message>
    <message>
        <source>Polkit.subject-pid:</source>
        <translation type="vanished">Polkit.subject-pid：</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation type="obsolete">重试</translation>
    </message>
    <message>
        <source>Device types:</source>
        <translation type="obsolete">设备类型：</translation>
    </message>
    <message>
        <source>Vendor:</source>
        <translation type="vanished">发行商：</translation>
    </message>
    <message>
        <source>Action:</source>
        <translation type="vanished">动作：</translation>
    </message>
    <message>
        <source>Polkit.caller-pid:</source>
        <translation type="vanished">Polkit.caller-pid：</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="441"/>
        <location filename="../src/mainwindow.cpp" line="1077"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="460"/>
        <location filename="../src/mainwindow.cpp" line="1073"/>
        <source>Authenticate</source>
        <translation>ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1079"/>
        <location filename="../src/mainwindow.cpp" line="1189"/>
        <source>Use password</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠭᠰᠠᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Auth</source>
        <translation type="obsolete">授权</translation>
    </message>
    <message>
        <source>Too many unsuccessful attempts,please enter password.</source>
        <translation type="vanished">指纹验证失败达最大次数，请使用密码解锁</translation>
    </message>
    <message>
        <source>%1 authentication failure,there are still %2 remaining opportunities</source>
        <translation type="vanished">%1认证失败，还剩%2次尝试机会</translation>
    </message>
    <message>
        <source>%1 too many unsuccessful attempts,please enter password.</source>
        <translation type="vanished">%1验证失败达最大次数，请使用密码登录</translation>
    </message>
    <message>
        <source>in authentication, please wait...</source>
        <translation type="vanished">认证中，请稍等...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1225"/>
        <location filename="../src/mainwindow.cpp" line="1298"/>
        <location filename="../src/mainwindow.cpp" line="1299"/>
        <source>Please try again in %1 minutes.</source>
        <translation>%1 ᠮᠢᠨᠦ᠋ᠲ᠎ᠦᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠱᠢᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1236"/>
        <location filename="../src/mainwindow.cpp" line="1308"/>
        <location filename="../src/mainwindow.cpp" line="1309"/>
        <source>Please try again in %1 seconds.</source>
        <translation>%1 ᠮᠢᠨᠦ᠋ᠲ᠎ᠦᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠱᠢᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1246"/>
        <location filename="../src/mainwindow.cpp" line="1247"/>
        <location filename="../src/mainwindow.cpp" line="1317"/>
        <location filename="../src/mainwindow.cpp" line="1318"/>
        <source>Account locked permanently.</source>
        <translation>ᠳᠠᠩᠰᠠ ᠨᠢᠭᠡᠨᠳᠡ ᠦᠨᠢᠳᠡ ᠤᠨᠢᠰᠤᠯᠠᠭᠳᠠᠪᠠ.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="834"/>
        <location filename="../src/mainwindow.cpp" line="835"/>
        <source>Password cannot be empty</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Failed to verify %1, please enter password.</source>
        <translation type="vanished">验证%1失败，请输入密码.</translation>
    </message>
    <message>
        <source>Unable to verify %1, please enter password.</source>
        <translation type="vanished">无法验证%1，请输入密码.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="242"/>
        <location filename="../src/mainwindow.cpp" line="246"/>
        <source>Failed to verify %1, you still have %2 verification opportunities</source>
        <translation>%1᠎ᠶᠢᠨ/᠎ᠦᠨ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠢᠴᠠᠭᠠᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠲᠠ ᠪᠠᠰᠠ%2 ᠤᠳᠠᠭᠠᠨ᠎ᠤ ᠳᠤᠷᠱᠢᠬᠤ ᠵᠠᠪᠱᠢᠶᠠᠨ ᠲᠠᠢ</translation>
    </message>
    <message>
        <source>An application is attempting to perform an action that requires privileges. Authentication is required to perform this action.</source>
        <translation type="vanished">一个程序正试图执行一个需要特权的动作。要求授权以执行该动作。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="731"/>
        <source>Password: </source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋᠄ </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="335"/>
        <source>Please enter your password or enroll your fingerprint </source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠤᠶᠤ ᠬᠤᠷᠤᠭᠤᠨ ᠤ᠋ ᠤᠷᠤᠮ ᠵᠢᠨᠨ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="259"/>
        <source>Abnormal network</source>
        <translation>ᠲᠤᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="665"/>
        <source>This operation requires the administrator&apos;s authorization. Please enter your password to allow this operation.</source>
        <translation>ᠳᠤᠰ ᠤᠳᠠᠭᠠᠨ ᠤ᠋ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠵᠢᠨ ᠡᠷᠬᠡ ᠤᠯᠭᠤᠯᠳᠠ ᠪᠡᠷ ᠰᠠᠶᠢ ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠨ ᠬᠡᠷᠡᠭᠵᠢᠬᠦᠯᠦᠨ᠎ᠡ᠂ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠵᠢᠨᠨ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠳᠤᠰ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠭᠡᠷᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="733"/>
        <source>_Password: </source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋： </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="735"/>
        <source>_Password:</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋：</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="850"/>
        <source>Authentication failed, please try again.</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="779"/>
        <source>days left</source>
        <translation>ᠡᠳᠦᠷ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠤᠨᠢᠰᠤ ᠳᠠᠢᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Biometric/code scan authentication failed too many times, please enter the password.</source>
        <translation type="vanished">生物/扫码验证失败达最大次数，请使用密码解锁.</translation>
    </message>
    <message>
        <source>Bioauth/code scan authentication failed, you still have %1 verification opportunities</source>
        <translation type="vanished">生物/扫码验证失败，您还有%1次尝试机会</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="226"/>
        <location filename="../src/mainwindow.cpp" line="1421"/>
        <location filename="../src/mainwindow.cpp" line="1423"/>
        <source>Failed to verify %1, please enter password to unlock</source>
        <translation>%1᠎ᠶᠢ/᠎ᠢ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ ᠂ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="231"/>
        <location filename="../src/mainwindow.cpp" line="233"/>
        <location filename="../src/mainwindow.cpp" line="1425"/>
        <source>Unable to verify %1, please enter password to unlock</source>
        <translation>%1᠎ᠶᠢ/᠎ᠢ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ ᠂ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>NET Exception</source>
        <translation type="vanished">网络异常</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="671"/>
        <source>A program is attempting to perform an action that requires privileges.It requires authorization to perform the action.</source>
        <translation>ᠨᠢᠭᠡ ᠫᠡᠷᠦᠭᠷᠡᠮ ᠶᠠᠭ ᠨᠢᠭᠡ ᠤᠨᠠᠴᠠ ᠡᠷᠬᠡ ᠬᠡᠷᠡᠭᠰᠡᠬᠦ ᠬᠦᠳᠡᠯᠬᠡᠬᠡᠨ ᠢ᠋ ᠬᠡᠷᠡᠭᠵᠢᠬᠦᠯᠬᠦ ᠪᠡᠷ ᠳᠤᠷᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠳᠤᠰ ᠬᠦᠳᠡᠯᠬᠡᠬᠡᠨ ᠢ᠋ ᠬᠦᠢᠴᠡᠳᠬᠡᠬᠦ ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ ᠵᠢ ᠱᠠᠭᠠᠷᠳᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="108"/>
        <location filename="../src/mainwindow.cpp" line="729"/>
        <source>Input Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="105"/>
        <source>Insert the ukey into the USB port</source>
        <translation>ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠦ ᠨᠢᠭᠤᠴᠠ ᠶᠢ USB ᠦᠵᠦᠭᠦᠷ ᠲᠦ ᠬᠠᠳᠬᠤᠵᠤ ᠣᠷᠣᠭᠠᠷᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="107"/>
        <source>Enter the ukey password</source>
        <translation>ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠣᠷᠣᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="783"/>
        <source>hours left</source>
        <translation>ᠨᠢᠭᠡ ᠴᠠᠭ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠤᠨᠢᠰᠤ ᠳᠠᠢᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="787"/>
        <source>minutes left</source>
        <translation>ᠮᠢᠨᠦ᠋ᠲ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠤᠨᠢᠰᠤ ᠳᠠᠢᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="791"/>
        <source>seconds left</source>
        <translation>ᠰᠸᠺᠦᠨ᠋ᠲ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠤᠨᠢᠰᠤ ᠳᠠᠢᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="880"/>
        <location filename="../src/mainwindow.cpp" line="888"/>
        <source>Input your password to authentication</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠳᠤ ᠡᠷᠬᠡ ᠣᠯᠭᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1440"/>
        <source>Verify face recognition or enter password to unlock</source>
        <translation>ᠨᠢᠭᠤᠷ ᠱᠢᠷᠪᠢᠵᠤ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1445"/>
        <source>Press fingerprint or enter password to unlock</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ ᠤᠷᠤᠮ ᠳᠠᠷᠤᠬᠤ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1450"/>
        <source>Verify voiceprint or enter password to unlock</source>
        <translation>ᠳᠠᠭᠤ᠎ᠪᠠᠷ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1455"/>
        <source>Verify finger vein or enter password to unlock</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ ᠨᠠᠮᠵᠢᠭᠤᠨ ᠰᠤᠳᠠᠯ᠎ᠢᠶᠠᠷ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1460"/>
        <source>Verify iris or enter password to unlock</source>
        <translation>ᠰᠤᠯᠤᠩᠭ᠎ᠠ ᠪᠦᠷᠬᠦᠪᠴᠢ᠎ᠶᠢ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1465"/>
        <source>Use the bound wechat scanning code or enter the password to unlock</source>
        <translation>ᠤᠶᠠᠭᠰᠠᠨ ᠸᠢᠴᠠᠲ᠎ᠢᠶᠠᠷ ᠺᠤᠳ᠋ ᠱᠢᠷᠪᠢᠬᠦ᠌ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Use the bound wechat scanning code or enter the password to log in</source>
        <translation type="vanished">使用绑定的微信扫码或输入密码登录</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="779"/>
        <location filename="../src/mainwindow.cpp" line="783"/>
        <location filename="../src/mainwindow.cpp" line="787"/>
        <location filename="../src/mainwindow.cpp" line="791"/>
        <source>Account locked,</source>
        <translation>ᠳᠠᠨᠭᠰᠠ ᠵᠢ ᠨᠢᠬᠡᠨᠳᠡ ᠤᠨᠢᠰᠤᠯᠠᠪᠠ</translation>
    </message>
    <message>
        <source>Authentication failed, please try again</source>
        <translation type="obsolete">认证失败，请重试</translation>
    </message>
</context>
<context>
    <name>PolkitListener</name>
    <message>
        <location filename="../src/PolkitListener.cpp" line="88"/>
        <source>Another client is already authenticating, please try again later.</source>
        <translation>ᠦᠬᠡᠷ᠎ᠡ ᠨᠢᠭᠡ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠵᠦᠬᠦᠷᠯᠢᠭ ᠢ᠋ ᠶᠠᠭ ᠨᠤᠳᠠᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠳᠠᠬᠢᠨ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../src/PolkitListener.cpp" line="261"/>
        <source>Authentication failure, please try again.</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../src/PolkitListener.cpp" line="268"/>
        <source>Password input error!</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠪᠤᠷᠤᠭᠤ ᠤᠷᠤᠭᠤᠯᠪᠠ!</translation>
    </message>
    <message>
        <source>Account locked %1 minutes due to %2 fail attempts</source>
        <translation type="vanished">账户锁定%1分钟由于%2次错误尝试</translation>
    </message>
    <message>
        <source>Authentication failure,there are still %1 remaining opportunities</source>
        <translation type="vanished">认证失败，还剩余%1次尝试机会</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>FingerPrint</source>
        <translation type="obsolete">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="obsolete">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="obsolete">虹膜</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="obsolete">人脸</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="obsolete">声纹</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
</TS>
