<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>BioAuthWidget</name>
    <message>
        <location filename="../src/bioauthwidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>More</source>
        <translation type="vanished">更多</translation>
    </message>
    <message>
        <location filename="../src/bioauthwidget.ui" line="146"/>
        <location filename="../src/bioauthwidget.cpp" line="41"/>
        <source>Retry</source>
        <translation>重試</translation>
    </message>
    <message>
        <source>Too many unsuccessful attempts,please enter password.</source>
        <translation type="vanished">验证失败达最大次数，请使用密码解锁</translation>
    </message>
    <message>
        <source>Bioauth authentication failed, you still have %1 verification opportunities</source>
        <translation type="vanished">生物认证失败，您还有%1次尝试机会</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <source>Current Device: </source>
        <translation type="vanished">当前设备：</translation>
    </message>
    <message>
        <location filename="../src/bioauthwidget.cpp" line="96"/>
        <location filename="../src/bioauthwidget.cpp" line="212"/>
        <source>%1 too many unsuccessful attempts,please enter password.</source>
        <translation>%1失敗的嘗試次數太多，請輸入密碼</translation>
    </message>
    <message>
        <location filename="../src/bioauthwidget.cpp" line="102"/>
        <source>%1 authentication failure,there are still %2 remaining opportunities</source>
        <translation>%1 身份驗證失敗，仍有 %2 個剩餘機會</translation>
    </message>
    <message>
        <location filename="../src/bioauthwidget.cpp" line="226"/>
        <source>Please use wechat to scan the code</source>
        <translation>請使用微信掃碼</translation>
    </message>
</context>
<context>
    <name>BioDevices</name>
    <message>
        <location filename="../src/biodevices.cpp" line="78"/>
        <source>Unplugging of %1 device detected</source>
        <translation>檢測到 %1 設備的拔出</translation>
    </message>
    <message>
        <location filename="../src/biodevices.cpp" line="88"/>
        <source>%1 device insertion detected</source>
        <translation>檢測到 %1 個設備插入</translation>
    </message>
    <message>
        <location filename="../src/biodevices.cpp" line="97"/>
        <source>ukui-biometric-manager</source>
        <translation>生物特徵管理工具</translation>
    </message>
    <message>
        <location filename="../src/biodevices.cpp" line="100"/>
        <source>biometric</source>
        <translation>生物特徵管理工具</translation>
    </message>
    <message>
        <location filename="../src/biodevices.cpp" line="647"/>
        <source>FingerPrint</source>
        <translation>指紋</translation>
    </message>
    <message>
        <location filename="../src/biodevices.cpp" line="649"/>
        <source>FingerVein</source>
        <translation>指靜脈</translation>
    </message>
    <message>
        <location filename="../src/biodevices.cpp" line="651"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../src/biodevices.cpp" line="653"/>
        <source>Face</source>
        <translation>人臉</translation>
    </message>
    <message>
        <location filename="../src/biodevices.cpp" line="655"/>
        <source>VoicePrint</source>
        <translation>聲紋</translation>
    </message>
    <message>
        <location filename="../src/biodevices.cpp" line="657"/>
        <source>ukey</source>
        <translation>安全金鑰</translation>
    </message>
    <message>
        <location filename="../src/biodevices.cpp" line="659"/>
        <source>QRCode</source>
        <translation>微信掃碼</translation>
    </message>
    <message>
        <source>Wechat</source>
        <translation type="vanished">微信</translation>
    </message>
</context>
<context>
    <name>BioDevicesWidget</name>
    <message>
        <location filename="../src/biodeviceswidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Device types:</source>
        <translation type="vanished">设备类型：</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">返回</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>FingerPrint</source>
        <translation type="vanished">指纹</translation>
    </message>
</context>
<context>
    <name>LoginOptionsWidget</name>
    <message>
        <location filename="../src/loginoptionswidget.cpp" line="61"/>
        <source>Login Options</source>
        <translation>登錄選項</translation>
    </message>
    <message>
        <location filename="../src/loginoptionswidget.cpp" line="294"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <source>Wechat</source>
        <translation type="vanished">微信</translation>
    </message>
    <message>
        <source>Biometric</source>
        <translation type="vanished">生物识别</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>FingerPrint</source>
        <translation type="vanished">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="vanished">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="vanished">虹膜</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="vanished">人脸</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="vanished">声纹</translation>
    </message>
</context>
</TS>
